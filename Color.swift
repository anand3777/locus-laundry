//
//  Color.swift
//  RippleJump
//
//  Created by Anand on 09/01/17.
//  Copyright © 2017 Augusta. All rights reserved.
//

import UIKit
import Foundation

extension UIColor {
    
    
    class func deSelectedCatrgoryBG() -> UIColor {
        return UIColor(red: 103.0/255.0, green: 177.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    }
    
    
    class func sectionColor() -> UIColor {
        return UIColor(red: 251.0/255.0, green: 251.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    }
    
    class func appBGColor() -> UIColor {
        return UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    }
 
    class func msgBG() -> UIColor {
        return UIColor.red
    }

    class func textFieldTextColor() -> UIColor {
        return UIColor(red: 127.0/255.0, green: 192.0/255.0, blue: 246.0/255.0, alpha: 1.0)
    }
    
    class func unSelectedViewColor() -> UIColor {
        return UIColor(red: 112.0/255.0, green: 193.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    }
}
