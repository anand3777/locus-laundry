//
//  PriceListVC.swift
//  SWNavigationDrawer
//
//  Created by Anand on 24/08/16.
//  Copyright © 2016 Anand. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PriceListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var priceTbl: UITableView!
    var laundryList:NSMutableArray = NSMutableArray()
    let priceListTblIdentifier = "PriceListCell"
    var ListArray:NSDictionary = NSDictionary()
    
    let segmentCtrl_G = WBSegmentControl()
    var orderPlaceInfo : MLOrderPlaceModel!
    
    
    var laundryArray = [[String : String]]()
    var menArray = [[String : String]]()
    
    var seleectedCategory:Int = 0
    
    let selectedItemList:NSMutableArray = NSMutableArray()
    let seleetedMutableIndexList:NSMutableArray = NSMutableArray ()
    
    var catImageArray:NSArray = NSArray.init()
    
    
    
    // var priceListArray:NSArray = NSArray.init()
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    var arySavebooking = NSMutableArray()
    
    @IBOutlet var laundryView: UIView!
    @IBOutlet var landryBtn: UIButton!
    @IBOutlet var menView: UIView!
    @IBOutlet var menBtn: UIButton!
    @IBOutlet var womenView: UIView!
    @IBOutlet var womenBtn: UIButton!
    @IBOutlet var houseHoldView: UIView!
    @IBOutlet var houseHoldBtn: UIButton!
    
    
    override func loadView() {
        super.loadView()
        
        Session().saveSelectedObjects(value: [])
        Session().saveTotalPrice(value: "0")
        Session().saveSelectedServiceTitle(value: [])
        
        
        //self.view.addSubview(segmentCtrl_G)
//        segmentCtrl_G.frame = CGRect.init(x: 0, y: 64, width: deviceSize.width, height: 60)
//        segmentCtrl_G.segments = [
//            TextSegment(text: "Laundry".localized()),
//            TextSegment(text: "Men".localized()),
//            TextSegment(text: "Women".localized()),
//            TextSegment(text: "House hold".localized())
//            // TextSegment(text: "Accessories".localized()),
//        ]
        
//        segmentCtrl_G.imageArray = ["p_laundry",
//                                    "p_men",
//                                    "p_women",
//                                    "p_house_hold"]
//        //"p_house_hold"]
//        
//        
//        segmentCtrl_G.enableSeparator = true
//        segmentCtrl_G.delegate = self
//        segmentCtrl_G.style = .strip
//        segmentCtrl_G.nonScrollDistributionStyle = .center
        
        if selectedDrawerRow > 3 {
            Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"PRICE LIST".localized() as NSString, isRightBtnImg: false, rightBtnName: "Next", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        } else {
            Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"PRICE LIST".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
            
        }
        
        
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
        
        
        let totalPrice:Double = Double(Session().getTotalPrice() as String)!
        print("totalPrice:----->\(totalPrice)");

        if totalPrice > 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLaundryVC") as! SelectLaundryVC
            vc.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [vc]
        } else{
            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Items", bgColor: UIColor.msgBG(), controller: self)
        }
        
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        self.configInitallView()
        self.callPriceListAPI()
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
    func configInitallView() {
        
        totalLabel.text = "Total".localized()
        
        segmentCtrl_G.selectedIndex = seleectedCategory
        
        //        laundryList = ["Edit Profile","Change Password","Blocked Users"]
        
        priceTbl.delegate = self
        priceTbl.dataSource = self
        priceTbl.tableFooterView = UIView()
        priceTbl!.register(UINib(nibName: priceListTblIdentifier, bundle: nil), forCellReuseIdentifier: priceListTblIdentifier)
        
        /*    laundryArray = [
         ["title" : "Wash & Fold (per kg)",
         "price" : "59",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ]
         */
        
        /*    menArray = [
         ["title" : "T-Shirt",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Shirt",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Sweater",
         "price" : "80",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Trouser",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Suite Shirt",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Suite Trowser",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Suite Vest",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Shorts",
         "price" : "79",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Coat / Blazer",
         "price" : "200",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Leather Jacket",
         "price" : "600",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ["title" : "Jacket",
         "price" : "400",
         "category_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "quantity" : "0"],
         ]
         */
        
    }
    
    //price List API
    
    func callPriceListAPI() {
        
            if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            var service_id:String = ""
         //   let index = Session().savedSelectedServiceIDs().object(at: 0) as! String
            let index = Session().savedSelectedServiceIDs()

            service_id = index.componentsJoined(by: ",") // "1-2-3"
                
             print("service_id:----->\(service_id)")
     
            let params:NSDictionary = [
                "user_id" : userID,
                "latitude_value" : currentLocation.coordinate.latitude as Any,
                "longitude_value" : currentLocation.coordinate.longitude as Any,
                "service_id" : service_id
            ]
            print("params:---->\(params)")
            Alamofire.request(BASE_URL + kPriceList, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.request?.url as Any)  // original URL request
                    self.ListArray = response.result.value as! NSDictionary
                    let list = self.ListArray.value(forKeyPath: "details") as! NSArray
                    self.laundryList.addObjects(from: list as! [Any])
                    self.catImageArray = self.laundryList.value(forKeyPath: "cat_icons") as! NSArray!
                    print(self.catImageArray.mutableCopy() as! NSMutableArray)
                    
                    self.segmentCtrl_G.frame = CGRect.init(x: 0, y: 64, width: deviceSize.width, height: 60)
                    self.segmentCtrl_G.segments = [
                        TextSegment(text: "Laundry (Kg)".localized()),
                        TextSegment(text: "Men".localized()),
                        TextSegment(text: "Women".localized()),
                        TextSegment(text: "House hold".localized())
                        // TextSegment(text: "Accessories".localized()),
                    ]

                  //  DispatchQueue.main.async {
                        self.segmentCtrl_G.imageArray = ["p_laundry",
                                                        "p_men",
                                                        "p_women",
                                                        "p_house_hold"]

                        //self.segmentCtrl_G.imageArray = self.catImageArray.mutableCopy() as! NSMutableArray
                 //   }
                    //"p_house_hold"]
                    
                    self.segmentCtrl_G.enableSeparator = true
                    self.segmentCtrl_G.delegate = self
                    self.segmentCtrl_G.style = .strip
                    self.segmentCtrl_G.nonScrollDistributionStyle = .center

                    self.priceTbl.reloadData()
                    self.hideActivityIndicator(self.view)
            }
        }
    }
    
    
    //MARK: - UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = Bundle.main.loadNibNamed("PriceListSessionHeaderView", owner: nil, options: nil)![0] as! PriceListSessionHeaderView
        
        switch section {
        case 0:
            headerCell.headerTitleLbl.text = "Laundry"
        case 1:
            headerCell.headerTitleLbl.text = "Premium Laundry"
        case 2:
            headerCell.headerTitleLbl.text = "Add Ons"
        default:
            headerCell.headerTitleLbl.text = ""
        }
        return headerCell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*  switch seleectedCategory {
         case 0:
         return laundryArray.count
         case 1:
         return menArray.count
         default:
         return laundryArray.count
         }
         */
        print(laundryList)
        
        return laundryList.count == 0 ? 0 : ((laundryList.object(at: seleectedCategory) as! NSDictionary).value(forKey: "sub_category") as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var listCell:PriceListCell = tableView.dequeueReusableCell(withIdentifier: priceListTblIdentifier) as! PriceListCell
        listCell.selectionStyle = .none
        
        
        if listCell == nil {
            listCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:priceListTblIdentifier) as! PriceListCell
        }
        
//        for subView in (listCell.subviews) {
//            subView.removeFromSuperview()
//        }

        var tieleName = String(format: "%@","\((((laundryList.object(at: seleectedCategory) as! NSDictionary).value(forKey: "sub_category") as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "service_name")!)")
        
        if tieleName.contains("Wash & Iron") || tieleName.contains("Dry Clean"){
            
        } else {
            tieleName = tieleName.replacingOccurrences(of: " (Kg)", with: "")
        }
        
        let catimageURL = "\((((laundryList.object(at: seleectedCategory) as! NSDictionary).value(forKey: "sub_category") as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "sub_cat_icons")!)"
        let catimagePrice = "SR \((((laundryList.object(at: seleectedCategory) as! NSDictionary).value(forKey: "sub_category") as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "price_per_unit")!)"
        let quantity = "\((((laundryList.object(at: seleectedCategory) as! NSDictionary).value(forKey: "sub_category") as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "count")!)"
        
              

        DispatchQueue.global().async {
            let url = URL(string: catimageURL)
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                    if data != nil{
                    let image = UIImage.init(data: data!)
                    listCell.catImage.image = image
                }
            }
        }
        
        listCell.catTitleLbl.text = tieleName
        listCell.priceLbl.text = catimagePrice
        listCell.quantityCountLbl.text = quantity
        
        listCell.addProductBtn.tag = indexPath.row
        listCell.reduceProductBtn.tag = indexPath.row
        
        listCell.addProductBtn.addTarget(self, action: #selector(addProductButtonTapped), for: .touchUpInside)
        listCell.reduceProductBtn.addTarget(self, action: #selector(reduceProductButtonTapped), for: .touchUpInside)
        
//        for subView in (listCell.subviews) {
//            subView.removeFromSuperview()
//        }
        
        return listCell
    }
    
    //MARK:- Add & Remove Product By increament or Decreament Count
    func addProductButtonTapped(sender : UIButton)->Void{
        
        var selectedIndedict = NSDictionary()
        var selectedIndexDictionar = NSMutableDictionary()
        selectedIndedict = laundryList.object(at: seleectedCategory) as! NSDictionary
        selectedIndexDictionar = selectedIndedict.mutableCopy() as! NSMutableDictionary
        let selectedproductary = NSMutableArray()
        selectedproductary.addObjects(from: (selectedIndexDictionar.value(forKey: "sub_category") as! NSArray) as! [Any])
        var selectedProduct = NSDictionary()
        selectedProduct = selectedproductary.object(at: sender.tag) as! NSDictionary
        print(selectedProduct)
        
        
        var replaceProduct = NSMutableDictionary()
        replaceProduct = selectedProduct.mutableCopy() as! NSMutableDictionary
        var count = Int()
        var amount = Int()
        var categoryId = Int()
        
        count = Int("\(replaceProduct.value(forKey: "count")!)")!
        amount = Int("\(replaceProduct.value(forKey: "price_per_unit")!)")!
        categoryId = Int("\(selectedIndexDictionar.value(forKey: "category_id")!)")!
        
        count = count + 1
        amount = amount * count
        replaceProduct.setObject("\(count)", forKey: "count" as NSCopying)
        replaceProduct.setObject("\(amount)", forKey: "total_amount" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "price_per_unit")!, forKey: "single_unit_price" as NSCopying)
        replaceProduct.setObject("\(categoryId)", forKey: "category_id" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "sub_service_id")!, forKey: "sub_category_id" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "service_name")!, forKey: "sub_category_name" as NSCopying)


        selectedproductary.replaceObject(at: sender.tag, with: replaceProduct)
        
        print(selectedproductary)
        
        selectedIndexDictionar.setObject(selectedproductary, forKey: "sub_category" as NSCopying)
        laundryList.replaceObject(at: seleectedCategory, with: selectedIndexDictionar)
        print(selectedIndexDictionar)
        
        if count != 0 {
            var indexForCartList = Int()
            print(selectedproductary)
            
            if arySavebooking.contains(selectedProduct) {
                indexForCartList = arySavebooking.index(of: selectedProduct)
                arySavebooking.replaceObject(at: indexForCartList, with: replaceProduct)
            }else{
                arySavebooking.add(replaceProduct)
            }
            print(arySavebooking)
            
            //Save selected Array
            Session().saveSelectedObjects(value: arySavebooking)
        }
        
        
        
        totalValue.text = "SR \(getTotalAmount(totalArray: laundryList))"
        
        Session().saveTotalPrice(value: getTotalAmount(totalArray: laundryList) as NSString)
        
        priceTbl.reloadData()
    }
    
    func reduceProductButtonTapped(sender : UIButton)->Void{
        
        var selectedIndedict = NSDictionary()
        var selectedIndexDictionar = NSMutableDictionary()
        selectedIndedict = laundryList.object(at: seleectedCategory) as! NSDictionary
        selectedIndexDictionar = selectedIndedict.mutableCopy() as! NSMutableDictionary
        let selectedproductary = NSMutableArray()
        selectedproductary.addObjects(from: (selectedIndexDictionar.value(forKey: "sub_category") as! NSArray) as! [Any])
        var selectedProduct = NSDictionary()
        selectedProduct = selectedproductary.object(at: sender.tag) as! NSDictionary
        print(selectedProduct)
        var replaceProduct = NSMutableDictionary()
        replaceProduct = selectedProduct.mutableCopy() as! NSMutableDictionary
        
        var count = Int()
        var amount = Int()
        var categoryId = Int()

        
        count = Int("\(replaceProduct.value(forKey: "count")!)")!
        count = count == 0 ? count : count - 1
        amount = Int("\(replaceProduct.value(forKey: "price_per_unit")!)")!
        categoryId = Int("\(selectedIndexDictionar.value(forKey: "category_id")!)")!
        
        amount = amount * count

        replaceProduct.setObject("\(count)", forKey: "count" as NSCopying)
        replaceProduct.setObject("\(amount)", forKey: "total_amount" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "price_per_unit")!, forKey: "single_unit_price" as NSCopying)
        replaceProduct.setObject("\(categoryId)", forKey: "category_id" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "sub_service_id")!, forKey: "sub_category_id" as NSCopying)
        replaceProduct.setObject(replaceProduct.value(forKey: "service_name")!, forKey: "sub_category_name" as NSCopying)
        
        selectedproductary.replaceObject(at: sender.tag, with: replaceProduct)
        selectedIndexDictionar.setObject(selectedproductary, forKey: "sub_category" as NSCopying)
        laundryList.replaceObject(at: seleectedCategory, with: selectedIndexDictionar)
        
        totalValue.text = "SR \(getTotalAmount(totalArray: laundryList))"
        
        var indexForCartList = Int()
        if count != 0 {
            
            print(selectedproductary)
            if arySavebooking.contains(selectedProduct) {
                indexForCartList = arySavebooking.index(of: selectedProduct)
                arySavebooking.replaceObject(at: indexForCartList, with: replaceProduct)
            }else{
                arySavebooking.add(replaceProduct)
            }
            print(arySavebooking)
            
            //Save selected Array
            Session().saveSelectedObjects(value: arySavebooking)
        }else{
            if count == 0 {
                indexForCartList = arySavebooking.index(of: selectedProduct)
                arySavebooking.removeObject(at: indexForCartList)
                //Save selected Array
                Session().saveSelectedObjects(value: arySavebooking)

            }
//            indexForCartList = arySavebooking.index(of: selectedProduct)
//            arySavebooking.removeObject(at: indexForCartList)
//            //Save selected Array
//            Session().saveSelectedObjects(value: arySavebooking)
        }
       
        
        
        totalValue.text = "SR \(getTotalAmount(totalArray: laundryList))"
        
        Session().saveTotalPrice(value: getTotalAmount(totalArray: laundryList) as NSString)
        
        
        priceTbl.reloadData()
    }
    
    private func getTotalAmount(totalArray:NSMutableArray) -> String {
        var addProduct = 0.0
        for itemCount in laundryList {
            for subcategory in (itemCount as AnyObject).value(forKey: "sub_category") as! NSArray {
                print(subcategory)
                addProduct =  Int("\(((subcategory as AnyObject).value(forKey: "count"))!)")! > 0 ? ((Double("\(((subcategory as AnyObject).value(forKey: "count"))!)")! - 0) * Double("\(((subcategory as AnyObject).value(forKey: "price_per_unit"))!)")!) + addProduct : addProduct
            }
        }
        return "\(addProduct)"
    }
    
    @IBAction func didTapOnLaundry(_ sender: Any) {
        seleectedCategory = 0
        self.setCategoryBg()
    }
    
    @IBAction func didTapOnMen(_ sender: Any) {
        seleectedCategory = 1
        self.setCategoryBg()
    }
    
    @IBAction func didTapOnWomen(_ sender: Any) {
        seleectedCategory = 2
        self.setCategoryBg()
    }
    
    @IBAction func didTapOnHouseHold(_ sender: Any) {
        seleectedCategory = 3
        self.setCategoryBg()
    }
    
    func setCategoryBg(){
        
        switch seleectedCategory {
        case 0:
            laundryView.backgroundColor = UIColor.appBGColor()
            menView.backgroundColor = UIColor.unSelectedViewColor()
            womenView.backgroundColor = UIColor.unSelectedViewColor()
            houseHoldView.backgroundColor = UIColor.unSelectedViewColor()

            landryBtn.setImage(UIImage.init(named: "p_laundry"), for: .normal)
            menBtn.setImage(UIImage.init(named: "p_ds_men"), for: .normal)
            womenBtn.setImage(UIImage.init(named: "p_ds_women"), for: .normal)
            houseHoldBtn.setImage(UIImage.init(named: "p_ds_house_hold"), for: .normal)
            break
        case 1:
            laundryView.backgroundColor = UIColor.unSelectedViewColor()
            menView.backgroundColor = UIColor.appBGColor()
            womenView.backgroundColor = UIColor.unSelectedViewColor()
            houseHoldView.backgroundColor = UIColor.unSelectedViewColor()
            
            landryBtn.setImage(UIImage.init(named: "p_ds_laundry"), for: .normal)
            menBtn.setImage(UIImage.init(named: "p_men"), for: .normal)
            womenBtn.setImage(UIImage.init(named: "p_ds_women"), for: .normal)
            houseHoldBtn.setImage(UIImage.init(named: "p_ds_house_hold"), for: .normal)

            break
        case 2:
            laundryView.backgroundColor = UIColor.unSelectedViewColor()
            menView.backgroundColor = UIColor.unSelectedViewColor()
            womenView.backgroundColor = UIColor.appBGColor()
            houseHoldView.backgroundColor = UIColor.unSelectedViewColor()

            landryBtn.setImage(UIImage.init(named: "p_ds_laundry"), for: .normal)
            menBtn.setImage(UIImage.init(named: "p_ds_men"), for: .normal)
            womenBtn.setImage(UIImage.init(named: "p_women"), for: .normal)
            houseHoldBtn.setImage(UIImage.init(named: "p_ds_house_hold"), for: .normal)

            break
        case 3:
            laundryView.backgroundColor = UIColor.unSelectedViewColor()
            menView.backgroundColor = UIColor.unSelectedViewColor()
            womenView.backgroundColor = UIColor.unSelectedViewColor()
            houseHoldView.backgroundColor = UIColor.appBGColor()

            landryBtn.setImage(UIImage.init(named: "p_ds_laundry"), for: .normal)
            menBtn.setImage(UIImage.init(named: "p_ds_men"), for: .normal)
            womenBtn.setImage(UIImage.init(named: "p_ds_women"), for: .normal)
            houseHoldBtn.setImage(UIImage.init(named: "p_house_hold"), for: .normal)

            break
        default:
            break
        }
        
        priceTbl.reloadData()

    }
    
}

extension PriceListVC: WBSegmentControlDelegate {
    
    func segmentControl(_ segmentControl: WBSegmentControl, selectIndex newIndex: Int, oldIndex: Int) {
        //        print("newIndex:---->\(newIndex)")
        //        print("oldIndex:---->\(oldIndex)")
        seleectedCategory = newIndex
        priceTbl.reloadData()
    }
}

