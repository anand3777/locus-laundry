//
//  SliderViewController.swift
//  MR-Laundry
//
//  Created by Gowthaman on 19/11/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit



class SliderViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //@IBOutlet var logoImg:UIImageView!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var newUserBtn: UIButton!

    @IBOutlet var serivcePageCtrl: UICollectionView!
    @IBOutlet var pageCtrl: UIPageControl!
    let collectionIdentifier = "ServiceCollectionCell"
    let arrayCount = 4
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    @IBOutlet var scrollTitle: UILabel!
    @IBOutlet var scrollContent: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        self.configPageCtrl()
    }
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "LoginVC", sender: nil)
    }
    
    @IBAction func newUserButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "SignUpVC", sender: nil)
    }
    
    func configPageCtrl() {
       serivcePageCtrl.register(UINib(nibName: collectionIdentifier, bundle:nil), forCellWithReuseIdentifier: collectionIdentifier)
        pageCtrl.numberOfPages = arrayCount
        serivcePageCtrl.delegate = self
        serivcePageCtrl.dataSource = self
        serivcePageCtrl.isPagingEnabled = true
    }
    
    // MARK: - CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let postsCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionIdentifier, for: indexPath) as! ServiceCollectionCell
        
        postsCollectionCell.serviceImages.image = UIImage.init(named: String(format: "bg%d", (indexPath.row+1)))
        
        if indexPath.row % 2 == 0 {
            postsCollectionCell.backgroundColor = UIColor.lightGray
        } else {
            postsCollectionCell.backgroundColor = UIColor.darkGray
        }
        
        return postsCollectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : self.view.frame.width , height : deviceSize.height - (20 + 50))
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    /*
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let path = IndexPath(row: 0, section: 0)
        let cell = serviceTbl.cellForRow(at: path) as! PageCtrlCell
        let x = cell.serivcePageCtrl.contentOffset.x
        let w = cell.serivcePageCtrl.bounds.size.width
        let currentPage = Int(ceil(x/w))
        cell.pageCtrl.currentPage = currentPage
        if isLastPage{
            let index = IndexPath(row:0 , section : 0 )
            cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: false)
            cell.pageCtrl.currentPage = 0
            isLastPage = false
            return
        }
        
        if currentPage == arrayCount - 1{
            isLastPage = true
        }
    }
    
    func scrollToNextCell(){
        let path = IndexPath(row: 0, section: 0)
        let cell = serviceTbl.cellForRow(at: path) as! PageCtrlCell
        let x = cell.serivcePageCtrl.contentOffset.x
        let w = cell.serivcePageCtrl.bounds.size.width
        let currentPage = Int(ceil(x/w))
        
        if currentPage == arrayCount - 1{
            isLastPage = true
            let index = IndexPath(row:0 , section : 0 )
            cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: false)
            cell.pageCtrl.currentPage = 0
            isLastPage = false
            return
        }
        let index = IndexPath(row: currentPage + 1 , section : 0 )
        cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: true)
        cell.pageCtrl.currentPage = currentPage + 1
        
    }
    
    func startTimer() {
        _ = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(SliderViewController.scrollToNextCell), userInfo: nil, repeats: true);
    }
    */
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))

        print("currentPage:----->\(currentPage)")
        
        switch currentPage {
        case 0:
            scrollTitle.text = "Dress Up Next Day"
            scrollContent.text = "We bring your fresh clothes back the very next Day"
            break
        case 1:
            scrollTitle.text = "Laundy & Dry Cleaning"
            scrollContent.text = "We pickup, wash and deliver your laundry"
            break
        case 2:
            scrollTitle.text = "Delivery"
            scrollContent.text = "Delivery with in 72 hours and express delivery on request"
            break
        case 3:
            scrollTitle.text = "Meet Our Pickup Pilot"
            scrollContent.text = "We collect your dirty clothes from your doorsteps"
            break
        default:
            break
        }
    }
    
    
    
    
    
    
    
    
    
    // testing function for uiimage
    fileprivate func testWithImage(_ rotation: rotationWay){
        
        let _image1: UIImage = UIImage(named:"bg1")!
        let _image2: UIImage = UIImage(named:"bg2")!
        let _image3: UIImage = UIImage(named:"bg3")!
        let _image4: UIImage = UIImage(named:"bg4")!
        
        let imageArray: [UIImage] = [_image1,_image2,_image3,_image4,_image1]
        
        let sss: Hola = Hola(frame: CGRect(), imageArray: imageArray, rotation)
        sss.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 50)
        view.addSubview(sss)
        //view.bringSubview(toFront: logoImg)
        
        let label1: UILabel = UILabel()
        label1.textColor = UIColor.white
        label1.text = "Simple View Label 1"
        label1.textAlignment = .center
        label1.font = UIFont.boldSystemFont(ofSize: 18)
        label1.frame = CGRect(x: 0, y: 0, width: 250, height:50)
        label1.center = view.center
        sss.addSubview(label1)
        
//        let signIn: UIButton = UIButton()
//        signIn.frame = CGRect(x: 0, y: view.frame.height - 50, width: view.frame.width/2 - 50, height:50)
//        signIn.titleLabel?.text = "Sign In"
//        signIn.titleLabel?.textColor = UIColor.red
//        signIn.backgroundColor = UIColor.white
//        sss.addSubview(signIn)
//        
//        let newUser: UIButton = UIButton()
//        newUser.frame = CGRect(x: signIn.frame.origin.x + signIn.frame.size.width + 1, y: view.frame.height - 50, width: view.frame.width - signIn.frame.size.width - 1 , height:50)
//        newUser.titleLabel?.text = "New User"
//        newUser.titleLabel?.textColor = UIColor.white
//        newUser.setImage(#imageLiteral(resourceName: "signUp"), for: .normal)
//        sss.addSubview(newUser)
        

    }
    
    // testing function for uiview
    fileprivate func testWithView(_ rotation: rotationWay){
        let view1: UIView = UIView()
        view1.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view1.backgroundColor = UIColor.lightGray
        
        let label1: UILabel = UILabel()
        label1.textColor = UIColor.white
        label1.text = "Simple View Label 1"
        label1.textAlignment = .center
        label1.font = UIFont.boldSystemFont(ofSize: 18)
        label1.frame = CGRect(x: 0, y: 0, width: 250, height:50)
        label1.center = view1.center
        view1.addSubview(label1)
        
        let view2: UIView = UIView()
        view2.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view2.backgroundColor = UIColor.brown
        
        let label2: UILabel = UILabel()
        label2.textColor = UIColor.white
        label2.text = "Simple View Label 2"
        label2.textAlignment = .center
        label2.font = UIFont.boldSystemFont(ofSize: 18)
        label2.frame = CGRect(x: 0, y: 0, width: 250, height:50)
        label2.center = view2.center
        view2.addSubview(label2)
        
        let view3: UIView = UIView()
        view3.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view3.backgroundColor = UIColor.black
        
        let label3: UILabel = UILabel()
        label3.textColor = UIColor.white
        label3.text = "Simple View Label 3"
        label3.textAlignment = .center
        label3.font = UIFont.boldSystemFont(ofSize: 18)
        label3.frame = CGRect(x: 0, y: 0, width: 250, height:50)
        label3.center = view3.center
        view3.addSubview(label3)
        
        let viewArray: [UIView] = [view1,view2,view3]
        
        let ttt: Hola = Hola(frame: CGRect(), viewArray: viewArray, rotation)
        ttt.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(ttt)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
