//
//  AppDelegate.swift
//  MR-Laundry
//
//  Created by LocusTech on 29/05/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

var deviceSize = UIScreen.main.bounds.size
var checkFirst: Bool = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        
        IQKeyboardManager.sharedManager().enable = true
        
        Session().setInitialLoadingStaus(false)
        GMSPlacesClient.provideAPIKey("AIzaSyBUsm4tivjNAzMs_ix2csObZc9yrAaKRxQ")
        GMSServices.provideAPIKey("AIzaSyBUsm4tivjNAzMs_ix2csObZc9yrAaKRxQ")
                
//        Helper.sharedInstance.saveSelectedLanguage(lang: "en")
//        Localize.setCurrentLanguage("en")
        if Helper.sharedInstance.getSelectedLanguage() == ""{
            
            Helper.sharedInstance.saveSelectedLanguage(lang: "en")
        }
        
        if Helper.sharedInstance.getSelectedLanguage() == "en" {
           // Helper.sharedInstance.saveSelectedLanguage(lang: "ar")
            Localize.setCurrentLanguage("en")
        }else{
            Helper.sharedInstance.saveSelectedLanguage(lang: "ar")
            Localize.setCurrentLanguage("ar")
        }
        
        if Session().getIsUserLoginStaus() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initial = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let rootNavigationController = UINavigationController(rootViewController: initial)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = rootNavigationController
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initial = storyboard.instantiateViewController(withIdentifier: "SliderViewController") as! SliderViewController
            let rootNavigationController = UINavigationController(rootViewController: initial)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = rootNavigationController
        }
        

        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

