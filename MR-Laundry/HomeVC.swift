//
//  HomeVC.swift
//  SWNavigationDrawer
//
//  Created by Anand on 27/08/16.
//  Copyright © 2016 Anand. All rights reserved.
//

import UIKit
import CoreLocation
var bottomViewheight: Int = 40
var currentLocation = CLLocation()


class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate{
    @IBOutlet var initialActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var initialView: UIView!
    @IBOutlet var botomViewHeight: NSLayoutConstraint!
    @IBOutlet var tblBottom: NSLayoutConstraint!
    
    var navigationDrawer:NavigationDrawer!
    
    @IBOutlet var serviceTbl: UITableView!
    let collectionIdentifier = "ServiceCollectionCell"
    let pageCtrlCellIdentifier = "PageCtrlCell"
    let selectServiceCellIdentifier = "SelectServiceCell"
    let serviceCellIdentifier = "HomeScreenServiceCell"
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    var isLastPage = Bool()
    let arrayCount = 5
    var serviceArray = [[String : String]]()
    
    var params = Dictionary<String, AnyObject>()
    var userDetailsInfo : MLUserModel!
    var orderPlaceInfo : MLOrderPlaceModel!
    
    let locationManager = CLLocationManager()
    
    
    @IBOutlet weak var minimumOrderLbl: UILabel!
    
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var priceList: UIButton!
    
    var isButtonSelected:Bool = Bool()
    var lat  = ""
    var long = ""
    
    @IBOutlet var languBtn: UIButton!
    
    var selectedList:NSMutableArray = NSMutableArray.init()
    var selectedServiceIDList:NSMutableArray = NSMutableArray.init()
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.isNavigationBarHidden = true

        if Helper.sharedInstance.getSelectedLanguage() == "en" {
            self.languBtn.setImage(UIImage.init(named: "sauthi"), for: .normal)
        }else{
            self.languBtn.setImage(UIImage.init(named: "us"), for: .normal)
        }
        
        
        
        selectedList = []
        
        isButtonSelected = false
        
        //didload
        orderPlaceInfo = MLOrderPlaceModel()
        
        if ((navigationDrawer == nil) && checkFirst == true) {
            checkFirst = false
            let options = NavigationDrawerOptions()
            options.navigationDrawerType = .leftDrawer
            options.navigationDrawerOpenDirection = .anyWhere
            options.navigationDrawerYPosition = 0
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
            navigationDrawer = NavigationDrawer.sharedInstance
            navigationDrawer.setup(withOptions: options)
            navigationDrawer.setNavigationDrawerController(vc)
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
        
        // Swift 3
        let modelName = UIDevice.current.modelName
        print("modelName:======>\(modelName))")
        userDetailsInfo = DemoGlobalData.sharedInstance.userDetailInfo
        languBtn.layer.cornerRadius = languBtn.frame.size.width / 2
        languBtn.clipsToBounds = true
    }
    
    @IBAction func didTapOnWashAndFold(_ sender: Any) {
    
    }
    
    @IBAction func didTapOnWashAndIron(_ sender: Any) {
        
    }
    
    @IBAction func didTapOnDryClean(_ sender: Any) {
        
    }
    
    @IBAction func didTapOnSpeedy(_ sender: Any) {
        
    }
    
    @IBAction func didTapOnRateList(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
        selectedDrawerRow = 3
        Session.sharedInstance.saveSelectedServiceIDs(value: [0])
        self.navigationController?.viewControllers = [vc]
    }
    
    @IBAction func didTapOnSelectTimeSlop(_ sender: Any) {
        selectedDrawerRow = 100
        self.validateServices()
    }
    
    func validateServices() {
        self.clearPreviousOrderDetails()
        Session.sharedInstance.saveSelectedService(value: selectedList)
        Session.sharedInstance.saveSelectedServiceIDs(value: selectedServiceIDList)
        print("saveSelectedService:----->\(Session.sharedInstance.savedSelectedService())")
        print("saveSelectedService IDs:----->\(Session.sharedInstance.savedSelectedServiceIDs())")
        
        let ids = Session.sharedInstance.savedSelectedServiceIDs()
        print(ids)
        
        var test = ""
        for id in ids {
            test = id as! String
            print("test:------>\(test)")
        }
        
        if test == "1"{
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else if test == "2"{
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else if test == "1" && test == "4" || test == "3" {
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else if test == "1" && test == "3" || test == "4" {
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else if test == "2" && test == "3" || test == "4"{
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else if test == "2" && test == "4" || test == "3"{
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        } else {
            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Service", bgColor: UIColor.msgBG(), controller: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        titleLabel.text = "SCHEDULE A PICKUP".localized()
        if Session().getInitialLoadingStaus() == true {
            initialView.isHidden = true
        } else {
            if Session().getIsUserLoginStaus() {
                initialActivityIndicator.startAnimating()
                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.dashBoardView), userInfo: nil, repeats: false);
            } else {
                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.showLogin), userInfo: nil, repeats: false);
                initialActivityIndicator.startAnimating()
                initialActivityIndicator.hidesWhenStopped = true
            }
        }
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        
        
        /*Old Code
         titleLabel.text = "SCHEDULE A PICKUP".localized()
         
         if Session().getInitialLoadingStaus() == true {
         initialView.isHidden = true
         } else {
         if Session().getIsUserLoginStaus() {
         initialActivityIndicator.startAnimating()
         _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.dashBoardView), userInfo: nil, repeats: false);
         } else {
         _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.showLogin), userInfo: nil, repeats: false);
         initialActivityIndicator.startAnimating()
         initialActivityIndicator.hidesWhenStopped = true
         }
         }
         self.startTimer()
         self.configIntialView()
         NavigationDrawer.sharedInstance.initialize(forViewController: self)
         */
    }
 
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        lat = String(location.latitude)
        long = String(location.longitude)
        print(lat)
        print(long)
    }
    
    func locationStatus(){
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            currentLocation = locationManager.location!
            print("Place latitude: ",currentLocation.coordinate.latitude)
            print("Place longitude: ",currentLocation.coordinate.longitude)
        }else{
            locationManager.requestWhenInUseAuthorization()
            locationStatus()
        }
    }
    
    
    func configIntialView() {
        
        proceedBtn.setTitle("PROCEED".localized(), for: .normal)
        priceList.setTitle("PRICE LIST".localized(), for: .normal)
        // minimumOrderLbl.text = "Minimum order of $200".localized()
        
        serviceArray = [
            ["service_title" : "Wash & Fold".localized(),
             "service_ID" : "1",
             "service_se_image" : "s_se_wi",
             "service_de_image" : "s_de_wi",
             "isServiceSelected" : "0"],
            ["service_title" : "Wash & Iron".localized(),
             "service_ID" : "2",
             "service_se_image" : "s_se_wf",
             "service_de_image" : "s_de_wf",
             "isServiceSelected" : "0"],
            ["service_title" : "Dry Clean".localized(),
             "service_ID" : "3",
             "service_se_image" : "s_se_dc",
             "service_de_image" : "s_de_dc",
             "isServiceSelected" : "0"],
            ["service_title" : "Speedy (24 hrs)".localized(),
             "service_ID" : "4",
             "service_se_image" : "s_se_s",
             "service_de_image" : "s_de_s",
             "isServiceSelected" : "0"]
        ]
        
        serviceTbl.delegate = self
        serviceTbl.dataSource = self
        serviceTbl.tableFooterView = UIView()
        serviceTbl!.register(UINib(nibName: pageCtrlCellIdentifier, bundle: nil), forCellReuseIdentifier: pageCtrlCellIdentifier)
        serviceTbl!.register(UINib(nibName: serviceCellIdentifier, bundle: nil), forCellReuseIdentifier: serviceCellIdentifier)
        serviceTbl!.register(UINib(nibName: selectServiceCellIdentifier, bundle: nil), forCellReuseIdentifier: selectServiceCellIdentifier)
        serviceTbl.isScrollEnabled = false
        serviceTbl.separatorStyle = .none
        
        //        if Session().getInitialLoadingStaus() == true {
        //            initialView.isHidden = true
        //            //Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SCHEDULE A PICKUP".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        //        } else {
        //            if Session().getIsUserLoginStaus() {
        //                initialActivityIndicator.startAnimating()
        //                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.dashBoardView), userInfo: nil, repeats: false);
        //            } else {
        //                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.showLogin), userInfo: nil, repeats: false);
        //                initialActivityIndicator.startAnimating()
        //                initialActivityIndicator.hidesWhenStopped = true
        //            }
        //        }
        //        self.startTimer()
        
        if deviceSize.height <= 568 {
            bottomViewheight = 40
            botomViewHeight.constant = CGFloat(bottomViewheight)
            tblBottom.constant = 35
        } else {
            bottomViewheight = 60
        }
        
        
        
    }
    
    func dashBoardView() {
        Session().setInitialLoadingStaus(true)
        initialActivityIndicator.stopAnimating()
        initialView.isHidden = true
        
        //Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SCHEDULE A PICKUP".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
    }
    
    func showLogin() {
        Session().setInitialLoadingStaus(true)
        initialActivityIndicator.stopAnimating()
        
        let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC: LoginVC = storyBooard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navController = UINavigationController(rootViewController: presentVC)
        self.present(navController, animated: true, completion: nil)
        
        
    }
    @IBAction func languageButtonTapped(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Language", preferredStyle: .actionSheet)
        
        let arabic = UIAlertAction(title: "Arabic", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.languBtn.setImage(UIImage.init(named: "us"), for: .normal)
            Helper.sharedInstance.saveSelectedLanguage(lang: "ar")
            Localize.setCurrentLanguage("ar")
            self.configIntialView()
            self.dashBoardView()
            self.titleLabel.text = "SCHEDULE A PICKUP".localized()
            self.serviceTbl.reloadData()
        })
        
        let english = UIAlertAction(title: "English", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.languBtn.setImage(UIImage.init(named: "sauthi"), for: .normal)
            
            Helper.sharedInstance.saveSelectedLanguage(lang: "en")
            Localize.setCurrentLanguage("en")
            self.configIntialView()
            self.dashBoardView()
            self.titleLabel.text = "SCHEDULE A PICKUP".localized()
            self.serviceTbl.reloadData()
        })
        
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(arabic)
        optionMenu.addAction(english)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK:- IBActions
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
    }
    
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
    /*
    @IBAction func didTapOnRateList(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
        selectedDrawerRow = 3
        Session.sharedInstance.saveSelectedServiceIDs(value: [0])
        self.navigationController?.viewControllers = [vc]
    }
    
    @IBAction func didTapOnSelectTimeSlop(_ sender: Any) {
        selectedDrawerRow = 100
        self.validateServices()
    }*/
    
    @IBAction func didTapOnService1Btn(_ sender: Any) {
        
        print((sender as AnyObject).tag)
        let selectedServiceBtn1: Int = (sender as AnyObject).tag
        var tempDict1:NSMutableDictionary = NSMutableDictionary()
        tempDict1 = serviceArray[selectedServiceBtn1] as! NSMutableDictionary
        let service1Name: String = tempDict1["service_title"] as! String
        let service1ID: String = tempDict1["service_ID"] as! String
        
        print("service1ID:----->\(service1ID)")
        print("service1Name:----->\(service1Name)")
        if selectedServiceBtn1 == 0 {
            if serviceArray[1]["isServiceSelected"] == "1" {
                Helper.sharedInstance.showAlertPopUPWithMessage("Already Selected a Service", bgColor: UIColor.msgBG(), controller: self)
                return
            } else{
                
            }
        }
        
        ///Save selected Service
        if (selectedList .contains(service1Name)) {
            selectedList.remove(service1Name)
        }else {
            selectedList.add(service1Name)
        }
        
        ///Save selected IDs
        if (selectedServiceIDList .contains(service1ID)) {
            selectedServiceIDList.remove(service1ID)
        } else {
            selectedServiceIDList.add(service1ID)
            print(selectedServiceIDList)
        }
        
        
        if serviceArray[selectedServiceBtn1]["isServiceSelected"] == "1" {
            serviceArray[selectedServiceBtn1]["isServiceSelected"] = "0"
        } else {
            serviceArray[selectedServiceBtn1]["isServiceSelected"] = "1"
        }
        
        if (orderPlaceInfo.serviceNames.contains(service1Name)) {
            
        } else {
            if (orderPlaceInfo.serviceNames.characters.count > 0) {
                orderPlaceInfo.serviceNames = orderPlaceInfo.serviceNames + "," + service1Name
                orderPlaceInfo.serviceIDs = orderPlaceInfo.serviceIDs + "," + service1ID
            } else {
                orderPlaceInfo.serviceNames = orderPlaceInfo.serviceIDs + "" + service1Name
                orderPlaceInfo.serviceIDs = orderPlaceInfo.serviceIDs + "" + service1ID
            }
        }
        
        serviceTbl.reloadData()
    }
    
    @IBAction func didTapOnService2Btn(_ sender: Any) {
        
        let selectedServiceBtn2: Int = (sender as AnyObject).tag
        
        var tempDict1:NSMutableDictionary = NSMutableDictionary()
        tempDict1 = serviceArray[selectedServiceBtn2] as! NSMutableDictionary
        let service1Name: String = tempDict1["service_title"] as! String
        let service1ID: String = tempDict1["service_ID"] as! String
        
        print("service1ID:----->\(service1ID)")
        print("service1Name:----->\(service1Name)")
        
        if selectedServiceBtn2 == 1 {
            if serviceArray[0]["isServiceSelected"] == "1" {
                Helper.sharedInstance.showAlertPopUPWithMessage("Already Selected a Service", bgColor: UIColor.msgBG(), controller: self)
                return
            } else{
            }
        }
        
        ///Save selected services
        if (selectedList .contains(service1Name)) {
            selectedList.remove(service1Name)
        } else {
            selectedList.add(service1Name)
            print(selectedList)
        }
        
        ///Save selected IDs
        if (selectedServiceIDList .contains(service1ID)) {
            selectedServiceIDList.remove(service1ID)
        } else {
            selectedServiceIDList.add(service1ID)
            print(selectedServiceIDList)
        }
        
        if serviceArray[selectedServiceBtn2]["isServiceSelected"] == "1" {
            serviceArray[selectedServiceBtn2]["isServiceSelected"] = "0"
        } else {
            serviceArray[selectedServiceBtn2]["isServiceSelected"] = "1"
        }
        
        if (orderPlaceInfo.serviceNames.contains(service1Name)) {
        } else {
            if (selectedList.count > 0) {
                orderPlaceInfo.serviceNames = orderPlaceInfo.serviceNames + "," + service1Name
                orderPlaceInfo.serviceIDs = orderPlaceInfo.serviceIDs + "," + service1ID
            } else {
                orderPlaceInfo.serviceNames = orderPlaceInfo.serviceNames + "" + service1Name
                orderPlaceInfo.serviceIDs = orderPlaceInfo.serviceIDs + "" + service1ID
            }
        }
        
        serviceTbl.reloadData()
    }
    
    func clearPreviousOrderDetails() {
        Session().savePickupDate(value: "")
        Session().saveDropDate(value: "")
    }
    
    
    /*
    func validateServices() {
        
        self.clearPreviousOrderDetails()
        
        Session.sharedInstance.saveSelectedService(value: selectedList)
        
        Session.sharedInstance.saveSelectedServiceIDs(value: selectedServiceIDList)
        
        print("saveSelectedService:----->\(Session.sharedInstance.savedSelectedService())")
        print("saveSelectedService IDs:----->\(Session.sharedInstance.savedSelectedServiceIDs())")
        
        let ids = Session.sharedInstance.savedSelectedServiceIDs()
        print(ids)
        
        var test = ""
        
        for id in ids {
            test = id as! String
            print(test)
        }
        
        if test == "1"{
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }else if test == "2"{
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }
//        else if test == "4"{ // Extra only
//            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Service", bgColor: UIColor.msgBG(), controller: self)
//        }else if test == "3"{ // Extra only
//            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Service", bgColor: UIColor.msgBG(), controller: self)
//        }
        else if test == "1" && test == "4" || test == "3" {
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }else if test == "1" && test == "3" || test == "4" {
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }
        else if test == "2" && test == "3" || test == "4"{
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }
        else if test == "2" && test == "4" || test == "3"{
            print("Ok")
            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
            priceListVC.orderPlaceInfo = orderPlaceInfo
            self.navigationController?.viewControllers = [priceListVC]
        }
        else {
            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Service", bgColor: UIColor.msgBG(), controller: self)
        }
//        if (Session.sharedInstance.savedSelectedServiceIDs().count) > 0 {
//            let priceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
//            priceListVC.orderPlaceInfo = orderPlaceInfo
//            self.navigationController?.viewControllers = [priceListVC]
//            
//        } else {
//            Helper.sharedInstance.showAlertPopUPWithMessage("Select Your Service", bgColor: UIColor.msgBG(), controller: self)
//        }
        
    }
    
    */
    
    
    //MARK:- TableView Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.callTableViewCell1(indexPath: indexPath)
        case 1:
            return self.callSelectServiceCell(indexPath: indexPath)
        case 3:
            return self.callSelectServiceCell(indexPath: indexPath)
        default:
            return self.callTableViewCell2(indexPath: indexPath)
        }
    }
    
    func callTableViewCell1(indexPath:IndexPath) -> PageCtrlCell {
        let userActivityCell: PageCtrlCell = serviceTbl.dequeueReusableCell(withIdentifier: pageCtrlCellIdentifier, for: indexPath) as! PageCtrlCell
        
        userActivityCell.serivcePageCtrl.register(UINib(nibName: collectionIdentifier, bundle:nil), forCellWithReuseIdentifier: collectionIdentifier)
        if deviceSize.height > 568 {
            userActivityCell.collectionHeight.constant = 180
        }
        userActivityCell.pageCtrl.numberOfPages = arrayCount
        userActivityCell.serivcePageCtrl.delegate = self
        userActivityCell.serivcePageCtrl.dataSource = self
        userActivityCell.selectionStyle = .none
        return userActivityCell
    }
    
    func callSelectServiceCell(indexPath:IndexPath) -> SelectServiceCell {
        let userActivityCell: SelectServiceCell = serviceTbl.dequeueReusableCell(withIdentifier: selectServiceCellIdentifier, for: indexPath) as! SelectServiceCell
        userActivityCell.selectionStyle = .none
        if deviceSize.height <= 568 {
            userActivityCell.selectServiceHeight.constant = 30
        }
        userActivityCell.selectService.text = "Select Services".localized()
        if indexPath.row == 3 {
            //            userActivityCell.selectServiceHeight.constant = 33
            userActivityCell.selectService.text = "Extra Services".localized()
        }
        return userActivityCell
    }
    
    func callTableViewCell2(indexPath:IndexPath) -> HomeScreenServiceCell
    {
        let userActivityCell: HomeScreenServiceCell = serviceTbl.dequeueReusableCell(withIdentifier: serviceCellIdentifier, for: indexPath) as! HomeScreenServiceCell
        userActivityCell.serviceViewWidth.constant = deviceSize.width / 2
        userActivityCell.selectionStyle = .none
        if deviceSize.height > 568 {
            userActivityCell.service1Height.constant = (deviceSize.height - ((64 + 180 + 40 + 30  + 50))) / 2
            userActivityCell.service2Height.constant = (deviceSize.height - ((64 + 180 + 40 + 30  + 50))) / 2
        } else {
            userActivityCell.service1Height.constant = (deviceSize.height - ((64 + 180 + 30 + 20  + 40))) / 2
            userActivityCell.service2Height.constant = (deviceSize.height - ((64 + 180 + 30 + 20  + 40))) / 2
        }
        
        
        userActivityCell.service1Btn.addTarget(self, action: #selector(didTapOnService1Btn(_:)), for: .touchUpInside)
        userActivityCell.service2Btn.addTarget(self, action: #selector(didTapOnService2Btn(_:)), for: .touchUpInside)
        
        //print("indexPath.row:------>\(indexPath.row)")
        var tempDict1:NSMutableDictionary = NSMutableDictionary()
        var tempDict2:NSMutableDictionary = NSMutableDictionary()
        
        switch indexPath.row {
        case 2:
            userActivityCell.service1Btn.tag = indexPath.row - 2
            userActivityCell.service2Btn.tag = indexPath.row - 1
            tempDict1 = serviceArray[0] as! NSMutableDictionary
            tempDict2 = serviceArray[1] as! NSMutableDictionary
        default:
            userActivityCell.service1Btn.tag = indexPath.row - 2
            userActivityCell.service2Btn.tag = indexPath.row - 1
            tempDict1 = serviceArray[2] as! NSMutableDictionary
            tempDict2 = serviceArray[3] as! NSMutableDictionary
        }
        
        
        let service1Name: String = tempDict1["service_title"] as! String
        let service2Name: String = tempDict2["service_title"] as! String
        
        let selectedImg1: String = tempDict1["service_se_image"] as! String
        let selectedImg2: String = tempDict2["service_se_image"] as! String
        
        let deSelectedImg1: String = tempDict1["service_de_image"] as! String
        let deSelectedImg2: String = tempDict2["service_de_image"] as! String
        
        let isServiceSelected1: String = tempDict1["isServiceSelected"] as! String
        let isServiceSelected2: String = tempDict2["isServiceSelected"] as! String
        
        
        userActivityCell.service1Lbl.text = service1Name
        userActivityCell.service2Lbl.text = service2Name
        
        switch isServiceSelected1 {
        case "1":
            userActivityCell.service1Img.image = UIImage.init(named: selectedImg1)
            userActivityCell.service2Img.image = UIImage.init(named: deSelectedImg2)
            
            
        default:
            userActivityCell.service1Img.image = UIImage.init(named: deSelectedImg1)
            
        }
        
        switch isServiceSelected2 {
        case "1":
            userActivityCell.service2Img.image = UIImage.init(named: selectedImg2)
            
        default:
            userActivityCell.service2Img.image = UIImage.init(named: deSelectedImg2)
            
        }
        
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            // Code for iPhone 4 & iPhone 5
            userActivityCell.serviceBtn1Width.constant = 73
            userActivityCell.serviceBtn1Height.constant = 73
            userActivityCell.serviceBtn2Width.constant = 73
            userActivityCell.serviceBtn2Height.constant = 73
            break
        case .iPhone6:
            // Code for iPhone 6 & iPhone 7
            break
        case .iPhone6Plus:
            // Code for iPhone 6 Plus & iPhone 7 Plus
            userActivityCell.serviceBtn1Width.constant = 130
            userActivityCell.serviceBtn1Height.constant = 130
            userActivityCell.serviceBtn2Width.constant = 130
            userActivityCell.serviceBtn2Height.constant = 130
            break
        default:
            break
        }
        
        return userActivityCell
    }
    
    // MARK: - CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCount
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let postsCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionIdentifier, for: indexPath) as! ServiceCollectionCell
        
        postsCollectionCell.serviceImages.image = UIImage.init(named: String(format: "%d", (indexPath.row+1)))
        
        if indexPath.row % 2 == 0 {
            postsCollectionCell.backgroundColor = UIColor.lightGray
        } else {
            postsCollectionCell.backgroundColor = UIColor.darkGray
        }
        
        return postsCollectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : self.view.frame.width , height : 277.5)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let path = IndexPath(row: 0, section: 0)
        let cell = serviceTbl.cellForRow(at: path) as! PageCtrlCell
        let x = cell.serivcePageCtrl.contentOffset.x
        let w = cell.serivcePageCtrl.bounds.size.width
        let currentPage = Int(ceil(x/w))
        cell.pageCtrl.currentPage = currentPage
        if isLastPage{
            let index = IndexPath(row:0 , section : 0 )
            cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: false)
            cell.pageCtrl.currentPage = 0
            isLastPage = false
            return
        }
        
        if currentPage == arrayCount - 1{
            isLastPage = true
        }
    }
    
    func scrollToNextCell(){
        let path = IndexPath(row: 0, section: 0)
        let cell = serviceTbl.cellForRow(at: path) as! PageCtrlCell
        let x = cell.serivcePageCtrl.contentOffset.x
        let w = cell.serivcePageCtrl.bounds.size.width
        let currentPage = Int(ceil(x/w))
        
        if currentPage == arrayCount - 1{
            isLastPage = true
            let index = IndexPath(row:0 , section : 0 )
            cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: false)
            cell.pageCtrl.currentPage = 0
            isLastPage = false
            return
        }
        let index = IndexPath(row: currentPage + 1 , section : 0 )
        cell.serivcePageCtrl.scrollToItem(at: index, at: .right, animated: true)
        cell.pageCtrl.currentPage = currentPage + 1
        
    }
    
    func startTimer() {
        
        _ = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(HomeVC.scrollToNextCell), userInfo: nil, repeats: true);
    }
    
    func callGetProductSuggestionAPI(searchText: String) {
        params = [ "deviceId" : "62c7c656567" as AnyObject,
                   "searchKey" : searchText as AnyObject]
        HttpManager().loadPostMethod(path: API_GET_PRODUCT_SUGGEST, params: self.params as Dictionary<String, AnyObject>, controller: self) {
            (response) in
            print("Response => \(response)")
        }
    }
    
}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

public extension UIDevice {
    
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case Unknown
    }
    var screenType: ScreenType {
        guard iPhone else { return .Unknown}
        switch UIScreen.main.nativeBounds.height  {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        default:
            return .Unknown
        }
    }
    
}

