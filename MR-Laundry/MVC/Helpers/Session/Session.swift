//
//  Session.swift
//  MR-Laundry
//
//  Created by LocusTech on 14/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class Session: NSObject {
    
    
    
    var userDefaults :UserDefaults
    
    class var sharedInstance: Session {
        struct Static {
            static let instance = Session()
        }
        return Static.instance
    }
    
    override init() {
        self.userDefaults = UserDefaults()
    }
    
    func saveValue() {
        self.userDefaults.synchronize()
    }
}

// MARK: - Sample Session Methods
extension Session {
    
    // MARK: Default Format
    func setString (_ value: NSString) {
        self.userDefaults.set(value, forKey: "String")
        self.saveValue()
    }
    
    func getString ()-> NSString {
        var value: NSString? = self.userDefaults.object(forKey: "String") as? NSString
                if Helper.sharedInstance.isNilOrEmpty(value) {
                    value = ""
                }
        return value!
    }
    
    // MARK: Int Value
    func setInt (_ value: Int) {
        self.userDefaults.set(value, forKey: "Int")
        self.saveValue()
    }
    
    func getInt ()-> Int {
        
        return self.userDefaults.integer(forKey: "Int")
    }
    
    // MARK: Bool Value
    func setBool (_ value: Bool) {
        self.userDefaults.set(value, forKey: "Bool")
        self.saveValue()
    }
    
    func getBool ()-> Bool {
        return self.userDefaults.bool(forKey: "Bool")
    }
    
    func setIsUserLoginStaus (_ value: Bool) {
        self.userDefaults.set(value, forKey: "userLoginStatus")
        self.saveValue()
    }
    
    func getIsUserLoginStaus ()-> Bool {
        return self.userDefaults.bool(forKey: "userLoginStatus")
    }
    
    func setInitialLoadingStaus (_ value: Bool) {
        self.userDefaults.set(value, forKey: "initailLoading")
        self.saveValue()
    }
    
    func getInitialLoadingStaus ()-> Bool {
        return self.userDefaults.bool(forKey: "initailLoading")
    }
    
    // MARK: MutableArray value
    func setMutableArray (_ value: NSMutableArray) {
        self.userDefaults.set(value, forKey: "MutableArray")
        self.saveValue()
    }
    
    func getMutableArray ()-> NSMutableArray {
        
        var array = self.userDefaults.object(forKey: "MutableArray")
        
        if (array! as AnyObject).count == 0 {
            array = NSMutableArray() as [AnyObject]
        }
        
        return array as! NSMutableArray
    }
    
    
    
    func setNSDictionary(_ value:NSDictionary) {
        self.userDefaults.set(value, forKey: "NSDictionary")
        self.saveValue()
    }
    
    func getNSDictionary () -> NSDictionary {
        var value = self.userDefaults.object(forKey: "NSDictionary")
        if (value == nil) {
            value = NSDictionary()
        }
        return value as! NSDictionary
    }
    
    // MARK: String Value
    func setUserLoginDetails (_ value: NSDictionary) {
        self.userDefaults.set(value, forKey: "userLoginDetails")
        self.saveValue()
    }
    
    func getUserLoginDetails ()-> NSDictionary {
        var value = self.userDefaults.object(forKey: "userLoginDetails")
        if (value == nil) {
            value = NSDictionary()
        }
        return value as! NSDictionary
    }
    
    func saveOrderList(value : NSDictionary) -> Void{
        self.userDefaults.set(value, forKey: "OrderList")
        self.saveValue()
    }
    func getOrderList() -> NSDictionary{
        let value = UserDefaults.standard.object(forKey: "OrderList")
        
        if value != nil {
            return value as! NSDictionary
        }
        return [:]
    }
    func saveSelectedService(value : NSArray) -> Void{
        self.userDefaults.set(value, forKey: "SelectedServiceHome")
        self.saveValue()
    }
    func savedSelectedService() -> NSArray{
        let value = UserDefaults.standard.object(forKey: "SelectedServiceHome")
        
        if value != nil {
            return value as! NSArray
        }
        return []
    }
    
    func saveSelectedServiceIDs(value : NSArray) -> Void{
        self.userDefaults.set(value, forKey: "SelectedServiceIDs")
        self.saveValue()
    }
    func savedSelectedServiceIDs() -> NSArray{
        let value = UserDefaults.standard.object(forKey: "SelectedServiceIDs")
        
        if value != nil {
            return value as! NSArray
        }
        return []
    }

    func saveStoreID(value : String) -> Void{
        self.userDefaults.set(value, forKey: "StoreId")
        self.saveValue()
    }
    func savedStoreID() -> String{
        let value = UserDefaults.standard.value(forKey: "StoreId")
        
        if value != nil {
            return value as! String
        }
        return ""
    }
    //Save pickup date
    func savePickupDate(value : String) -> Void{
        self.userDefaults.set(value, forKey: "PickupDate")
        self.saveValue()
    }
    func savedPickupDate() -> String{
        let value = UserDefaults.standard.value(forKey: "PickupDate")
        
        if value != nil {
            return value as! String
        }
        return ""
    }
    
    //Save drop date
    func saveDropDate(value : String) -> Void{
        self.userDefaults.set(value, forKey: "DropDate")
        self.saveValue()
    }
    func savedDropDate() -> String{
        let value = UserDefaults.standard.value(forKey: "DropDate")
        
        if value != nil {
            return value as! String
        }
        return ""
    }
    //Save pickup Time
    func savePickupTime(value : String) -> Void{
        self.userDefaults.set(value, forKey: "PickupTime")
        self.saveValue()
    }
    func savedPickupTime() -> String{
        let value = UserDefaults.standard.value(forKey: "PickupTime")
        
        if value != nil {
            return value as! String
        }
        return ""
    }
    
    //Save pickup Time
    func saveDropTime(value : String) -> Void{
        self.userDefaults.set(value, forKey: "DropTime")
        self.saveValue()
    }
    func savedDropTime() -> String{
        let value = UserDefaults.standard.value(forKey: "DropTime")
        
        if value != nil {
            return value as! String
        }
        return ""
    }





    // MARK: String Value

    func setAddressDetails (value: NSArray) {
        
//        UserDefaults.standard.set(value, forKey: "addAddressDetails")
//        let result = UserDefaults.standard.value(forKey: "addAddressDetails")
//        print(result!)
//        userDefaults.synchronize()
        
        self.userDefaults.set(value, forKey: "addAddressDetails")
        self.saveValue()
    }
    
    func getAddressDetails ()-> NSArray {
        var value = self.userDefaults.object(forKey: "addAddressDetails")
        if (value == nil) {
            value = NSArray()
        }
        return value as! NSArray
    }

    func saveTotalPrice(value : NSString) -> Void{
        self.userDefaults.set(value, forKey: "totalPrice")
        self.saveValue()
    }
    func getTotalPrice() -> NSString{
        var value = UserDefaults.standard.object(forKey: "totalPrice")
        if (value == nil) {
            value = NSString()
        }

        return value as! NSString
    }
    
    func saveSelectedServiceTitle(value : NSMutableArray) -> Void{
        self.userDefaults.set(value, forKey: "selectedService")
        self.saveValue()
    }
    func getSelectedServiceTitle() -> NSArray{
        var value = UserDefaults.standard.object(forKey: "selectedService")
        if (value == nil) {
            value = NSArray()
        }
        return value as! NSArray
    }
    //Save Booking Array (Selected Array)
    func saveSelectedObjects(value : NSMutableArray) -> Void{
        self.userDefaults.set(value, forKey: "SelectedObjects")
        self.saveValue()
    }
    func getSelectedObjects() -> NSArray{
        var value = UserDefaults.standard.object(forKey: "SelectedObjects")
        if (value == nil) {
            value = NSArray()
        }
        return value as! NSArray
    }
}


