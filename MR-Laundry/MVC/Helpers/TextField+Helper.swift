//
//  TextField+Helper.swift
//  MR-Laundry
//
//  Created by LocusTech on 02/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setTextFieldPadding () {
        
//        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 23.5))
//        let passwprdButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 25, height: 20))
//
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func setTextFieldProperties(txtField:UITextField, placehodercolor:UIColor, placehoderText:NSString, txtcolor:UIColor, title:NSString)  {
        
        let textField = txtField
        textField.attributedPlaceholder = NSAttributedString(string:placehoderText as String,
                                                             attributes:[NSForegroundColorAttributeName: placehodercolor])
        textField.text = title as String
        textField.textColor = txtcolor
        textField.autocorrectionType = .no
        textField .setTextFieldPadding()
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        
    }
    
    func setAddBusinessTextFieldProperties(txtField:UITextField, placehodercolor:UIColor, placehoderText:NSString, txtcolor:UIColor, title:NSString)  {
        
        let textField = txtField
        
        var myMutableStringTitle = NSMutableAttributedString()
        let Name:String  = placehoderText as String
        
//        myMutableStringTitle = NSMutableAttributedString(string:Name as String, attributes: [NSFontAttributeName:UIFont(name: "Roboto-Regular", size: 18.0)!]) // Font
        myMutableStringTitle = NSMutableAttributedString(string: Name as String, attributes: [NSFontAttributeName: UIFont.setAppFontRegular(size: 18)]) // Font

        myMutableStringTitle.addAttribute(NSForegroundColorAttributeName, value: placehodercolor, range:NSRange(location:0,length:Name.characters.count))    // Color
        textField.attributedPlaceholder = myMutableStringTitle
        
        
        textField.text = title as String
        textField.textColor = txtcolor
        textField.autocorrectionType = .no
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        
    }
    
    
    func setTextFieldPaddingwithimage(lefticonName:NSString, righticonName:NSString) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 40, width: 100, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
        if lefticonName.length != 0 {
            self.leftViewMode = UITextFieldViewMode.always
            self.leftView = UIImageView(image: UIImage(named: lefticonName as String))
            
        }
        if righticonName.length != 0 {
            self.leftViewMode = UITextFieldViewMode.always
            self.leftView = UIImageView(image: UIImage(named: righticonName as String))
            
        }
    }
}
