//
//  HttpManager.swift
//  MR-Laundry
//
//  Created by LocusTech on 14/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SystemConfiguration
import Kingfisher


class HttpManager: NSObject {
    
    var request: NSMutableURLRequest
    
    override init() {
        
        self.request = NSMutableURLRequest()
        self.request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        self.request.addValue(API_AUTHORIZATION_TOKEN, forHTTPHeaderField: "Authorization")
    }
    
    func isInternetAvailable() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    // MARK: - API Calls
    
    func getAPICall(path: String, params: [String: AnyObject], controller: UIViewController, completion : @escaping (_ response:Dictionary<String, AnyObject>) -> Void) {
        
        if !self.isInternetAvailable() {
            
           // Helper().errorAlert(controller: controller,message: "No Internet Connection" , viewIndex: 3 )
            return
        }
        
        showLoading(controller: controller)
        
        let parameterString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(API_BASE_URL)\(path)?\(parameterString)")!
        
        self.request.url = requestURL
        
        print("REQUEST : \(API_BASE_URL) \(path) \(parameterString)" as Any)
        
        let task = URLSession.shared.dataTask(with: self.request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                hideLoading(controller: controller)
                
                if (error == nil) {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                        let responseDict: Dictionary<String, AnyObject> = jsonData as! Dictionary
                        
                        print("Response: \(responseDict)" as Any)
                        
                        if (responseDict["success"] as! Bool == true) {
                            
                            completion(responseDict)
                        }
                        else {
                            
                            //Helper().errorAlert(controller: controller,message: "Invalid data" , viewIndex: 3 )
                        }
                    }
                    catch let error {
                        
                        print("ERROR JSONSerialization : \(error)")
                    }
                }
                else {
                    
                   // Helper().errorAlert(controller: controller,message: "Could not connect to server" , viewIndex: 3 )
                }
            }
        })
        
        task.resume()
    }
    
    func loadPostMethod(path: String, params: Dictionary<String, AnyObject>, controller: UIViewController, completion: @escaping (_ response: Dictionary<String, AnyObject>) -> Void) {
        
        if !self.isInternetAvailable() {
            
           // Helper().errorAlert(controller: controller,message: "No Internet Connection" , viewIndex: 3 )
            return
        }
        
        showLoading(controller: controller)
        
        self.request.url = NSURL(string: API_BASE_URL + path)! as URL
        self.request.httpMethod = "POST"
        self.request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        print("REQUEST : \(API_BASE_URL) \(path) \(params)" as Any)
        
        let task = URLSession.shared.dataTask(with: self.request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                hideLoading(controller: controller)
                
                if (error == nil) {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                        let responseDict: Dictionary<String, AnyObject> = jsonData as! Dictionary
                        
                        print("Response: \(responseDict)" as Any)
                        
                        if (responseDict["success"] as! Bool == true) {
                            
                            completion(responseDict)
                        }
                        else {
                            
                          //  Helper().errorAlert(controller: controller,message: "Invalid data" , viewIndex: 3 )
                        }
                    }
                    catch let error {
                        
                        print("ERROR JSONSerialization : \(error)")
                    }
                }
                else {
                    
                   // Helper().errorAlert(controller: controller,message: "Could not connect to server" , viewIndex: 3 )
                }
            }
        })
        
        task.resume()
    }
    
  //For filter
    func loadPostMethodForFilter(path: String, params: Dictionary<String, AnyObject>, view: UIView, completion: @escaping (_ response: Dictionary<String, AnyObject>) -> Void) {
        
        if !self.isInternetAvailable() {
            
            // Helper().errorAlert(controller: controller,message: "No Internet Connection" , viewIndex: 3 )
            return
        }
        
        //showLoading(controller: controller)
        showLoadingFilter(view: view)
        
        self.request.url = NSURL(string: API_BASE_URL + path)! as URL
        self.request.httpMethod = "POST"
        self.request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        print("REQUEST : \(API_BASE_URL) \(path) \(params)" as Any)
        
        let task = URLSession.shared.dataTask(with: self.request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                hideLoadingFilter(View: view)
                
                if (error == nil) {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                        let responseDict: Dictionary<String, AnyObject> = jsonData as! Dictionary
                        
                        print("Response: \(responseDict)" as Any)
                        
                        if (responseDict["success"] as! Bool == true) {
                            
                            completion(responseDict)
                        }
                        else {
                            
                            //  Helper().errorAlert(controller: controller,message: "Invalid data" , viewIndex: 3 )
                        }
                    }
                    catch let error {
                        
                        print("ERROR JSONSerialization : \(error)")
                    }
                }
                else {
                    
                    // Helper().errorAlert(controller: controller,message: "Could not connect to server" , viewIndex: 3 )
                }
            }
        })
        
        task.resume()
    }

    
    
    
    // MARK: - Load Image from URL
    
    func loadImage(url: URL, imageView: UIImageView) {
        
        /*let modifier = AnyModifier { request in
            
            var req = request
            
            req.addValue("application/json", forHTTPHeaderField: "Content-Type")
            req.addValue(API_AUTHORIZATION_TOKEN, forHTTPHeaderField: "Authorization")
            
            return req
        }*/
        
        //imageView.kf.setImage(with: url, placeholder: nil, options: [.requestModifier(modifier)])
        
        imageView.kf.setImage(with: url, placeholder: nil, options: nil)
        
        /*imageView.kf.setImage(with: url, placeholder: nil, options: [.transition(ImageTransition.fade(1)), .requestModifier(modifier)], progressBlock: { (receivedSize, totalSize) in
         
         // print("\(indexPath.row + 1) : \(receivedSize)/\(totalSize)")
         
         }) { (image, error, cacheType, imageURL) in
         
         // print("\(indexPath.row + 1) : Finished")
         }*/
    }
}

// MARK: - Private Methods

func showLoading(controller: UIViewController) {
    
    if LoadingOverlay.shared.isLoading == false {
        
        LoadingOverlay.shared.showOverlay(controller.view)
    }
}

func hideLoading(controller: UIViewController) {
    
    if LoadingOverlay.shared.isLoading == true {
        
        LoadingOverlay.shared.hideOverlayView()
    }
    
}

func showLoadingFilter(view: UIView) {
    
    if LoadingOverlay.shared.isLoading == false {
        
        LoadingOverlay.shared.showOverlay(view)
    }
}

func hideLoadingFilter(View: UIView) {
    
    if LoadingOverlay.shared.isLoading == true {
        
        LoadingOverlay.shared.hideOverlayView()
    }
    
}





extension Dictionary {
    
    func stringFromHttpParameters() -> String {
        
        let parameterArray = self.map { (key, value) -> String in
            
            let percentEscapedKey = (key as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let percentEscapedValue = (value as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}







