//
//  ProfileVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 04/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet var logOutBtn: UIButton!
    
    @IBOutlet weak var signInProfileTitle: UILabel!
    @IBOutlet weak var phoneTitle: UILabel!
    @IBOutlet weak var phoneNoField: UILabel!
    @IBOutlet weak var myDetails: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameValueLabel: UILabel!
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var emailValueLabel: UILabel!
    var userDetailsInfo : MLUserModel!
    @IBOutlet var profileAddressTbl: UITableView!
    let profileAddressCellIdentifier = "ProfileAddressCell"
    var addressListArray = NSArray()
    @IBOutlet var userProfileImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"PROFILE".localized() as NSString, isRightBtnImg: false, rightBtnName: "Log Out", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        self.configInitailView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        //Log out title
        
        userDetailsInfo = DemoGlobalData.sharedInstance.userDetailInfo

        
        logOutBtn.setTitle("LOG OUT".localized(), for: .normal)
        signInProfileTitle.text = "USER DETAILS".localized()
        phoneTitle.text = "Phone No:".localized()
        //myDetails.text = "MY DETAILS".localized()
        nameLabel.text = "User Name:".localized()
        emailTitleLabel.text = "Email Address:".localized()
        emailValueLabel.text = (Session.sharedInstance.getUserLoginDetails()["userEmail"] as? String)!
        nameValueLabel.text = (Session.sharedInstance.getUserLoginDetails()["userName"] as? String)!
        phoneNoField.text = (Session.sharedInstance.getUserLoginDetails()["telephoneCode"] as? String)! + " " + (Session.sharedInstance.getUserLoginDetails()["mobileNumber"] as? String)!
        
        userProfileImg.layer.cornerRadius = userProfileImg.frame.size.width / 2
        userProfileImg.clipsToBounds = true
        userProfileImg.layer.borderWidth = 2.0
        userProfileImg.layer.borderColor = UIColor.white.cgColor
       
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
        self.moveToLogin()
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func configInitailView() {
        logOutBtn.layer.cornerRadius = 20.0
        logOutBtn.layer.borderColor = UIColor.white.cgColor
        logOutBtn.layer.borderWidth = 1
        logOutBtn.clipsToBounds = true
        
        addressListArray = [
            ["title" : "Home",
             "value" : "3/592, Thirumagal Nagar, Perur Chettipalayam, Coimbatore -641 010."],
            ["title" : "Office",
             "value" : "3/592, Thirumagal Nagar, Perur Chettipalayam, Coimbatore -641 010."]
        ]
        
        profileAddressTbl.delegate = self
        profileAddressTbl.dataSource = self
        profileAddressTbl.backgroundColor = UIColor.clear
        profileAddressTbl.tableFooterView = UIView()
        profileAddressTbl.separatorStyle = .none
        profileAddressTbl!.register(UINib(nibName: profileAddressCellIdentifier, bundle: nil), forCellReuseIdentifier: profileAddressCellIdentifier)
        
        

    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    func moveToLogin() {
        Session().setIsUserLoginStaus(false)
        let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC: SliderViewController = storyBooard.instantiateViewController(withIdentifier: "SliderViewController") as! SliderViewController
        let navController = UINavigationController(rootViewController: presentVC)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func didTapOnLogOut(_ sender: Any) {
        Session().setIsUserLoginStaus(false)
    }
    
  
    func callAddressListCell(indexPath:IndexPath) -> ProfileAddressCell {
        let addressListCell:ProfileAddressCell = profileAddressTbl.dequeueReusableCell(withIdentifier:profileAddressCellIdentifier, for: indexPath) as! ProfileAddressCell
        addressListCell.selectionStyle = .none
        
        
        let tempDict = addressListArray[indexPath.row] as! NSDictionary
        
        addressListCell.addressTitleLbl.text = tempDict.isHaveStringValue("title")
        addressListCell.addressLbl.text = tempDict.isHaveStringValue("value")
        if (addressListArray.count - 1) == indexPath.row {
            addressListCell.sepratorLeading.constant = 0
        } else {
            addressListCell.sepratorLeading.constant = 20
        }
        return addressListCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return callAddressListCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
