//
//  SelectLaundryVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire


class SelectLaundryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var laundryShopTbl: UITableView!
    var laundryShopList:NSArray = []
    let laundryShopCellIdentifier = "SelectLaundryCell"
    var orderPlaceInfo = MLOrderPlaceModel()
    var selectLaundryInfo = MLSelectLaundryModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configInitallView()
        self.callSelectLaundryAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
        // self.moveToTimeSlot()
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    
    
    func configInitallView() {
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SELECT LAUNDRY".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        
        /*    laundryShopList = [
         ["title" : "Floris Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 5.00],
         
         ["title" : "Dubai Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 0.0],
         
         ["title" : "The Laundry Hub".localized(),
         "laundry_image" : "http://sentonmission.com/wp-content/uploads/2013/02/laundryHub.jpg",
         "laundry_rating" : 2.0],
         
         ["title" : "Dima Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 4.0],
         
         ["title" : "Dubai Dry Cleaning Service".localized(),
         "laundry_image" : "http://images.locanto.ae/2078501176/Express-Dry-Cleaning-service-Eastern-Rose-Laundry_1.jpg",
         "laundry_rating" : 3.5],
         
         ["title" : "Diva Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 2.0],
         
         ["title" : "SUDS R US Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 1.5],
         
         ["title" : "FRESH N CLEAN".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 1.0],
         
         ["title" : "Dubai Dry Cleaning Service".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 0.5],
         
         ["title" : "Monarch Laundry And Dry Cleaning".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 5.0],
         
         
         ["title" : "Floris Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 4.5],
         
         ["title" : "Dubai Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 4.0],
         
         ["title" : "The Laundry Hub".localized(),
         "laundry_image" : "http://sentonmission.com/wp-content/uploads/2013/02/laundryHub.jpg",
         "laundry_rating" : 3.5],
         
         ["title" : "Dima Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 3.5],
         
         ["title" : "Dubai Dry Cleaning Service".localized(),
         "laundry_image" : "http://images.locanto.ae/2078501176/Express-Dry-Cleaning-service-Eastern-Rose-Laundry_1.jpg",
         "laundry_rating" : 2.0],
         
         ["title" : "Diva Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 1.0],
         
         ["title" : "SUDS R US Laundry".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 1.5],
         
         ["title" : "FRESH N CLEAN".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 2.0],
         
         ["title" : "Dubai Dry Cleaning Service".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 4.5],
         
         ["title" : "Monarch Laundry And Dry Cleaning".localized(),
         "laundry_image" : "http://www.yello.ae/img/ae/g/1419863345_80257.png",
         "laundry_rating" : 3.5],
         ]
         */
        laundryShopTbl.delegate = self
        laundryShopTbl.dataSource = self
        laundryShopTbl.tableFooterView = UIView()
        laundryShopTbl!.register(UINib(nibName: laundryShopCellIdentifier, bundle: nil), forCellReuseIdentifier: laundryShopCellIdentifier)
        
    }
    
    //MARK: - UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = Bundle.main.loadNibNamed("PriceListSessionHeaderView", owner: nil, options: nil)![0] as! PriceListSessionHeaderView
        headerCell.headerTitleLbl.text = "Select Laundry Nears You".localized()
        headerCell.backgroundColor = UIColor.deSelectedCatrgoryBG()
        return headerCell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laundryShopList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return callLaundryCell(indexPath: indexPath)
    }
    
    func callLaundryCell(indexPath:IndexPath) -> SelectLaundryCell {
        let laundyShopCell:SelectLaundryCell = laundryShopTbl.dequeueReusableCell(withIdentifier: laundryShopCellIdentifier, for: indexPath) as! SelectLaundryCell
        
        let tempDict:NSDictionary = laundryShopList[indexPath.row] as! NSDictionary
        
        print(tempDict)
        
        let laundryTitle = tempDict.value(forKey: "store_name") as! String
        let laundryImageURL = tempDict.value(forKey: "store_logo") as! String
        var laundryRatingCount = tempDict.value(forKey: "store_ratings") as! String
        
        if laundryRatingCount.characters.count == 0 {
            laundryRatingCount = "0"
        }
        
        URLSession.shared.dataTask(with: NSURL(string: laundryImageURL)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                laundyShopCell.shopimage.image = image
            })
            
        }).resume()
        
        laundyShopCell.selectLaundrBtn.layer.cornerRadius = 16.0
        laundyShopCell.clipsToBounds = true
        
        laundyShopCell.laundryNameLbl.text = laundryTitle
        laundyShopCell.selectLaundrBtn.tag = indexPath.row
        laundyShopCell.selectLaundrBtn.addTarget(self, action: #selector(didTapOnSelectLaundry(_:)), for: .touchUpInside)
        laundyShopCell.selectionStyle = .none
        
        laundyShopCell.preservesSuperviewLayoutMargins = false
        laundyShopCell.separatorInset = .zero
        laundyShopCell.layoutMargins = .zero
        
        if laundryRatingCount == "" {
            laundryRatingCount = "3"
        }
        
        configRatingView(ratingCell: laundyShopCell, ratingCount: Float(laundryRatingCount)!)
        
        return laundyShopCell
        
    }
    
    func configRatingView(ratingCell :SelectLaundryCell, ratingCount: Float) {
        // Required float rating view params
        ratingCell.floatRatingView.emptyImage = UIImage(named: "star_Empty")
        ratingCell.floatRatingView.fullImage = UIImage(named: "star_Full")
        // Optional params
        ratingCell.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        ratingCell.floatRatingView.maxRating = 5
        ratingCell.floatRatingView.minRating = 0
        ratingCell.floatRatingView.rating = ratingCount
        ratingCell.floatRatingView.editable = false
        ratingCell.floatRatingView.halfRatings = true
        ratingCell.floatRatingView.floatRatings = false
    }
    
    @IBAction func didTapOnSelectLaundry(_ sender: UIButton) {
        let tempDict:NSDictionary = laundryShopList[sender.tag] as! NSDictionary
        let shopName: String = tempDict["store_name"] as! String
        
        print(shopName)
        orderPlaceInfo.shopName = shopName
        
        //Save Store Id
//       UserDefaults.standard.set(tempDict["store_id"] as! String, forKey: "StoreId")
//        UserDefaults.standard.synchronize()
        Session.sharedInstance.saveStoreID(value: tempDict["store_id"] as! String)
        
        self.moveToTimeSlot()
        
    }
    
    func moveToTimeSlot() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimeSlot") as! SelectTimeSlot
        vc.orderPlaceInfo = orderPlaceInfo
        self.navigationController?.viewControllers = [vc]
        
    }
    
    
    // SelectLaundry(get nearest store)API
    func callSelectLaundryAPI() {
        
        /*     if Reachability.isConnectedToNetwork() == true {
         self.showActivityIndicator(self.view)
         
         let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
         
         let tempDict:NSMutableDictionary = [
         "user_id" : userID,
         "latitude_value" : currentLocation.coordinate.latitude as Any,
         "longitude_value" : currentLocation.coordinate.longitude as Any
         ]
         print(tempDict)
         WebserviceManager.sharedInstance.selectLaundryAPI(selectLaundryDetails: tempDict, successBlock:
         {[unowned self] (success, message) in
         
         
         //  let messageString:String = message
         
         if success {
         self.selectLaundryInfo = MLSelectLaundryModel()
         print(self.selectLaundryInfo.userID)
         }
         else {
         self.showAlertView(message: message, controller: self)
         }
         self.hideActivityIndicator(self.view)
         
         }, failureBlock: {[unowned self] (errorMesssage) in
         print(errorMesssage.description)
         self.hideActivityIndicator(self.view);
         })
         }
         }
         */
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSDictionary = [
                "user_id" : userID,
                "latitude_value" : currentLocation.coordinate.latitude as Any,
                "longitude_value" : currentLocation.coordinate.longitude as Any
            ]
            
            Alamofire.request(BASE_URL + kSelectLaundry, method: .post, parameters: tempDict as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    //print(response.request as Any)  // original URL request
                    let shopList = response.result.value as! NSDictionary
                    self.laundryShopList = shopList.value(forKeyPath: "details")as! NSArray
                    print(self.laundryShopList)
                    self.laundryShopTbl.reloadData()
                    self.hideActivityIndicator(self.view);
            }
        }
    }
    
}
