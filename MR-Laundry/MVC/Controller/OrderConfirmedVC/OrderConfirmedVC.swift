//
//  OrderConfirmedVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire

class OrderConfirmedVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var orderDetailsTbl: UITableView!
    
    @IBOutlet weak var orderStatusBtn: UIButton!
    
    @IBOutlet weak var rateUsBtn: UIButton!
   
    var orderID: String = "00"
    
    let confirmedOrderedDetailsCellIdentifier = "ConfirmedOrderedDetailsCell"
    let confirmedOrderedInfoCellIdentifier = "OrderConfirmedInfoCell"

    
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        self.configInitallView()
        
        
     //   cancelOrderAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    func configInitallView() {
        
        bottomViewHeight.constant = CGFloat(bottomViewheight)
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"ORDER PLACED" as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        

       
        orderDetailsTbl.delegate = self
        orderDetailsTbl.dataSource = self
        orderDetailsTbl.tableFooterView = UIView()
        orderDetailsTbl.separatorStyle = .none
        orderDetailsTbl!.register(UINib(nibName: confirmedOrderedDetailsCellIdentifier, bundle: nil), forCellReuseIdentifier: confirmedOrderedDetailsCellIdentifier)
        orderDetailsTbl!.register(UINib(nibName: confirmedOrderedInfoCellIdentifier, bundle: nil), forCellReuseIdentifier: confirmedOrderedInfoCellIdentifier)
        
        
    }
    
    //MARK: - UITableView Delegate & DataSource
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return callOrderedDetailCell(indexPath: indexPath)

        default:
            return callConfirmedOrderInfoCell(indexPath: indexPath)

        }
    }
    
    func callOrderedDetailCell(indexPath:IndexPath) -> ConfirmedOrderedDetailsCell {
        let laundyShopCell:ConfirmedOrderedDetailsCell = orderDetailsTbl.dequeueReusableCell(withIdentifier: confirmedOrderedDetailsCellIdentifier, for: indexPath) as! ConfirmedOrderedDetailsCell
        laundyShopCell.orderIDLbl.text = orderID
        laundyShopCell.selectionStyle = .none
        return laundyShopCell
        
    }
    
    func callConfirmedOrderInfoCell(indexPath:IndexPath) -> OrderConfirmedInfoCell {
        let laundyShopCell:OrderConfirmedInfoCell = orderDetailsTbl.dequeueReusableCell(withIdentifier: confirmedOrderedInfoCellIdentifier, for: indexPath) as! OrderConfirmedInfoCell
        laundyShopCell.selectionStyle = .none
        return laundyShopCell

    }
   
    func cancelOrderAPI()->Void{
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSDictionary = [
                "booking_id" : (UserDefaults.standard.value(forKeyPath: "SaveBookingResponse.")as! NSDictionary).value(forKeyPath: "booking_id")!,
                "user_id" : userID
            ]
            print(tempDict)
            let saveBookingURL = BASE_URL + kCancelOrder
            Alamofire.request(saveBookingURL, method: .post, parameters: tempDict as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.result.value as Any)
                    if response.result.value != nil{
                        let successResponse = response.result.value as! NSDictionary
                        if successResponse.value(forKey: "status")as! Int == 1{
                            
                        }
                    }
                    self.hideActivityIndicator(self.view)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderDetailsVC" {
            if let pushToOrderDetailVC = segue.destination as? OrderDetailsVC {
                pushToOrderDetailVC.bookingID = orderID
            }
        }
    }


    @IBAction func didTapOnRateUs(_ sender: Any) {
        self.showAlertView(message: "Not yet published on App Store", controller: self)
    }
    
    @IBAction func tapOnOrderStatus(_ sender: Any) {
        self.performSegue(withIdentifier: "OrderDetailsVC", sender: nil)

    }
}
