//
//  ReviewVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire

class ReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var orderReviewTbl: UITableView!
    @IBOutlet var totalLabel: UILabel!
    var serviceArray:NSArray = []
    var serviceId:NSArray = []
    var orderDetails:NSDictionary = [:]
    let reviewCellIdentifier = "ReviewServiceCell"
    let reviewScheduleCellIdentifier = "ReviewScheduleCell"
    let reviewAddressCellIdentifier = "ReviewAddressCell"
    let reviewCouponCellIdentifier = "ReviewCouponCell"
    let reviewItemCellIdentifier = "SelectedServiceCell"
    
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet var bottomView: UIView!
    
    var orderPlaceInfo = MLOrderPlaceModel()
    
    var itemArray = ["Shirt x 2","Pant x 2","Shirt x 4"]
    var confirmedOrderID: String = ""
    
    var isFromMyOrder:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(orderDetails)
        
           }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if !isFromMyOrder {
            bottomView.isHidden = false
            //do{
            totalLabel.text = String(format : "Total : SR %@".localized(), Session().getTotalPrice())
            //}
            //catch{
            
            //}
            serviceArray = Session.sharedInstance.savedSelectedService()
            print(serviceArray)
            
            Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"REVIEW".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
            
            
        }else{
            Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"ORDER REVIEW".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
            
            bottomView.isHidden = true
            
            serviceId = orderDetails.value(forKey: "service_list") as! NSArray
            print(serviceId)
        }
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        self.configInitallView()

    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    func configInitallView() {
        bottomViewHeight.constant = CGFloat(bottomViewheight)
        
        
        orderReviewTbl.delegate = self
        orderReviewTbl.dataSource = self
        orderReviewTbl.tableFooterView = UIView()
        orderReviewTbl.separatorStyle = .none
        orderReviewTbl!.register(UINib(nibName: reviewCellIdentifier, bundle: nil), forCellReuseIdentifier: reviewCellIdentifier)
        orderReviewTbl!.register(UINib(nibName: reviewScheduleCellIdentifier, bundle: nil), forCellReuseIdentifier: reviewScheduleCellIdentifier)
        orderReviewTbl!.register(UINib(nibName: reviewAddressCellIdentifier, bundle: nil), forCellReuseIdentifier: reviewAddressCellIdentifier)
        orderReviewTbl!.register(UINib(nibName: reviewCouponCellIdentifier, bundle: nil), forCellReuseIdentifier: reviewCouponCellIdentifier)
        
        
        orderReviewTbl!.register(UINib(nibName: reviewItemCellIdentifier, bundle: nil), forCellReuseIdentifier: reviewItemCellIdentifier)
        
    }
    
    //MARK: - UITableView Delegate & DataSource
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if !isFromMyOrder {
            return 5
        }
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isFromMyOrder {
            if section == 0 {
                return Session().savedSelectedService().count
            }else if section == 2{
                return Session().getSelectedObjects().count
            }else{
                return 1
            }
        }else{
            if section == 0 {
                return 1
            }else if section == 1{
                return 1 //Session().savedSelectedService().count
            }else if section == 2{
                return (orderDetails.value(forKeyPath: "service_list.sub_category_name") as! NSArray!).count
            }else{
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.section == 0 {
        //            if serviceArray.count == 1 {
        //                return 70
        //            }else if serviceArray.count == 2 {
        //                return 105
        //            }else if serviceArray.count == 3{
        //                return 140
        //            }else{
        //                return UITableViewAutomaticDimension
        //            }
        //
        //        }else{
        return UITableViewAutomaticDimension
        //        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(indexPath.section)
        if !isFromMyOrder {
            switch indexPath.section {
            case 0:
                return callReviewServiceCell(indexPath: indexPath)
            case 1:
                return callReviewScheduleCell(indexPath: indexPath)
            case 2:
                return callReviewItemCell(indexPath: indexPath)
            case 3:
                return callReviewAddressCell(indexPath: indexPath)
            case 4:
                return callReviewCouponCell(indexPath: indexPath)
                
            default:
                return callReviewScheduleCell(indexPath: indexPath)
                
            }
            
        }else{
            switch indexPath.section {
            case 0:
                return callReviewScheduleCell(indexPath: indexPath)
            case 1:
                return callReviewServiceCell(indexPath: indexPath)
            case 2:
                return callReviewItemCell(indexPath: indexPath)
            case 3:
                return callReviewAddressCell(indexPath: indexPath)
                //        case 4:
                //            return callReviewCouponCell(indexPath: indexPath)
                
            default:
                return callReviewScheduleCell(indexPath: indexPath)
                
            }
            
        }
        //        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !isFromMyOrder {
            if section == 0 {
                return 30
            }
            if Session().getSelectedObjects().count > 0 {
                if section == 2 {
                    return 30
                }
            }
            
        }else{
            if section == 0 {
                return 0
            }else if section == 1{
                return 30
            }
            // if Session().getSelectedObjects().count > 0 {
            if section == 2 {
                if (orderDetails.value(forKeyPath: "service_list.sub_category_name") as! NSArray!).count > 0 {
                    return 30
                }
            }
            //  }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !isFromMyOrder {
            
            if section == 2 {
                let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                
                headerView.backgroundColor = UIColor.init(red: 112/255, green: 192/255, blue: 247/255, alpha: 1)
                let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                titleLabel.textColor = UIColor.white
                titleLabel.text = "     ITEM"
                
                headerView.addSubview(titleLabel)
                
                
                return headerView
            }else if section == 0{
                let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                
                headerView.backgroundColor = UIColor.init(red: 112/255, green: 192/255, blue: 247/255, alpha: 1)
                let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 2, height: 30))
                titleLabel.textColor = UIColor.white
                titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
                let orderId = UILabel.init(frame: CGRect(x: self.view.frame.width / 2 + 5, y: 0, width: self.view.frame.width / 2 - 15, height: 30))
                orderId.textColor = UIColor.white
                orderId.textAlignment = .right
                orderId.font = UIFont.boldSystemFont(ofSize: 15)
                
                titleLabel.text = String(format :"    SERVICES".localized())
                
                headerView.addSubview(titleLabel)
                
                
                return headerView
                
            }
        }else{
            if section == 2 {
                let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                
                headerView.backgroundColor = UIColor.init(red: 112/255, green: 192/255, blue: 247/255, alpha: 1)
                let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                titleLabel.textColor = UIColor.white
                titleLabel.text = "     ITEM".localized()
                
                headerView.addSubview(titleLabel)
                
                
                return headerView
            }else if section == 1{
                let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
                
                headerView.backgroundColor = UIColor.init(red: 112/255, green: 192/255, blue: 247/255, alpha: 1)
                let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 2, height: 30))
                titleLabel.textColor = UIColor.white
                titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
                let orderId = UILabel.init(frame: CGRect(x: self.view.frame.width / 2 + 5, y: 0, width: self.view.frame.width / 2 - 15, height: 30))
                orderId.textColor = UIColor.white
                orderId.textAlignment = .right
                orderId.font = UIFont.boldSystemFont(ofSize: 15)
                
                titleLabel.text = String(format :"    SERVICES".localized())
                
                headerView.addSubview(titleLabel)
                
                
                return headerView
                
            }
        }
        return nil
    }
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if section == 2 {
    //            return "Item"
    //        }
    //        return nil
    //    }
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //
    //    }
    func callReviewServiceCell(indexPath:IndexPath) -> ReviewServiceCell {
        let laundyShopCell:ReviewServiceCell = orderReviewTbl.dequeueReusableCell(withIdentifier: reviewCellIdentifier, for: indexPath) as! ReviewServiceCell
        
        if !isFromMyOrder {
            laundyShopCell.fold.text = serviceArray.object(at: indexPath.row) as? String
            
            laundyShopCell.selectionStyle = .none
            
            let name =  serviceArray.object(at: indexPath.row) as? String
            
            if name == "Wash & Iron" {
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_wi") , for: .normal)
            }else if name == "Dry Clean" {
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_wf") , for: .normal)
            }else if name == "Exclusive Wash" {
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_dc") , for: .normal)
            }else {
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_s") , for: .normal)
            }
            
        }else{
            
            let id = orderDetails.object(forKey: "service_id") as! String
            
            if id == "0" {
                laundyShopCell.fold.text = "Wash & Iron"
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_wi") , for: .normal)
            }else if id == "1" {
                laundyShopCell.fold.text = "Dry Clean"
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_wf") , for: .normal)
            }else if id == "2" {
                laundyShopCell.fold.text = "Exclusive Wash"
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_dc") , for: .normal)
            }else {
                laundyShopCell.fold.text = "Speedy"
                laundyShopCell.imageBtn.setImage(UIImage.init(named: "s_se_s") , for: .normal)
            }
        }
        laundyShopCell.selectionStyle = .none

        return laundyShopCell
        
    }
    
    func callReviewScheduleCell(indexPath:IndexPath) -> ReviewScheduleCell {
        let laundyShopCell:ReviewScheduleCell = orderReviewTbl.dequeueReusableCell(withIdentifier: reviewScheduleCellIdentifier, for: indexPath) as! ReviewScheduleCell
        
        if !isFromMyOrder {
            

            
            
            let pickupDate = convertDateFormater(Session().savedPickupDate())
            let deliveryDate = convertDateFormater(Session().savedDropDate())
            
            let pickupTime = Session().savedPickupTime()
            let dropTime = Session().savedDropTime()
            
            laundyShopCell.pickupValue.text = String(format: "%@ %@",pickupDate,pickupTime)
            laundyShopCell.delivetValue.text = String(format: "%@ %@",deliveryDate,dropTime)
            
//            laundyShopCell.delivetValue.text = String(format: "%@ %@",Session().savedDropDate(), orderPlaceInfo.dropTime)
        } else {
            laundyShopCell.orderId.isHidden = false
            
            laundyShopCell.title.text = String(format :"  %@",orderDetails.object(forKey: "driver_name") as! CVarArg)
            laundyShopCell.orderId.text = String(format :"ORDER ID : %@",orderDetails.value(forKey: "booking_id") as! CVarArg)
            
            laundyShopCell.pickupValue.text = String(format: "%@",orderDetails.value(forKey: "pickup_time_from") as! CVarArg)
            laundyShopCell.delivetValue.text = String(format: "%@",orderDetails.value(forKey: "drop_time_to") as! CVarArg)
        }
        laundyShopCell.selectionStyle = .none
        
        return laundyShopCell
        
    }
    
    func callReviewItemCell(indexPath:IndexPath) -> SelectedServiceCell {
        
        let laundyShopCell:SelectedServiceCell = orderReviewTbl.dequeueReusableCell(withIdentifier: reviewItemCellIdentifier, for: indexPath) as! SelectedServiceCell
        
        
        
        if !isFromMyOrder {
            laundyShopCell.titleLbl.text = String(format: "  %@ x %@",(Session().getSelectedObjects().value(forKey: "service_name")as! NSArray).object(at: indexPath.row) as! CVarArg,(Session().getSelectedObjects().value(forKey: "count")as! NSArray).object(at: indexPath.row) as! CVarArg)
            
        }else{
            laundyShopCell.titleLbl.text = String(format: "  %@ x %@",(orderDetails.value(forKeyPath: "service_list.sub_category_name") as! NSArray!).object(at: indexPath.row) as! CVarArg,(orderDetails.value(forKeyPath: "service_list.count") as! NSArray!).object(at: indexPath.row) as! CVarArg)
            
        }
        laundyShopCell.selectionStyle = .none
        
        return laundyShopCell
        
    }
    
    func callReviewAddressCell(indexPath:IndexPath) -> ReviewAddressCell {
        let laundyShopCell:ReviewAddressCell = orderReviewTbl.dequeueReusableCell(withIdentifier: reviewAddressCellIdentifier, for: indexPath) as! ReviewAddressCell
        
        if !isFromMyOrder {
            //        let tempDict:NSDictionary = laundryShopList[indexPath.row] as! NSDictionary
            //        let laundryTitle = tempDict.value(forKey: "title") as! String
            laundyShopCell.addressLbl.text = orderPlaceInfo.pickUpAddress
            laundyShopCell.selectionStyle = .none
            
        }else{
            laundyShopCell.addressLbl.text = orderDetails.value(forKey: "drop_location") as? String
            laundyShopCell.selectionStyle = .none
        }
        
        return laundyShopCell
        
    }
    
    func callReviewCouponCell(indexPath:IndexPath) -> ReviewCouponCell {
        let laundyShopCell:ReviewCouponCell = orderReviewTbl.dequeueReusableCell(withIdentifier: reviewCouponCellIdentifier, for: indexPath) as! ReviewCouponCell
        
        //        let tempDict:NSDictionary = laundryShopList[indexPath.row] as! NSDictionary
        //        let laundryTitle = tempDict.value(forKey: "title") as! String
        laundyShopCell.selectionStyle = .none
        
        laundyShopCell.cashBtn.layer.borderColor = UIColor.white.cgColor
        laundyShopCell.cashBtn.layer.borderWidth = 1.0
        laundyShopCell.cashBtn.clipsToBounds = true
        laundyShopCell.cashBtn.layer.cornerRadius = 16.0
        
        laundyShopCell.cardBtn.layer.borderColor = UIColor.white.cgColor
        laundyShopCell.cardBtn.layer.borderWidth = 1.0
        laundyShopCell.cardBtn.clipsToBounds = true
        laundyShopCell.cardBtn.layer.cornerRadius = 16.0
        
        laundyShopCell.payPalBtn.layer.borderColor = UIColor.white.cgColor
        laundyShopCell.payPalBtn.layer.borderWidth = 1.0
        laundyShopCell.payPalBtn.clipsToBounds = true
        laundyShopCell.payPalBtn.layer.cornerRadius = 16.0
        
        laundyShopCell.walletBtn.layer.borderColor = UIColor.white.cgColor
        laundyShopCell.walletBtn.layer.borderWidth = 1.0
        laundyShopCell.walletBtn.clipsToBounds = true
        laundyShopCell.walletBtn.layer.cornerRadius = 16.0
        
        laundyShopCell.cardBtn.setTitle("Credit/Debit Card".localized(), for: .normal)
        laundyShopCell.cashBtn.setTitle("Cash upon Pickup".localized(), for: .normal)
        laundyShopCell.payPalBtn.setTitle("Use My PayPal".localized(), for: .normal)
        laundyShopCell.walletBtn.setTitle("My Wallet".localized(), for: .normal)

        
        return laundyShopCell
        
    }
    
    @IBAction func didTapOnConfirmOrder(_ sender: Any) {
        //self.callPlaceBusinessServiceAPI(userName: "", emailID: "", mobileNumber: "", password: "")
        //  self.performSegue(withIdentifier: "OrderConfirmedVC", sender: nil)
        if Session().savedPickupDate().characters.count == 0 || Session().savedDropDate().characters.count == 0 {
            
        }else
        {
            self.callOrderConfirmAPI()
        }
        
    }
    /*
     func callPlaceBusinessServiceAPI(userName: String,
     emailID: String,
     mobileNumber: String,
     password: String) {
     
     if Reachability.isConnectedToNetwork() == true {
     self.showActivityIndicator(self.view)
     
     /*
     {"user_id":"",
     "pickup_latitude":"",
     "pickup_longitude":"",
     "pickup_location":"",
     
     "store_id":"",
     "drop_latitude":"",
     "drop_longitude":"",
     "drop_location":"",
     
     "address":"",
     "pickup_time":"",
     "drop_time":"",
     "coupon_code":"",
     "payment_type":"",
     "service_id":"",
     "estimate_price":""}
     */
     let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
     
     let tempDict:NSMutableDictionary = [
     "user_id" : userID,
     "pickup_latitude" : currentLocation.coordinate.latitude,
     "pickup_longitude" : currentLocation.coordinate.longitude,
     "pickup_location" : orderPlaceInfo.pickUpAddress,
     
     "store_id" : "1",
     "drop_latitude" : currentLocation.coordinate.latitude,
     "drop_longitude" : currentLocation.coordinate.longitude,
     "drop_location" : orderPlaceInfo.pickUpAddress,
     
     "address" : orderPlaceInfo.pickUpAddress,
     "pickup_time" : orderPlaceInfo.pickUpDate + " " + orderPlaceInfo.pickUpTime,
     "drop_time" : orderPlaceInfo.dropDate + " " + orderPlaceInfo.dropTime,
     
     "coupon_code" : "ss",
     "payment_type" : "1",
     "service_ids" : orderPlaceInfo.serviceIDs,//wash fold,wash icon ids
     "estimate_price" : "295"
     //
     //
     //                "service_name": orderPlaceInfo.serviceNames,//wash fold,wash icon
     //                "drop_address": orderPlaceInfo.pickUpAddress,
     //                "land_mark":"dfdd",
     //                "shop_name": orderPlaceInfo.shopName,
     //                "selected_serive_name": orderPlaceInfo.serviceNames,//Here 2->Quantity  ,50->amount
     //                "total_price":"235"
     ]
     print(tempDict)
     WebserviceManager.sharedInstance.orderPlaceDetails(orderDetails: tempDict, successBlock:
     {[unowned self] (success, message) in
     self.hideActivityIndicator(self.view)
     self.performSegue(withIdentifier: "OrderConfirmedVC", sender: nil)
     }, failureBlock: {[unowned self] (errorMesssage) in
     print(errorMesssage.description)
     self.hideActivityIndicator(self.view);
     })
     }
     }
     */
    func callOrderConfirmAPI()->Void{
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            /*
             print("pickup_time:------>\(orderPlaceInfo.pickUpAPITime)")
             
             let pickUpDate = Session().savedPickupDate()
             let dropDate = Session().savedDropDate()
             
             
             let pickUpTime = orderPlaceInfo.pickUpAPITime.components(separatedBy: " to ")
             let dropTime = orderPlaceInfo.dropAPITime.components(separatedBy: " to ")
             "pickup_time" : pickUpDate + " " + pickUpTime.first + " " + "to" + pickUpDate + " " + pickUpTime.last,
             "drop_time" : dropDate + " " + dropTime.first + " " + "to" + dropDate + " " + dropTime.last,
             
             */
            print("pickup_time:------>\(orderPlaceInfo.pickUpAPITime)")
            let pickUpDate = Session().savedPickupDate()
            let dropDate = Session().savedDropDate()
            let pickUpTime = orderPlaceInfo.pickUpAPITime.components(separatedBy: " to ")
            let dropTime = orderPlaceInfo.dropAPITime.components(separatedBy: " to ")
            var serviceIDs = ""
            
            let stringArray: NSArray = Session.sharedInstance.savedSelectedServiceIDs()
            serviceIDs = stringArray.componentsJoined(by: ", ")

            print("serviceIDs:------>\(serviceIDs)")

            
            let selectedPickUpDateTime: String = String(format: "%@ %@ to %@ %@", pickUpDate, pickUpTime.first!, pickUpDate, pickUpTime.last!)
            let selectedDropDateTime: String =  String(format: "%@ %@ to %@ %@", dropDate, dropTime.first!, dropDate, dropTime.last!)
            
            // + " " +  + " " + "to" + dropDate + " " + dropTime.last
            
            let tempDict:NSDictionary = [
                "user_id" : userID,
                "pickup_latitude" : currentLocation.coordinate.latitude,
                "pickup_longitude" : currentLocation.coordinate.longitude,
                "pickup_location" : orderPlaceInfo.pickUpAddress,
                
//                "service_id" : "1",
                "drop_latitude" : currentLocation.coordinate.latitude,
                "drop_longitude" : currentLocation.coordinate.longitude,
                "drop_location" : orderPlaceInfo.pickUpAddress,
                
                "address" : orderPlaceInfo.pickUpAddress,
                "pickup_time" :  selectedPickUpDateTime,
                "drop_time" : selectedDropDateTime,
                
                "coupon_code" : "ss",
                "payment_type" : "1",
                "service_id" : serviceIDs,//wash fold,wash icon ids
                "estimate_price" : Session().getTotalPrice(),
                "service_list" : Session().getSelectedObjects(),
                "store_id" : Session.sharedInstance.savedStoreID()
                
            ]
            print("callOrderConfirmAPI\(tempDict)")
            let saveBookingURL = BASE_URL + kSaveLaundryBooking
            Alamofire.request(saveBookingURL, method: .post, parameters: tempDict as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.result.value as Any)
                    if response.result.value != nil{
                        let successResponse = response.result.value as! NSDictionary
                        if successResponse.value(forKey: "status")as! Int == 1{
                            UserDefaults.standard.set(response.result.value as Any, forKey: "SaveBookingResponse")
                            
                            self.confirmedOrderID = String(format: "%@", successResponse.value(forKey: "booking_id") as! String)
//                            self.confirmedOrderID = String(format: "%d", successResponse.value(forKey: "booking_id") as! CVarArg )
                           // UserDefaults.standard.set(self.confirmedOrderID, forKey: "ORDER_ID")
                            print("confirmedOrderID:---->\(self.confirmedOrderID)")
                            UserDefaults.standard.synchronize()
                            self.performSegue(withIdentifier: "OrderConfirmedVC", sender: nil)
                            //Save selected Array Empty
                            Session().saveSelectedObjects(value: [])
                            Session().saveSelectedService(value: [])
                            Session().saveTotalPrice(value: "0")
                        }
                    }
                    self.hideActivityIndicator(self.view)
            }
            self.orderPlaceInfo.serviceNames = ""
            
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderConfirmedVC" {
            if let pushToOrderConfirmVC = segue.destination as? OrderConfirmedVC {
                pushToOrderConfirmVC.orderID = confirmedOrderID
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM yyyy"
        return  dateFormatter.string(from: date!)
        
    }
}



