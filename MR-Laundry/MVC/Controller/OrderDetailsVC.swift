//
//  OrderDetailsVC.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/6/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import AppusCircleTimer

class OrderDetailsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var orderCancelTbl: UITableView!
    
    @IBOutlet weak var orderCancelView: UIView!
    
    @IBOutlet weak var cancelOrderBtn: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var circleTimer: AppusCircleTimer!
    
    let orderCancelCellIdentifier = "OrderCancelReasonCell"
    
    var orderCancelArray:NSMutableArray = []
    
    var bookingID = ""
    @IBOutlet var orderIDLbl: UILabel!
    
    var selectedReason:String = ""
    
    var selectedIndex:Int = 0

    @IBOutlet weak var pickupDate: UILabel!
    
    @IBOutlet weak var dropDate: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        configInitialView()
    }
    
    @IBAction func handlemenu(_ sender: AnyObject) {
        
        self.showDrawer()
    }
    
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    
    func configInitialView() {
        
        orderCancelArray =
            [["title" : "Ordered by mistake"],
             ["title" : "Changed my mind"],
             ["title" : "Duplicate order"],
             ["title" : "Other"]]
        
        orderCancelTbl.delegate = self
        orderCancelTbl.dataSource = self
        orderCancelTbl.backgroundColor = UIColor.clear
        orderCancelTbl.tableFooterView = UIView()
        orderCancelTbl.separatorStyle = .none
        
        orderCancelTbl!.register(UINib(nibName: orderCancelCellIdentifier, bundle: nil), forCellReuseIdentifier: orderCancelCellIdentifier)
        cancelButton.layer.cornerRadius = 13.0
        cancelButton.layer.borderColor = UIColor.white.cgColor
        cancelButton.layer.borderWidth = 1
        cancelButton.clipsToBounds = true
        
        cancelOrderBtn.layer.cornerRadius = 13.0
        cancelOrderBtn.layer.borderColor = UIColor.white.cgColor
        cancelOrderBtn.layer.borderWidth = 1
        cancelOrderBtn.clipsToBounds = true
        
        orderCancelView.layer.cornerRadius = 10
        orderCancelView.isHidden = true
        
        
        circleTimer.delegate = self as? AppusCircleTimerDelegate
        circleTimer.isBackwards = true
        circleTimer.isActive = true
        circleTimer.totalTime = 60*15
        circleTimer.elapsedTime = 0
        circleTimer?.start()
        
        orderIDLbl.text = String(format : "ORDER ID : %@",bookingID)
        
        dropDate.text = String(format : "%@ %@",Session().savedPickupDate(),Session().savedPickupTime())
        pickupDate.text = String(format : "%@ %@",Session().savedDropDate(),Session().savedDropTime())

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return callOrderCancelCell(indexPath: indexPath)
    }
    
    
    
    func callOrderCancelCell(indexPath:IndexPath) -> OrderCancelReasonCell {
        let orderCancelCell:OrderCancelReasonCell = orderCancelTbl.dequeueReusableCell(withIdentifier: orderCancelCellIdentifier, for: indexPath) as! OrderCancelReasonCell
        
        
        let tempDict:NSDictionary = orderCancelArray[indexPath.row] as! NSDictionary
        let title = tempDict["title"] as! String
        orderCancelCell.orderCancelLbl.text = title
        orderCancelCell.selectionStyle = .none
        
        if selectedIndex == indexPath.row {
            orderCancelCell.selectionBtn.setBackgroundImage(UIImage.init(named: "service_deselected"), for: .normal)
        }else{
            orderCancelCell.selectionBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
            
        }
        
        
        return orderCancelCell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedReason = (orderCancelArray.value(forKeyPath: "title") as! NSArray).object(at: indexPath.row) as! String
        selectedIndex = indexPath.row
        tableView.reloadData()

    }
    @IBAction func tapOnCancel(_ sender: Any) {
        orderCancelView.isHidden = false
    }
    
    @IBAction func tapOnCancelOrderBtn(_ sender: Any) {
        orderCancelView.isHidden = true
        self.callCancelOrderAPI()
    }
    
    @IBAction func tapOnBackButton(_ sender: Any) {
        orderCancelView.isHidden = true
    }
    
    // cancel order API
    func callCancelOrderAPI() {
        if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSMutableDictionary = [
                "user_id" : userID,
                "booking_id" : UserDefaults.standard.value(forKeyPath: "SaveBookingResponse.booking_id")!,
                "cancel_reason":selectedReason
                ]
            print(tempDict)
            WebserviceManager.sharedInstance.cancelOrderAPI(cancelOrderDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    self.hideActivityIndicator(self.view)
                    self.circleTimer?.stop()
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.viewControllers = [vc]
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
    
}
