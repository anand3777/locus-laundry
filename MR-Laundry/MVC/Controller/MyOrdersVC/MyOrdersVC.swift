//
//  MyOrdersVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 04/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire

class MyOrdersVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var myOrderLoginView: UIView!
    @IBOutlet var orderDetailTbl: UITableView!
    let serviceCellIdentifier: String = "MyOrderCell"

    var userDetailsInfo : MLMyOrderListModel!
    
    var selectedIndex:NSDictionary = [:]
    
    @IBOutlet weak var noDataFoundLabel: UILabel!
    @IBOutlet var onGoingBtn: UIButton!
    @IBOutlet var previousBtn: UIButton!
    
    var orderList:NSArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        configInitailView()
        
        noDataFoundLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOrderList()
        
        onGoingBtn.setTitle("On Going Orders".localized(), for: .normal)
        previousBtn.setTitle("Previous Orders".localized(), for: .normal)

    }
    
    func configInitailView() {
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"MY ORDERS".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        onGoingBtn.backgroundColor = UIColor.appBGColor()
        
        if Session().getIsUserLoginStaus() == true {
        //    myOrderLoginView.isHidden = true
            orderDetailTbl.tableFooterView = UIView()
            orderDetailTbl.separatorStyle = .none
            orderDetailTbl.delegate = self
            orderDetailTbl.dataSource = self
            orderDetailTbl.register(UINib(nibName: serviceCellIdentifier, bundle: nil), forCellReuseIdentifier: serviceCellIdentifier)

        } else {
            orderDetailTbl.tableFooterView = UIView()
            orderDetailTbl.separatorStyle = .none
            orderDetailTbl.delegate = self
            orderDetailTbl.dataSource = self
            orderDetailTbl.register(UINib(nibName: serviceCellIdentifier, bundle: nil), forCellReuseIdentifier: serviceCellIdentifier)
      //      myOrderLoginView.isHidden = false
        }
      //  myOrderLoginView.isHidden = true
        
        orderDetailTbl.addSubview(self.refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return callServiceCell(indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = orderList.object(at: indexPath.row) as! NSDictionary
        self.performSegue(withIdentifier: "ReviewVC", sender: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let reviewVC = segue.destination as! ReviewVC
        reviewVC.orderDetails = selectedIndex
        reviewVC.isFromMyOrder = true
    }
    func callServiceCell(indexPath: IndexPath) -> MyOrderCell{
        let serviceCell: MyOrderCell = orderDetailTbl.dequeueReusableCell(withIdentifier: serviceCellIdentifier, for: indexPath) as! MyOrderCell
        
        let deviceSize = UIScreen.main.bounds.size
        
        serviceCell.view1Width.constant = ((deviceSize.width - 50) / 4)
        serviceCell.view2Width.constant = serviceCell.view1Width.constant
        serviceCell.view3Width.constant = serviceCell.view1Width.constant
        serviceCell.view4Width.constant = serviceCell.view1Width.constant
        
        serviceCell.amount.text = String(format: "SR %@",(orderList.value(forKey: "fare")as! NSArray).object(at: indexPath.row) as! CVarArg)
        serviceCell.orderId.text = String(format: "ORDER ID : %@".localized(),(orderList.value(forKey: "order_id")as! NSArray).object(at: indexPath.row) as! CVarArg)
        serviceCell.pickupDate.text = ((orderList.value(forKey: "pickup_time_from")as! NSArray).object(at: indexPath.row) as! String)
        serviceCell.deliveryDate.text = ((orderList.value(forKey: "drop_time_to")as! NSArray).object(at: indexPath.row) as! String)
        serviceCell.orderPlace.text = "Order Placed"
            //(orderList.value(forKey: "booking_id")as! NSArray).object(at: indexPath.row) as! String

        var orderStatus: String = ""
        orderStatus = (orderList.value(forKey: "order_status") as AnyObject).object(at: indexPath.row) as! String
        
        //orderStatus = indexPath.row
        
        switch orderStatus {
        case "0":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_ds"), for: .normal)
            serviceCell.orderPlace.text = "Not yet Confirmed"
            break
        case "1":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_s"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_ds"), for: .normal)
            serviceCell.orderPlace.text = "Order Placed"
            break
        case "2":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_s"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_ds"), for: .normal)
            serviceCell.orderPlace.text = "Way to Pick Up"
            break
        case "3":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_s"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_ds"), for: .normal)
            serviceCell.orderPlace.text = "At Laundry"
            break
        case "4":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_s"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_ds"), for: .normal)
            serviceCell.orderPlace.text = "Way to Deliver"
            break
        case "5":
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_s"), for: .normal)
            serviceCell.orderPlace.text = "Order Delivered"
            break
        default:
            serviceCell.btn1.setImage(UIImage.init(named: "ss_1_ds"), for: .normal)
            serviceCell.btn2.setImage(UIImage.init(named: "ss_2_ds"), for: .normal)
            serviceCell.btn3.setImage(UIImage.init(named: "ss_3_ds"), for: .normal)
            serviceCell.btn4.setImage(UIImage.init(named: "ss_4_ds"), for: .normal)
            serviceCell.btn5.setImage(UIImage.init(named: "ss_5_s"), for: .normal)
            serviceCell.orderPlace.text = "Order Canceled"
            break
        }
        
        
        serviceCell.selectionStyle = .none
        return serviceCell
    }

    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    
    @IBAction func didTapOnLogin(_ sender: Any) {/*
        let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC: LoginVC = storyBooard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.present(presentVC, animated: true, completion: nil)*/
        let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC: LoginVC = storyBooard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navController = UINavigationController(rootViewController: presentVC)
        self.present(navController, animated: true, completion: nil)

    }
    @IBAction func onGoingButtonTapped(_ sender: Any) {
        onGoingBtn.backgroundColor = UIColor.appBGColor()
        previousBtn.backgroundColor = UIColor.unSelectedViewColor()
        getOrderList()
    }
    
    @IBAction func pastBookingButtonTapped(_ sender: Any) {
        previousBtn.backgroundColor = UIColor.appBGColor()
        onGoingBtn.backgroundColor = UIColor.unSelectedViewColor()
        getPastBookingList()
    }
    
   
    func getOrderList() {
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let params:NSDictionary = [
                "user_id" : userID
            ]
            
            let orderListURL = BASE_URL + kMyOrderList

            

            Alamofire.request(orderListURL, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    
                    print("orderListURL:----->\(orderListURL) \n params:----->\(params) \n params:----->\(response.result)")

                    
                    //print(response.result.value! as Any)  // original URL request
                    let orderListArray = response.result.value as! NSDictionary
                    self.orderList = orderListArray.value(forKeyPath: "details") as! NSArray
                    print(self.orderList)
                    if self.orderList.count == 0{
                        self.noDataFoundLabel.isHidden = false
                    }else{
                        self.noDataFoundLabel.isHidden = true
                    }
                    self.orderDetailTbl.reloadData()
                    self.hideActivityIndicator(self.view);
                     self.refreshControl.endRefreshing()
            }
        }

    }
    func getPastBookingList() {
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let params:NSDictionary = [
                "user_id" : userID
                //userID
            ]
            Alamofire.request(BASE_URL + kPastBooking, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.result.value! as Any)  // original URL request
                    let orderListArray = response.result.value as! NSDictionary
                    self.orderList = orderListArray.value(forKeyPath: "details") as! NSArray
                    print(self.orderList)
                    if self.orderList.count == 0{
                        self.noDataFoundLabel.isHidden = false
                    }else{
                        self.noDataFoundLabel.isHidden = true
                    }
                    self.orderDetailTbl.reloadData()
                    self.hideActivityIndicator(self.view);
            }
        }
        
    }

    
    // MARK:- Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.appBGColor()
        refreshControl.addTarget(self, action: #selector(MyOrdersVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...

        self.getOrderList()

        
        // refreshControl.endRefreshing()
    }
}
