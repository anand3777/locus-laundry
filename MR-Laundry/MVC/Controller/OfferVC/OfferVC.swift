//
//  OfferVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 03/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class OfferVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
    }
    
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
