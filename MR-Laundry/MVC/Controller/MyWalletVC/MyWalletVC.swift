//
//  MyWalletVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 04/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire


class MyWalletVC: UIViewController {
    
    //@IBOutlet weak var pleaseLoginLabel: UILabel!
    @IBOutlet weak var youWalletAmountLabel: UILabel!
    @IBOutlet weak var addMoneyToWalletAccountLabel: UILabel!
    @IBOutlet weak var addMoneyBtn: UIButton!
    
    //@IBOutlet weak var loginBtn: UIButton!
    @IBOutlet var amtBtn1: UIButton!
    @IBOutlet var amtBtn2: UIButton!
    @IBOutlet var amtBtn3: UIButton!
    @IBOutlet var myWalletAmtLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "MY WALLET".localized()
        //pleaseLoginLabel.text = "Please Login to get your MyCash details".localized()
        //loginBtn.setTitle("LOGIN".localized(), for: .normal)
        
        self.configInitialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"MY WALLET".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)

        self.callPriceListAPI()
        
        youWalletAmountLabel.text = "Your Wallet Balanace".localized()
        addMoneyToWalletAccountLabel.text = "Add money to wallet account. It's quick, safe and secure".localized()
        addMoneyBtn.setTitle("ADD MONEY".localized(), for: .normal)
        
    }
    
    func configInitialView() {
        amtBtn1.backgroundColor = UIColor.clear
        amtBtn2.backgroundColor = UIColor.clear
        amtBtn3.backgroundColor = UIColor.clear

        amtBtn1.layer.borderWidth = 1.0
        amtBtn1.layer.borderColor = UIColor.appBGColor().cgColor
        amtBtn1.layer.cornerRadius = 2.0

        
        amtBtn2.layer.borderWidth = 1.0
        amtBtn2.layer.borderColor = UIColor.appBGColor().cgColor
        amtBtn2.layer.cornerRadius = 2.0

        amtBtn3.layer.borderWidth = 1.0
        amtBtn3.layer.borderColor = UIColor.appBGColor().cgColor
        amtBtn3.layer.cornerRadius = 2.0

    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    @IBAction func didTapOnLogin(_ sender: Any) {/*
         let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let presentVC: LoginVC = storyBooard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
         self.present(presentVC, animated: true, completion: nil)*/
        let storyBooard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC: LoginVC = storyBooard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navController = UINavigationController(rootViewController: presentVC)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    //price List API
    
    func callPriceListAPI() {
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            
                let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let params:NSDictionary = [
                    "user_id" : "17"  // Need to change Dynamic userid
                ]
                Alamofire.request(BASE_URL + kReferalSharing, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: nil)
                    .responseJSON { response in
                        print(response.result.value! as Any)  // original URL request
                        let tempDict: NSDictionary = response.result.value! as! NSDictionary
//                        self.myWalletAmtLbl.text = "SAR " + (Session.sharedInstance.getUserLoginDetails()["walletAmount"] as? String)!
                        self.myWalletAmtLbl.text = "SR " + tempDict.isHaveStringValue("wallet_amount")

                        Session.sharedInstance.saveOrderList(value: tempDict) // Saved Dict in Helper
//                        self.myWalletAmtLbl.text = "SAR " + tempDict.isHaveStringValue("wallet_amount")
                        self.hideActivityIndicator(self.view);
                }
            }
    }
  
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
