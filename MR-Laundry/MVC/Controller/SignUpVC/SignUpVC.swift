//
//  SignUpVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 22/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var signUpTbl: UITableView!
    let signUpTextCellIdentifier = "SignUpCell"
    let createAccountCellIdentifier = "CreateAccountCell"

    @IBOutlet var userNameTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var phoneNumberTxtField: UITextField!
    @IBOutlet var passwordTxtField: UITextField!
    @IBOutlet var confirmPasswordTxtField: UITextField!
    @IBOutlet var referalTxtField: UITextField!
    @IBOutlet weak var logoImg: UIImageView!
    
    var signUpArrray:[[String:Any]] = []


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configInitialView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func leftHeaderBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func configInitialView() {
        Helper().addHeaderPresent(forViewController: self, isleftBtnImg: true, leftBtnName: "back_arrow_white", isleftBtnImg2: false, leftBtnName2: "", centerName:"Sign Up".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        logoImg.layer.cornerRadius = logoImg.frame.size.width / 2
        logoImg.clipsToBounds = true
        
        signUpArrray = [
            ["placeHolder" : "USER NAME".localized(),
             "hintImage" : "hint_user",
            "value" : ""],
            ["placeHolder" : "ENTER YOUR EMAIL".localized(),
             "hintImage" : "hint_email",
             "value" : ""],
            ["placeHolder" : "PHONE NUMBER".localized(),
             "hintImage" : "hint_mobile",
             "value" : ""],
            ["placeHolder" : "PASSWORD".localized(),
             "hintImage" : "hint_password",
             "value" : ""],
            ["placeHolder" : "CONFIRM PASSWORD".localized(),
             "hintImage" : "hint_confirmpassword",
             "value" : ""],
            ["placeHolder" : "REFERAL CODE(OPTIONAL)".localized(),
             "hintImage" : "hint_confirmpassword",
             "value" : ""],

        ]
        
        signUpTbl.backgroundColor = UIColor.clear
        signUpTbl.tableFooterView = UIView()
        signUpTbl.delegate = self
        signUpTbl.dataSource = self
        signUpTbl.separatorStyle = .none
        signUpTbl!.register(UINib(nibName: signUpTextCellIdentifier, bundle: nil), forCellReuseIdentifier: signUpTextCellIdentifier)
        signUpTbl!.register(UINib(nibName: createAccountCellIdentifier, bundle: nil), forCellReuseIdentifier: createAccountCellIdentifier)
        
        


    }
    
    //MARK: - UITableView Delegate & DataSource
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.row {
        case 6:
            return callCreateAccountCell(indexPath: indexPath)
        default:
            return callSignUpCell(indexPath: indexPath)
            
        }
    }
    
    func callSignUpCell(indexPath:IndexPath) -> SignUpCell {
        let signUpCell:SignUpCell = signUpTbl.dequeueReusableCell(withIdentifier: signUpTextCellIdentifier, for: indexPath) as! SignUpCell
        let tempDict = signUpArrray[indexPath.row] as! NSDictionary
        let placeHolder = tempDict["placeHolder"] as! String
        let hintImage = tempDict["hintImage"] as! String
        let textFieldValue = tempDict["value"] as! String
        
        signUpCell.userValueTxt.placeholder = placeHolder
        signUpCell.userValueTxt.setTextFieldProperties(txtField: signUpCell.userValueTxt, placehodercolor: UIColor.white, placehoderText: signUpCell.userValueTxt.placeholder! as NSString, txtcolor: UIColor.white, title: signUpCell.userValueTxt.text! as NSString)

        
        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 23.5))
        let passwprdButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 25, height: 20))
        passwprdButton.setImage(UIImage.init(named: hintImage), for: .normal)
        passwordPaddingView.addSubview(passwprdButton)
        signUpCell.userValueTxt.leftView = passwordPaddingView
        signUpCell.userValueTxt.leftViewMode = UITextFieldViewMode.always
        
        signUpCell.userValueTxt.delegate = self
        signUpCell.userValueTxt.text = textFieldValue
        signUpCell.userValueTxt.tag = indexPath.row
        
        switch indexPath.row {
        case 0:
            userNameTxtField = signUpCell.userValueTxt
        case 1:
            emailTxtField = signUpCell.userValueTxt
            emailTxtField.keyboardType = .emailAddress
        case 2:
            phoneNumberTxtField = signUpCell.userValueTxt
            phoneNumberTxtField.keyboardType = .numberPad

        case 3:
            passwordTxtField = signUpCell.userValueTxt
            passwordTxtField.isSecureTextEntry = true
        case 4:
            confirmPasswordTxtField = signUpCell.userValueTxt
            confirmPasswordTxtField.isSecureTextEntry = true
        case 5:
            referalTxtField = signUpCell.userValueTxt

        default:
            break
        }
        
        signUpCell.selectionStyle = .none
        return signUpCell
    }
    
    func callCreateAccountCell(indexPath:IndexPath) -> CreateAccountCell {
        let createAccountCell:CreateAccountCell = signUpTbl.dequeueReusableCell(withIdentifier: createAccountCellIdentifier, for: indexPath) as! CreateAccountCell
        createAccountCell.createAccountBtn.clipsToBounds = true
        createAccountCell.createAccountBtn.layer.cornerRadius = 20
        createAccountCell.createAccountBtn.layer.borderColor = UIColor.white.cgColor
        createAccountCell.createAccountBtn.layer.borderWidth = 1.0
        
        createAccountCell.signInLbl.attributedText = self.signInAttributetext(string1: "Already have an account?".localized(), string2: "SIGNIN".localized())
        createAccountCell.signInBtn.addTarget(self, action: #selector(didTapOnAlreadyHaveAccount(sender:)), for: .touchUpInside)

        
        createAccountCell.selectionStyle = .none
        
        createAccountCell.createAccountBtn.addTarget(self, action: #selector(didTapOnCreateAccount(sender:)), for: .touchUpInside)
        return createAccountCell
    }
   
    
    func signInAttributetext(string1: String, string2: String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString()
        
        let defaultPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.white ,
            NSFontAttributeName : UIFont.setAppFontLight(size: 15.0)
        ]
        let selectedPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName : UIFont.setAppFontBold(size: 15.0)
        ]
        
        attributedString.append(NSAttributedString(string: string1 + " ", attributes: defaultPrice))
        attributedString.append(NSAttributedString(string: string2 , attributes: selectedPrice))
        return attributedString
    }
    
    @IBAction func didTapOnCreateAccount( sender: UIButton) {
        validateRegisteration()
    }
    
    @IBAction func didTapOnAlreadyHaveAccount( sender: UIButton) {

        self.performSegue(withIdentifier: "LoginVC", sender: nil)
    
    }
    
    func validateRegisteration() {
        
        if (userNameTxtField.text?.characters.count)! <= 0{
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter UserName", bgColor: UIColor.msgBG(), controller: self)
        } else if (emailTxtField.text?.characters.count)! <= 0{
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter your Email", bgColor: UIColor.msgBG(), controller: self)
        } else if Helper().isValidEmail(testStr: emailTxtField.text!) == false {
            
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter Valid EmailID", bgColor: UIColor.msgBG(), controller: self)
            
        }else if (phoneNumberTxtField.text?.characters.count)! <= 0 {
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter Phone Number", bgColor: UIColor.msgBG(), controller: self)
        } else if (passwordTxtField.text?.characters.count)! <= 0 {
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter Password", bgColor: UIColor.msgBG(), controller: self)
        } else if (passwordTxtField.text != confirmPasswordTxtField.text) {
            Helper.sharedInstance.showAlertPopUPWithMessage("Password not Matched", bgColor: UIColor.msgBG(), controller: self)

        } else {
            self.registerUserInfo(userName: userNameTxtField.text!, emailID: emailTxtField.text!, mobileNumber: phoneNumberTxtField.text!, referalCode: referalTxtField.text!, password: passwordTxtField.text!)

        }
    }
    
    
    func registerUserInfo(userName: String,
                          emailID: String,
                          mobileNumber: String,
                          referalCode: String,
                          password: String) {
        
        if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            /*
             {"name":"",
             "email":"",
             "password":"",
             "mobile_number":"",
             "telephone_code":"",
             "device_token":"",
             "device_type":""}*/
            
            let tempDict:NSMutableDictionary = [
                            "name":userName,
                            "email":emailID,
                            "password":password,
                            "mobile_number":mobileNumber,
                            "referral_code":referalCode,
                            "telephone_code":"+91",
                            "device_token":"xxxxx",
                            "device_type":"1"
            ]
           
            WebserviceManager.sharedInstance.userRegisterationAPI(userRegisterationDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                
                if success {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    
                    
                    self.navigationController?.viewControllers = [vc]
                }
                else {
                    self.showAlertView(message: message, controller: self)
                }
                self.hideActivityIndicator(self.view)
                
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
    
    //Mark:- TextField Delegate
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        let txtFieldTag: Int = textField.tag
        let txtValue:String = textField.text!
        

        switch txtFieldTag {
        case 0:
            userNameTxtField.text = txtValue
        case 1:
            emailTxtField.text = txtValue
        case 2:
            phoneNumberTxtField.text = txtValue
        case 3:
            passwordTxtField.text = txtValue
        case 4:
            confirmPasswordTxtField.text = txtValue
        case 5:
            referalTxtField.text = txtValue
        default:
            break
        }
        
        signUpArrray[txtFieldTag]["value"] = txtValue
        //print("signupArray:----->\(signUpArrray)")

        signUpTbl.reloadData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
        
        return true
    }
}
