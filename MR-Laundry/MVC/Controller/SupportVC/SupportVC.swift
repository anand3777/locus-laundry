//
//  SupportVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 03/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SupportVC: UIViewController {

    @IBOutlet weak var callSupportView: UIView!

    @IBOutlet weak var mailSupportView: UIView!
    
    @IBOutlet weak var whatsappSupport: UIView!
    
    @IBOutlet weak var callSupportImage: UIImageView!
    
    @IBOutlet weak var callLabel1: UILabel!
    
    @IBOutlet weak var callLabel2: UILabel!
   
    @IBOutlet weak var callUsLabel: UILabel!
    
    @IBOutlet weak var mailSupportImage: UIImageView!
    
    @IBOutlet weak var mailLabel1: UILabel!
    
    @IBOutlet weak var mailLabel2: UILabel!
    
    @IBOutlet weak var mailUsLabel: UILabel!
    
    @IBOutlet weak var tips: UILabel!
    
    @IBOutlet weak var whatsappButton: UIButton!

    @IBOutlet weak var contactOnWhatsApp: UILabel!

    @IBOutlet weak var whatsAppDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        callSupportView.layer.cornerRadius = 10
        mailSupportView.layer.cornerRadius = 10
        whatsappSupport.layer.cornerRadius = 10

        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SUPPORT".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        callLabel1.text = "Do u have questions about the MR-Laundry mobile app ?".localized()
        callLabel2.text = "Call Customer Support".localized()
        mailLabel1.text = "Do u have questions about the MR-Laundry mobile app ?".localized()
        mailLabel2.text = "Send An Email".localized()
        tips.text = "tips".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        contactOnWhatsApp.text = "Contact On WhatsApp".localized()
        whatsAppDescription.text = "Do u have questions about the MR-Laundry mobile app ?".localized()
    }

    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }

    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    //#MARK - WhatsApp
    @IBAction func whatsAppButtonTapped(_ sender: Any) {
        let msg = "MR-Laundry Test"
        let urlWhats = "whatsapp://send?text=\(msg)"
        
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    print("Please install whatsapp")
                    Helper.sharedInstance.showAlertPopUPWithMessage("please install watsapp", bgColor: UIColor.msgBG(), controller: self)
                    
                }
            }
        }
    }
    
    
    
}
