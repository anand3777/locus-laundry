//
//  LoginVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 06/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration

class LoginVC: UIViewController, UITextFieldDelegate {
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var passwordTxtField: UITextField!
    @IBOutlet var signUpLbl: UILabel!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var forgotPassword: UIButton!
    
    var userDetailsInfo : MLUserModel!
    @IBOutlet var language: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        self.configInitialView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(configInitialView), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func leftHeaderBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
        self.moveToHomeScreen()
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    @IBAction func didTapOnForgotPassword(_ sender: Any) {
        self.performSegue(withIdentifier: "ForgotPasswordVC", sender: nil)
    }
    
    
    
    
    func configInitialView() {
                Helper().addHeaderPresent(forViewController: self, isleftBtnImg: true, leftBtnName: "back_arrow_white", isleftBtnImg2: false, leftBtnName2: "", centerName:"".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        
        emailTxtField.setTextFieldProperties(txtField: emailTxtField, placehodercolor: UIColor.white, placehoderText: emailTxtField.placeholder! as NSString, txtcolor: UIColor.white, title: emailTxtField.text! as NSString)
        
        emailTxtField.setTextFieldProperties(txtField: passwordTxtField, placehodercolor: UIColor.white, placehoderText: passwordTxtField.placeholder! as NSString, txtcolor: UIColor.white, title: passwordTxtField.text! as NSString)
        
        
        emailTxtField.delegate = self
        emailTxtField.autocorrectionType = .no
        emailTxtField.placeholder = "EMAIL ADDRESS".localized()
        passwordTxtField.delegate = self
        emailTxtField.keyboardType = .emailAddress
        passwordTxtField.isSecureTextEntry = true
        passwordTxtField.placeholder = "Password".localized()
        forgotPassword.setTitle("Forgot Password?".localized(), for: .normal)
        //  language.setTitle("English".localized(), for: .normal)
        
        signUpLbl.attributedText = self.signUpAttributetext(string1: "Don't have an account?".localized(), string2: "SIGN UP".localized())
        signUpButton.layer.cornerRadius =  20.0
        signUpButton.clipsToBounds = true
        signUpButton.layer.borderWidth = 1.0
        signUpButton.layer.borderColor = UIColor.white.cgColor
        
        
        signInButton.layer.cornerRadius =  20.0
        signInButton.clipsToBounds = true
        signInButton.layer.borderWidth = 1.0
        signInButton.layer.borderColor = UIColor.white.cgColor
        signInButton.setTitle("Sign In".localized(), for: .normal)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 15))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 15))
        imageView.image = UIImage(named: "hint_email")  //location
        paddingView.addSubview(imageView)
        emailTxtField.leftView = paddingView
        emailTxtField.leftViewMode = UITextFieldViewMode.always
        
        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 23.5))
        let passwprdImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 23.5))
        passwprdImageView.image = UIImage(named: "hint_password")  //location
        passwordPaddingView.addSubview(passwprdImageView)
        passwordTxtField.leftView = passwordPaddingView
        passwordTxtField.leftViewMode = UITextFieldViewMode.always
        
        
        //print("Stored Information:---->\(Session.sharedInstance.getUserLoginDetails())")
        
        if Session.sharedInstance.getUserLoginDetails().count > 0 {
            emailTxtField.text = (Session.sharedInstance.getUserLoginDetails()["userEmail"] as? String)!
            passwordTxtField.text = Session.sharedInstance.getUserLoginDetails()["password"] as? String
        }
        
    }
    
    
    @IBAction func didTapOnSignUp(_ sender: Any) {
        self.performSegue(withIdentifier: "SignUpVC", sender: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTxtField {
            passwordTxtField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func signUpAttributetext(string1: String, string2: String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString()
        let defaultPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.white ,
            NSFontAttributeName : UIFont.setAppFontLight(size: 15.0)
        ]
        let selectedPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName : UIFont.setAppFontBold(size: 15.0)
        ]
        
        attributedString.append(NSAttributedString(string: string1 + " " , attributes: defaultPrice))
        
        attributedString.append(NSAttributedString(string: string2 , attributes: selectedPrice))
        return attributedString
    }
    
    @IBAction func didTapOnLogIn(_ sender: Any) {
        
        //self.performSegue(withIdentifier: "slider", sender: nil)
        
                self.view.endEditing(true)
        
                let emailIDValue = emailTxtField.text!
                let passwordValue = passwordTxtField.text!
        
        
                if Helper().isValidEmail(testStr: emailIDValue) == false {
        
                    Helper.sharedInstance.showAlertPopUPWithMessage("Enter Valid EmailID", bgColor: UIColor.msgBG(), controller: self)
        
                } else {
                    if Helper().isValidEmail(testStr: emailIDValue) == true &&  passwordValue.characters.count > 0{
        
                        self.getLoginDetailsInfo(email: emailTxtField.text!, password: passwordTxtField.text!)
        
                        } else {
                        Helper.sharedInstance.showAlertPopUPWithMessage("Enter Password", bgColor: UIColor.msgBG(), controller: self)
                    }
                }
    }
    
    func displayAlertMessage(messageToDisplay: String){
        
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    func getLoginDetailsInfo(email: String, password: String) {
        
//        var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.moveToHomeScreen), userInfo: nil, repeats: false)
//        
//        return
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            /*
             {"email":"",
             "password":"",
             "device_type":"",
             "device_token":""}
             */
            
            let tempDict:NSMutableDictionary = [
                "email":email,
                "password":password,
                "device_token":"xxxxx",
                "device_type":"1"
            ]
            
            WebserviceManager.sharedInstance.userLoginAPI(userLoginDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    let messageString:String = message
                    
                    
                    if success {
                        
                        
                        if messageString.range(of: "OTP not verfied") != nil {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                            vc.otpCode = ""
                            self.navigationController?.viewControllers = [vc]
                        } else {
                            self.userDetailsInfo = DemoGlobalData.sharedInstance.userDetailInfo
                            
                            print(self.userDetailsInfo)
                            let tempLoginDetails: NSDictionary = ["userName": self.userDetailsInfo.userName!,
                                                                  "password": password,
                                                                  "userEmail": self.userDetailsInfo.userEmailID!,
                                                                  "userID": self.userDetailsInfo.userID!,
                                                                  "telephoneCode": self.userDetailsInfo.userTelephoneCode!,
                                                                  "mobileNumber": self.userDetailsInfo.userMobileNumber!,
                                                                  "referralCode": self.userDetailsInfo.referallCode!,
                                                                  "walletAmount": self.userDetailsInfo.walletAmount!,
                                                                  ]
                            Session.sharedInstance.setUserLoginDetails(tempLoginDetails)
                            
                            self.moveToHomeScreen()
                        }
                    }
                    else {
                        self.showAlertView(message: message, controller: self)
                    }
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
    
    // MARK: - TextField delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        
        
        if textField == emailTxtField {
            let maxLength = 25
            return txtAfterUpdate.characters.count <= maxLength
        }
        return true
    }
    
    func moveToHomeScreen()  {
        Session().setIsUserLoginStaus(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.viewControllers = [vc]
        
    }
    
    //    @IBAction func languageButtonTapped(_ sender: Any) {
    //
    //        if Helper.sharedInstance.getSelectedLanguage() == "en" {
    //            Helper.sharedInstance.saveSelectedLanguage(lang: "ar")
    //            Localize.setCurrentLanguage("ar")
    //            language.setTitle("English".localized(), for: .normal)
    //        }else{
    //            Helper.sharedInstance.saveSelectedLanguage(lang: "en")
    //            Localize.setCurrentLanguage("en")
    //            language.setTitle("English".localized(), for: .normal)
    //        }
    //    }
}
