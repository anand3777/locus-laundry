//
//  orderDetailVC.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/6/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class orderDetailVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"ORDER DETAILS" as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        //configInitialView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
//    func configInitialView() {
//        cancelButton.layer.cornerRadius = 20.0
//        cancelButton.layer.borderColor = UIColor.white.cgColor
//        cancelButton.layer.borderWidth = 1
//        cancelButton.clipsToBounds = true
//    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
