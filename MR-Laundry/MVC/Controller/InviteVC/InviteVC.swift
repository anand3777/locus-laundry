//
//  InviteVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 04/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Social

class InviteVC: UIViewController , UIActionSheetDelegate {
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var titleLabel: UILabel!

    
    var shareMessage:String = ""
    var userDetailsInfo : MLUserModel!

    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationDrawer.sharedInstance.initialize(forViewController: self)

        self.userDetailsInfo = DemoGlobalData.sharedInstance.userDetailInfo
        
        if Session.sharedInstance.getOrderList().value(forKey: "referral_sharing_message") as? String != nil {
          //  let message = Session.sharedInstance.getOrderList().value(forKey: "referral_sharing_message") as! String
            let message = "Invite a friend & get 20SR"
            let referralCode = (Session.sharedInstance.getUserLoginDetails()["referralCode"] as? String)!
            shareMessage = String(format : "%@ \nReferral Code : %@", message, referralCode)
        }
        
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configInitiallView()
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"INVITE & EARN".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        titleLabel.text = "Let your friends fill your wallet".localized()
    }
    
    func configInitiallView() {
        
        loginBtn.layer.cornerRadius = 20.0
        loginBtn.layer.borderColor = UIColor.white.cgColor
        loginBtn.layer.borderWidth = 1
        loginBtn.clipsToBounds = true
        
        loginBtn.setTitle("Invite".localized(), for: .normal)
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    @IBAction func inviteBtnAction(_ sender: Any) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Invite & Earn", message: "Share Via", preferredStyle: .actionSheet)
        
        let deleteActionButton = UIAlertAction(title: "Cancel", style: .cancel)
        { _ in
            print("Delete")
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        
        let twitterBtn = UIAlertAction(title: "Twitter", style: .default) { _ in
            let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            //            vc.add(imageView.image!)
            //            vc.add(URL(string: "http://www.example.com/"))
            vc?.setInitialText(self.shareMessage)
            self.present(vc!, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(twitterBtn)
        
        let FBbtn = UIAlertAction(title: "Facebook", style: .default)
        { _ in
            let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
            //            vc.add(imageView.image!)
            //            vc.add(URL(string: "http://www.example.com/"))
            vc?.setInitialText(self.shareMessage)
            self.present(vc!, animated: true, completion: nil)

        }
        actionSheetControllerIOS8.addAction(FBbtn)
        
        let whatsAppBtn = UIAlertAction(title: "WhatsApp", style: .default)
        { _ in
            let msg = self.shareMessage
            let urlWhats = "whatsapp://send?text=\(msg)"
            
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.openURL(whatsappURL as URL)
                    } else {
                        print("Please install whatsapp")
                        Helper.sharedInstance.showAlertPopUPWithMessage("Please Install Whatsapp", bgColor: UIColor.msgBG(), controller: self)
                    }
                }
            }
        }
        actionSheetControllerIOS8.addAction(whatsAppBtn)
        
        
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    @IBAction func handleMenu(_ sender: AnyObject) {
        self.showDrawer()
    }
    
   
}
