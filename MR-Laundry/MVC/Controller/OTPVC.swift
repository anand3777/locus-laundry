//
//  OTPVC.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 6/26/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire

class OTPVC: UIViewController {

    @IBOutlet weak var OTPText: UITextField!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var OTPNotReceivedLabel: UILabel!
    
    @IBOutlet weak var skipBtn: UIButton!
    
    var otpCode: String = ""
    var userInfoDict :LoginModel!
    var userDetailsInfo : MLUserModel!

    @IBAction func resendButtonTapped(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            let params:NSDictionary = [
                "id" : userInfoDict.emailID!,
                "email" : userInfoDict.identi!
                //userID
            ]
            Alamofire.request(BASE_URL + kresendOTP, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.result.value! as Any)  // original URL request
                    self.hideActivityIndicator(self.view);
            }
        }

    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.viewControllers = [vc]

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         configInitailView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func leftHeaderBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    @IBAction func didTapOnVerify(_ sender: Any) {
        if (OTPText.text?.characters.count)! > 3 {
            callVerifyOTPAPI(email: userInfoDict.emailID!, OTPString: OTPText.text!, IDString: userInfoDict.identi!)
        } else {
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter OTP", bgColor: UIColor.msgBG(), controller: self)
        }
    }
    
    
    func configInitailView() {
        OTPNotReceivedLabel.attributedText = self.signInAttributetext(string1: "Didn't received OTP", string2: "RESEND")
        Helper().addHeaderPresent(forViewController: self, isleftBtnImg: true, leftBtnName: "back_arrow_white", isleftBtnImg2: false, leftBtnName2: "", centerName:"OTP Verification" as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        skipBtn.layer.cornerRadius = 20.0
        skipBtn.layer.borderColor = UIColor.white.cgColor
        skipBtn.layer.borderWidth = 1
        skipBtn.clipsToBounds = true
        
        userInfoDict = DemoGlobalData.sharedInstance.loginDetailInfo
        
        OTPText.textColor = UIColor.textFieldTextColor()
        OTPText.text = userInfoDict.OTP!
    }

    
    func signInAttributetext(string1: String, string2: String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString()
        
        let defaultPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.textFieldTextColor() ,
            NSFontAttributeName : UIFont.setAppFontRegular(size: 15.0)
        ]
        let selectedPrice:[String:Any] = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName : UIFont.setAppFontRegular(size: 15.0)
        ]
        
        attributedString.append(NSAttributedString(string: string1 + "  ", attributes: defaultPrice))
        attributedString.append(NSAttributedString(string: string2 , attributes: selectedPrice))
        return attributedString
    }
    
    
    
    func callVerifyOTPAPI(email: String, OTPString: String, IDString: String) {
        
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            /* { "email":"", "otp":"", "id":""} */
            
            let tempDict:NSMutableDictionary = [
                "email" : email,
                "otp" : OTPString,
                "id" : IDString,
            ]
            WebserviceManager.sharedInstance.verifyOTPAPI(OTPDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    let messageString:String = message
                    
                    if success {
                        self.userDetailsInfo = DemoGlobalData.sharedInstance.userDetailInfo
                        
                        print(self.userDetailsInfo)
                        let tempLoginDetails: NSDictionary = ["userName": self.userDetailsInfo.userName!,
                                                              "password": "",
                                                              "userEmail": self.userDetailsInfo.userEmailID!,
                                                              "userID": self.userDetailsInfo.userID!,
                                                              "telephoneCode": self.userDetailsInfo.userTelephoneCode!,
                                                              "mobileNumber": self.userDetailsInfo.userMobileNumber!,
                                                              "referralCode": self.userDetailsInfo.referallCode!,
                                                              "walletAmount": self.userDetailsInfo.walletAmount!,
                                                              
                                                              ]
                        
                        Session.sharedInstance.setIsUserLoginStaus(true)
                        Session.sharedInstance.setUserLoginDetails(tempLoginDetails)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.viewControllers = [vc]
                    }
                    else {
                        self.showAlertView(message: message, controller: self)
                    }
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
   
}
