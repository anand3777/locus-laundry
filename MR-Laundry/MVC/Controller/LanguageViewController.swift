//
//  LanguageViewController.swift
//  MR-Laundry
//
//  Created by Gowthaman on 30/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var languageArray:NSArray = ["English","Arabic"]
    
    @IBOutlet weak var tableView: UITableView!
    var navigationDrawer:NavigationDrawer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper().addHeaderPresent(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: true, leftBtnName2: "", centerName:"LANGUAGE SETTINGS".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        //Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SUPPORT".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
       
        // Do any additional setup after loading the view.
    }
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }

    @IBAction func handlemenu(_ sender: AnyObject) {
        
        self.showDrawer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return languageArray.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = languageArray[indexPath.row] as? String
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedLanguage = languageArray[indexPath.row]
        print(selectedLanguage)
        if indexPath.row == 0{
            Helper.sharedInstance.saveSelectedLanguage(lang: "en")
            Localize.setCurrentLanguage("en")
        }else{
            Helper.sharedInstance.saveSelectedLanguage(lang: "ar")
            Localize.setCurrentLanguage("ar")
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.viewControllers = [vc]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
