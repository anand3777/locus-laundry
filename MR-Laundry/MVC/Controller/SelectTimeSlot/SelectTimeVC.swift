//
//  SelectTimeVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 05/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
//import WBSegmentControl

class SelectTimeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet var selectTimeCollectionView: UICollectionView!
    
    @IBOutlet weak var dateView: UIView!
    
    let segmentCtrl_G = WBSegmentControl()
    
    var timeCollecitonCellIdentifier = "TimeCollectionCell"
    let headerViewIdentifier = "HeaderView"
    let timeSlot = [
        ["09:00:00 to 10:00:00",
         "10:00:00 to 11:00:00"],
        ["12:00:00 to 13:00:00", "13:00:00 to 14:00:00", "14:00:00 to 15:00:00"],
        ["05:00:00 to 06:00:00",
         "07:00:00 to 09:00:00"]
    ]
    
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    var orderPlaceInfo = MLOrderPlaceModel()
    var isFromPickUp = true
    var days = [String]()
    var sortedDate = [String]()

    
    override func loadView() {
        super.loadView()
        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
        
        
        let now = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now as Date)
        print("nameOfMonth:----->\(nameOfMonth)")
        
        
        for _ in 1 ... 10 {
            let day = cal.component(.day, from: date)
            
            let combinedStr = String(format: "%@", dayOfName(str: date))
            days.append(combinedStr)
            date = cal.date(byAdding: .day, value: 1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let str = dateFormatter.date(from: combinedStr)
            
            print(combinedStr)
            
            
        }
        print("days:----->\(days)")
        var date1:String = ""
        var date2:String = ""
        var date3:String = ""
        var date4:String = ""
        var date5:String = ""
        var date6:String = ""
        var date7:String = ""
        
        
        print(orderPlaceInfo.serviceIDs)
        print(days[0])
        print(days[1])

        if isFromPickUp == true {
            if orderPlaceInfo.serviceIDs.range(of:"3") != nil{
                
                sortedDate.append(days[0])
                sortedDate.append(days[1])
                sortedDate.append(days[2])
                sortedDate.append(days[3])
                sortedDate.append(days[4])
                sortedDate.append(days[5])
                sortedDate.append(days[6])

                date1 = days[0]
                date2 = days[1]
                date3 = days[2]
                date4 = days[3]
                date5 = days[4]
                date6 = days[5]
                date7 = days[6]
            } else {
                date1 = days[1]
                date2 = days[2]
                date3 = days[3]
                date4 = days[4]
                date5 = days[5]
                date6 = days[6]
                date7 = days[7]
                
                sortedDate.append(days[1])
                sortedDate.append(days[2])
                sortedDate.append(days[3])
                sortedDate.append(days[4])
                sortedDate.append(days[5])
                sortedDate.append(days[6])
                sortedDate.append(days[7])

                
            }
        } else {
            if orderPlaceInfo.serviceIDs.range(of:"3") != nil{
                date1 = days[1]
                date2 = days[2]
                date3 = days[3]
                date4 = days[4]
                date5 = days[5]
                date6 = days[6]
                date7 = days[7]
                sortedDate.append(days[1])
                sortedDate.append(days[2])
                sortedDate.append(days[3])
                sortedDate.append(days[4])
                sortedDate.append(days[5])
                sortedDate.append(days[6])
                sortedDate.append(days[7])

                
            } else {
                date1 = days[3]
                date2 = days[4]
                date3 = days[5]
                date4 = days[6]
                date5 = days[7]
                date6 = days[8]
                date7 = days[9]
                sortedDate.append(days[3])
                sortedDate.append(days[4])
                sortedDate.append(days[5])
                sortedDate.append(days[6])
                sortedDate.append(days[7])
                sortedDate.append(days[8])
                sortedDate.append(days[9])
            }
        }
        
        
        
    

        self.dateView.addSubview(segmentCtrl_G)
        segmentCtrl_G.frame = CGRect.init(x: 0, y: 0, width: deviceSize.width, height: 50)
        
        segmentCtrl_G.segments = [
            TextSegment(text: date1),
            TextSegment(text: date2),
            TextSegment(text: date3),
            TextSegment(text: date4),
            TextSegment(text: date5),
            TextSegment(text: date6),
            TextSegment(text: date7),
        ]
        segmentCtrl_G.delegate = self
        segmentCtrl_G.style = .strip
        segmentCtrl_G.nonScrollDistributionStyle = .center

        
        
        
    }
    
    func dayOfName (str:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let Monday = dateFormatter.string(from: str)
        return Monday
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentCtrl_G.selectedIndex = 0
        
        confiInitialView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
    }
    
    func leftHeaderBtnAction() {
        //self.showDrawer()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
    func confiInitialView() {
        
        bottomViewHeight.constant = CGFloat(bottomViewheight)
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SELECT TIMESLOT" as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        selectTimeCollectionView.register(UINib(nibName: timeCollecitonCellIdentifier, bundle:nil), forCellWithReuseIdentifier: timeCollecitonCellIdentifier)
        selectTimeCollectionView.delegate = self
        selectTimeCollectionView.dataSource = self
        
        selectTimeCollectionView.register(UINib(nibName: "TimeHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerViewIdentifier)
    }
    
    
    //MARK:-  UICollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var size = CGSize(width: 400, height: 30)
        
        switch section {
        case 1:
            size = CGSize(width: 0, height: 0)
        default:
            size = CGSize(width: 400, height: 30)
        }
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableview:UICollectionReusableView!
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView: TimeHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerViewIdentifier, for: indexPath) as! TimeHeaderView
            
            var titleString: String = ""
            switch indexPath.section {
            case 0:
                titleString = "MORNING"
            case 1:
                titleString = "AFTERNOON"
            default:
                titleString = "EVENING"
            }
            headerView.sectionLabel.text = titleString
            reusableview = headerView
        }
        return reusableview
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else if section == 1{
            return 0
        }
        else{
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let postCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: timeCollecitonCellIdentifier, for: indexPath) as! TimeCollectionCell
        
        var sortedTime = timeSlot[indexPath.section][indexPath.row]
        var fromTime:String = ""
        var toTime:String = ""


        print("sortedTime:------>\(sortedTime)")

        /*
         let expireArr = expireStr.components(separatedBy: "/")
         if(expireArr.count > 1) {
         exipryYear =  expireArr.last! as String  // "2018"
         exipryMonth =  expireArr.first! as String      //"12"
         }

         */
        let expireArr = sortedTime.components(separatedBy: "to")
        fromTime = expireArr.first! as String
        toTime = expireArr.last! as String

        let fromSplit = fromTime.components(separatedBy: ":")
        fromTime = fromSplit.first! + ":" + fromSplit[1]
        print("fromTime:------>\(fromTime)")

        let toSplit = toTime.components(separatedBy: ":")
        toTime = toSplit.first! + ":" + toSplit[1]
        print("toTime:------>\(toTime)")

        sortedTime = fromTime + " to " + toTime
        
        postCollectionCell.timeLbl.text = sortedTime
        
        postCollectionCell.layer.borderColor = UIColor.white.cgColor
        postCollectionCell.layer.borderWidth = 1.0
        return postCollectionCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        return CGSize.init(width: (screenSize.width / 2), height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //"pickup_time" : "2017-08-08 18:00:00 to 2017-08-08 18:00:00",
        var selectedTime:String = timeSlot[indexPath.section][indexPath.row]
        var selectedAPITime:String = timeSlot[indexPath.section][indexPath.row]
        
        print("selectedTime:------>\(selectedTime)")
        
        var fromTime:String = ""
        var toTime:String = ""
        print("selectedTime:------>\(selectedTime)")
        let expireArr = selectedTime.components(separatedBy: "to")
        fromTime = expireArr.first! as String
        toTime = expireArr.last! as String
        
        let fromSplit = fromTime.components(separatedBy: ":")
        var fromTimeInt = self.getRailwayTime(inputString: fromSplit.first!)
        print("fromTimeInt:------>\(fromTimeInt)")
        
        let toSplit = toTime.components(separatedBy: ":")
        var toTimeInt = self.getRailwayTime(inputString: (toSplit.first?.trimmingCharacters(in: .whitespaces))!)
        print("toTimeInt:------>\(toTimeInt)")

        
        if indexPath.section == 2 {
            selectedAPITime = fromTimeInt + " to " + toTimeInt
        }
        
        if isFromPickUp == true {
            orderPlaceInfo.pickUpTime = selectedTime
            Session().savePickupTime(value: selectedTime)
            orderPlaceInfo.pickUpAPITime = selectedAPITime
        } else {
            orderPlaceInfo.dropTime = selectedTime
            Session().saveDropTime(value: selectedTime)
            orderPlaceInfo.dropAPITime = selectedAPITime
        }
        self.leftHeaderBtnAction()
    }
    
    func getRailwayTime(inputString: String) -> String {
        var outPutString: String = ""
        
        switch inputString {
        case "01:00:00":
            outPutString = "13:00:00"
            break
        case "02:00:00":
            outPutString = "14:00:00"
            break
        case "03:00:00":
            outPutString = "15:00:00"
            break
        case "04:00:00":
            outPutString = "16:00:00"
            break
        case "05":
            outPutString = "17:00:00"
            break
        case "06":
            outPutString = "18:00:00"
            break
        case "07":
            outPutString = "19:00:00"
            break
        case "08":
            outPutString = "20:00:00"
            break
        case "09":
            outPutString = "21:00:00"
            break
        case "10:00:00":
            outPutString = "22:00:00"
            break
        case "11:00:00":
            outPutString = "23:00:00"
            break
        case "12:00:00":
            outPutString = "00:00:00"
            break
        default:
            outPutString = "12:00:00"
            break

        }
        return outPutString
    }
    
}


extension SelectTimeVC: WBSegmentControlDelegate {
    
    func segmentControl(_ segmentControl: WBSegmentControl, selectIndex newIndex: Int, oldIndex: Int) {
        print("newIndex:---->\(newIndex)")
        print("oldIndex:---->\(sortedDate[newIndex])")
        var selectedDate:String!
        
        
        selectedDate = sortedDate[newIndex]
       
        print("oldIndex:---->\(sortedDate[newIndex])")
        print("selectedDate:---->\(selectedDate)")

        
        selectedDate = selectedDate.replacingOccurrences(of: "\n", with: "")
        
        if isFromPickUp == true {
          //  orderPlaceInfo.pickUpDate = selectedDate
            Session().savePickupDate(value: selectedDate)
        } else {
          //  orderPlaceInfo.dropDate = selectedDate
            Session().saveDropDate(value: selectedDate)
        }
    }
}

extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return dateFormatter.string(from: self)
    }
}

