//
//  SelectTimeSlot.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import Alamofire
//tes

class SelectTimeSlot: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var selectStatusLbl: UILabel!
    @IBOutlet var addressView: UIView!
    
    let tableViewCellIdentifier = "AddressCell"
    var addAddressInfo = MLAddAddressModel()

    var addressListArray = NSArray()
    
    var isFromAddressSelection:Bool = Bool()
    @IBOutlet weak var addNow: UIView!

    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet var botomViewHeight: NSLayoutConstraint!
    var orderPlaceInfo = MLOrderPlaceModel()
    @IBOutlet var pickUpLbl: UILabel!
    @IBOutlet var dropLbl: UILabel!
    @IBOutlet var scheduleBtn: UIButton!
    @IBOutlet var addressBtn: UIButton!

    
    @IBOutlet weak var pickUpView: UIView!
    
    @IBOutlet weak var deliveryView: UIView!
    
    @IBOutlet weak var addNowBtn: UIButton!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
  
    @IBOutlet weak var selectAddressBtn: UIButton!
    
    @IBOutlet weak var addNewAddressLabel: UILabel!
    
    var selectedIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confiInitialView()
        
        self.callGetAddressAPI()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        serviceLabel.text = "Service".localized().uppercased()
        scheduleLabel.text = "SCHEDULE".localized()
        addressLabel.text = "Address".localized().uppercased()
        addNewAddressLabel.text = "".localized()
        pickUpLbl.text = "Choose Pickup".localized()
        dropLbl.text = "Choose Delivery".localized()

        
        NavigationDrawer.sharedInstance.initialize(forViewController: self)
        
        if (pickUpLbl.text?.contains(","))! && (dropLbl.text?.contains(","))! {
            scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        }
        
        if (orderPlaceInfo.pickUpTime.characters.count > 0) {
            pickUpLbl.text = Session().savedPickupDate() + " " + orderPlaceInfo.pickUpTime
            pickUpLbl.textColor = .white
            scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        }
        
        if (orderPlaceInfo.dropTime.characters.count > 0) {
            dropLbl.text = Session().savedDropDate() + " " + orderPlaceInfo.dropTime
            dropLbl.textColor = .white
            scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        }
        
       
        if Session.sharedInstance.getAddressDetails().count > 0 {
            
            orderPlaceInfo.pickUpAddress = (((Session().getAddressDetails()).value(forKey: "address") as AnyObject).object(at: 0) as? String)!
            scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        }

    }
    
    //MARK:- IBActions
  
    
    @IBAction func selectPickUpTime(_ sender: Any) {
        
        
        
        let pushToTimeVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimeVC") as! SelectTimeVC
        pushToTimeVC.orderPlaceInfo = orderPlaceInfo
        if (sender as AnyObject).tag == 0 {
            pushToTimeVC.isFromPickUp = true
        } else {
            if (orderPlaceInfo.pickUpTime.characters.count == 0) {
                Helper.sharedInstance.showAlertPopUPWithMessage("Choose your Pickup First", bgColor: UIColor.msgBG(), controller: self)
                return
            }
            pushToTimeVC.isFromPickUp = false
        }
        self.navigationController?.pushViewController(pushToTimeVC, animated: true)
        scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
    //    addressBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        
        

    }
    
    @IBAction func didTapOnAddAddress(_ sender: Any) {
        self.performSegue(withIdentifier: "AddAddressVC", sender: nil)
    }
    
    func leftHeaderBtnAction() {
        self.showDrawer()
    }
    
    func leftHeaderBtnAction2() {
        self.showDrawer()
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func showDrawer(){
        NavigationDrawer.sharedInstance.toggleNavigationDrawer(nil)
    }
    
   func confiInitialView() {
    
    botomViewHeight.constant = CGFloat(bottomViewheight)
    
    Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"SELECT TIMESLOT".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)

    
//    addressListArray = Session().getAddressDetails()
//    print(addressListArray)
    
    
    addressTableView.delegate = self
    addressTableView.dataSource = self
    addressTableView.backgroundColor = UIColor.clear
    addressTableView.tableFooterView = UIView()
    // tableView.separatorStyle = .none
    addressTableView!.register(UINib(nibName: tableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: tableViewCellIdentifier)
    
    if !isFromAddressSelection{
        addressView.isHidden = true
        // pickUpView.isHidden = true
        // deliveryView.isHidden = true
        selectStatusLbl.text = "Select Address".localized().uppercased()

    }else{
        addressView.isHidden = false
        selectStatusLbl.text = "REVIEW ORDER"
        addressBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)

    }
    addNow.isHidden = true

    addNowBtn.clipsToBounds = true
    addNowBtn.layer.cornerRadius = 15
    addNowBtn.layer.borderColor = UIColor.white.cgColor
    addNowBtn.layer.borderWidth = 1.0
    
    serviceLabel.text = "Service".localized().uppercased()
    scheduleLabel.text = "SCHEDULE".localized()
    addressLabel.text = "Address".localized().uppercased()
    addNewAddressLabel.text = "".localized()
    pickUpLbl.text = "Choose Pickup".localized()
    dropLbl.text = "Choose Delivery".localized()
    

    
    //let tempDict:NSDictionary = addressListArray
    //let title = tempDict["address"] as! String
    
//    orderPlaceInfo.pickUpAddress = title
   // orderPlaceInfo.pickUpAddress = "Anand"
    
    }

    @IBAction func didTapOnNext(_ sender: Any) {
        

        if selectStatusLbl.text == "SELECT ADDRESS" {
            if Session().savedPickupDate().characters.count > 0 && Session().savedDropDate().characters.count > 0  {
                selectStatusLbl.text = "REVIEW ORDER"
                addressView.isHidden = false
                scheduleBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
                addressBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
                addressTableView.reloadData()

            }else{
              Helper.sharedInstance.showAlertPopUPWithMessage("Select Pickup and delivery date", bgColor: UIColor.msgBG(), controller: self)
            }
            
        } else {
            
            if (orderPlaceInfo.pickUpAddress.characters.count) > 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
                vc.orderPlaceInfo = orderPlaceInfo
                self.navigationController?.viewControllers = [vc]
            }
            else {
                Helper.sharedInstance.showAlertPopUPWithMessage("Select Address", bgColor: UIColor.msgBG(), controller: self)
            }
        }

    }
    
    
    func callAddressListCell(indexPath:IndexPath) -> AddressCell {
        let addressListCell:AddressCell = addressTableView.dequeueReusableCell(withIdentifier:tableViewCellIdentifier, for: indexPath) as! AddressCell

        addressListCell.selectLabel.text = "Office"
        
        addressListCell.deleteButton.tag = indexPath.row
        
        if Session.sharedInstance.getAddressDetails().count > 0 {
            addressListCell.addressLabel.text = (((Session().getAddressDetails()).value(forKey: "address") as AnyObject).object(at: indexPath.row) as? String)
            
        }
        addressListCell.selectionStyle = .none
        addressListCell.deleteButton.addTarget(self, action: #selector(tapOnDeleteBtn(sender:)), for: .touchUpInside)
        addressListCell.editButton.addTarget(self, action: #selector(tapOnEditBtn(sender:)), for: .touchUpInside)
        
        if selectedIndex == indexPath.row {
            addressListCell.selectBtn.setBackgroundImage(UIImage.init(named: "service_selected"), for: .normal)
        }else{
            addressListCell.selectBtn.setBackgroundImage(UIImage.init(named: "service_deselected"), for: .normal)

        }
        
        return addressListCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return callAddressListCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tempDict = (((Session().getAddressDetails()).value(forKey: "address") as AnyObject).object(at: indexPath.row) as? String)!
        orderPlaceInfo.pickUpAddress = tempDict
        selectedIndex = indexPath.row
        tableView.reloadData()
    }

    @IBAction func tapOnAddNowBtn(_ sender: Any) {
         self.performSegue(withIdentifier: "AddAddressVC", sender: nil)
    }


    @IBAction func tapOnAddbtn(_ sender: Any) {
    
        addNow.isHidden = false
    }
    
    @IBAction func tapOnDeleteBtn(sender :UIButton){
    
        let addressId = (((Session().getAddressDetails()).value(forKey: "address_id") as AnyObject).object(at: sender.tag) as? String)
        
        let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
        let tempDict:NSDictionary = [
            "user_id" : userID,
            "address_id" : addressId!
            ]
        let saveBookingURL = BASE_URL + kDeleteAddress
        Alamofire.request(saveBookingURL, method: .post, parameters: tempDict as? Parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                if response.result.value != nil{
                    print(response.result.value!)
//                    let successResponse = response.result.value as! NSDictionary
//                    if successResponse.value(forKey: "status")as! NSNumber == 1{
//                        let addressArray = successResponse.value(forKey: "details") as! NSArray
//                        print(addressArray)
//                        Session.sharedInstance.setAddressDetails(value: addressArray as NSArray)
//                        self.addressTableView.reloadData()
                    self.callGetAddressAPI()
//                    }
                }
                self.hideActivityIndicator(self.view)
        }
        

   }
    
    @IBAction func tapOnEditBtn(sender :UIButton){
        
        self.performSegue(withIdentifier: "AddAddressVC", sender: nil)

   }
    
    
    //getaddress API
    
    func callGetAddressAPI() {
        
      /*  if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSMutableDictionary = [
                "user_id" : userID,
                ]
            print(tempDict)
            WebserviceManager.sharedInstance.getAddressLIstAPI(getAddressDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
*/
        let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
        let tempDict:NSDictionary = [
            "user_id" : userID,
            ]
        
        let saveBookingURL = BASE_URL + kGetAddressList
        Alamofire.request(saveBookingURL, method: .post, parameters: tempDict as? Parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                if response.result.value != nil{
                    let successResponse = response.result.value as! NSDictionary
                    if successResponse.value(forKey: "status")as! Int == 1{
                        self.addressListArray = successResponse.value(forKey: "details") as! NSArray
                        print(self.addressListArray)
                        Session.sharedInstance.setAddressDetails(value: self.addressListArray as NSArray)
                        self.addressTableView.reloadData()

                    }
                }
                self.hideActivityIndicator(self.view)
        }

    }

}
