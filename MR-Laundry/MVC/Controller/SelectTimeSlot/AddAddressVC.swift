//
//  AddAddressVC.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/13/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {

    
    @IBOutlet weak var homeView: UIView!
    
    @IBOutlet weak var offieceView: UIView!
    
    
    @IBOutlet weak var OtherView: UIView!
    @IBOutlet weak var mapView: UIView!
    
    
    @IBOutlet weak var addressTbl: UITableView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet var flatNoTextField: UITextField!
    @IBOutlet var landmarkTextField: UITextField!
    @IBOutlet var selectLocalityTextField: UITextField!
    @IBOutlet var zipcodeTextField: UITextField!

    
    @IBOutlet var homeViewWidth: NSLayoutConstraint!
    @IBOutlet var officeViewWidth: NSLayoutConstraint!
    @IBOutlet var otherViewWidth: NSLayoutConstraint!
    @IBOutlet var mapViewWidth: NSLayoutConstraint!
    
    var selctedAddressHeader = "Office"
    
    @IBOutlet weak var uncheckImg: UIImageView!
    
    var isboxclicked = Bool()
    var defaultStr:String = ""
    
   let addAddressCellIdentifier = "AddAddressCell"

    var addAddressArrray:[[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configInitialView()
       // self.callUpdateAddressAPI()
       // self.callDeleteAddressAPI()
        defaultStr = "0"
}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        var addressFromMap = UserDefaults.standard.value(forKey: "address")
        
        if addressFromMap == nil {
            addressFromMap = ""
        }
        
        addAddressArrray = [
            ["placeHolder" : "Flat/House No",
             "value" : ""],
            ["placeHolder" : "Street",
             "value" : ""],
            ["placeHolder" : "Area",
             "value" : ""],
            ["placeHolder" : "Address",
             "value" : addressFromMap!],
            ["placeHolder" : "Zipcode",
             "value" : ""]
        ]
        
        addressTbl.reloadData()

    }
    
    func leftHeaderBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }

    func configInitialView() {
        
        homeViewWidth.constant = (deviceSize.width / 4)
        officeViewWidth.constant = homeViewWidth.constant
        otherViewWidth.constant = homeViewWidth.constant
        mapViewWidth.constant = homeViewWidth.constant
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "back_arrow_white", isleftBtnImg2: false, leftBtnName2: "", centerName:"ADD ADDRESS" as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        

        
     
        
        addressTbl.backgroundColor = UIColor.clear
        //addressTbl.tableFooterView = UIView()
        addressTbl.delegate = self
        addressTbl.dataSource = self
        addressTbl.separatorStyle = .none
        addressTbl!.register(UINib(nibName: addAddressCellIdentifier, bundle: nil), forCellReuseIdentifier: addAddressCellIdentifier)
        
        
        saveButton.clipsToBounds = true
        saveButton.layer.cornerRadius = 20
        saveButton.layer.borderColor = UIColor.white.cgColor
        saveButton.layer.borderWidth = 1.0
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 44
        
        
        switch indexPath.row {
        case 2,3:
            return 70
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addAddressArrray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return addAddressCell(indexPath: indexPath)
        
    }
    
    func addAddressCell(indexPath:IndexPath) -> AddAddressCell {
        let addressCell:AddAddressCell = addressTbl.dequeueReusableCell(withIdentifier: addAddressCellIdentifier, for: indexPath) as! AddAddressCell
        let tempDict = addAddressArrray[indexPath.row] as NSDictionary
        let placeHolder = tempDict["placeHolder"] as! String
        let textFieldValue = tempDict["value"] as! String

        
        addressCell.addressText.placeholder = placeHolder
         addressCell.addressText.setTextFieldProperties(txtField: addressCell.addressText, placehodercolor: UIColor.white, placehoderText: addressCell.addressText.placeholder! as NSString, txtcolor: UIColor.white, title: addressCell.addressText.text! as NSString)
        
        addressCell.addressText.delegate = self as? UITextFieldDelegate
        addressCell.addressText.text = textFieldValue
        addressCell.addressText.tag = indexPath.row
        

        
        switch indexPath.row {
        case 0:
            flatNoTextField = addressCell.addressText
        case 1:
            landmarkTextField = addressCell.addressText
        case 2:
            selectLocalityTextField = addressCell.addressText
        case 3:
            zipcodeTextField = addressCell.addressText
        default:
            break
        }

        
        addressCell.layer.backgroundColor = UIColor.clear.cgColor
        addressCell.selectionStyle = .none
        return addressCell
    }
    
    @IBAction func tapOnSaveButton(_ sender: Any) {
        addressValidation()
        
    }
    
    @IBAction func tapOnHomeBtn(_ sender: Any) {
        selctedAddressHeader = "Home"
        homeView.layer.backgroundColor  = UIColor.clear.cgColor
        offieceView.layer.backgroundColor = UIColor.unSelectedViewColor().cgColor
        OtherView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        mapView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
 }
    
    @IBAction func tapOnOfficeBtn(_ sender: Any) {
        selctedAddressHeader = "Office"
        offieceView.layer.backgroundColor  = UIColor.appBGColor().cgColor
        homeView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        OtherView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        mapView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
 }
    
    @IBAction func tapOnOthersBtn(_ sender: Any) {
        selctedAddressHeader = "Others"
        OtherView.layer.backgroundColor  = UIColor.appBGColor().cgColor
        homeView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        offieceView.layer.backgroundColor = UIColor.unSelectedViewColor().cgColor
        mapView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
 }
    
    @IBAction func tapOnMapBtn(_ sender: Any) {
        
        mapView.layer.backgroundColor  = UIColor.appBGColor().cgColor
        homeView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        offieceView.layer.backgroundColor = UIColor.unSelectedViewColor().cgColor
        OtherView.layer.backgroundColor  = UIColor.unSelectedViewColor().cgColor
        
        self.performSegue(withIdentifier: "showMapVC", sender: nil)
 }
    
    @IBAction func tapOnDefaultValueBtn(_ sender: Any) {
        if isboxclicked == true {
            isboxclicked = false
            uncheckImg.image = UIImage(named: "unChecked")
            defaultStr = "0"
        }
        else{
            isboxclicked = true
            uncheckImg.image = UIImage(named: "checked")
            defaultStr = "1"
        }
    }
    
     func addressValidation(){
            
            if (flatNoTextField.text?.characters.count)! <= 0{
                Helper.sharedInstance.showAlertPopUPWithMessage("Enter Flat No", bgColor: UIColor.msgBG(), controller: self)
            } else if (landmarkTextField.text?.characters.count)! <= 0{
                Helper.sharedInstance.showAlertPopUPWithMessage("Enter Landmark", bgColor: UIColor.msgBG(), controller: self)
            }
            else if (selectLocalityTextField.text?.characters.count)! <= 0{
                Helper.sharedInstance.showAlertPopUPWithMessage("Enter Locality", bgColor: UIColor.msgBG(), controller: self)
                }
            else if (zipcodeTextField.text?.characters.count)! <= 0{
                Helper.sharedInstance.showAlertPopUPWithMessage("Enter Zipcode", bgColor: UIColor.msgBG(), controller: self)
            }
            else {
                //Helper.sharedInstance.showAlertPopUPWithMessage("AddressAdded", bgColor: UIColor.msgBG(), controller: self)
                let appendString = "\(flatNoTextField.text!) \(landmarkTextField.text!) \(selectLocalityTextField.text!) \(zipcodeTextField.text!) \(selctedAddressHeader)"
                self.callAddAddressAPI(address: appendString)
        }
        
    }
    
    //MARK:- API Methods
    
    func callAddAddressAPI(address : String) {
        
        UserDefaults.standard.removeObject(forKey: "address")
        
        if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!

            print(address)
            
            let tempDict:NSMutableDictionary = [
                "user_id" : userID,
                "address" : address,
                "default_address" : defaultStr,
                "latitude":"11.79879",
                "longitude":"76.87897"
                
            ]
            print(tempDict)
           // Session.sharedInstance.setAddressDetails(tempDict)
            
            WebserviceManager.sharedInstance.addAddressAPI(addAddressDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    if success {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimeSlot") as! SelectTimeSlot
                        vc.isFromAddressSelection = true
                        self.navigationController?.viewControllers = [vc]
                       // Session.sharedInstance.setAddressDetails(tempDict)

                    }
                    else {
                        self.showAlertView(message: message, controller: self)
                    }

                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
    
    //Mark:- TextField Delegate
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        let txtFieldTag: Int = textField.tag
        let txtValue:String = textField.text!
        
        
        switch txtFieldTag {
        case 0:
            flatNoTextField.text = txtValue
        case 1:
            landmarkTextField.text = txtValue
        case 2:
            selectLocalityTextField.text = txtValue
        case 3:
            zipcodeTextField.text = txtValue
        default:
            break
        }
        
        addAddressArrray[txtFieldTag]["value"] = txtValue
        print("addaddressarray:----->\(addAddressArrray)")
        
        addressTbl.reloadData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }

    /*
    //updateAddress API
    func callUpdateAddressAPI() {
        
        if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSMutableDictionary = [
                "user_id" : userID,
                "address":"0",
                "address_id":"2",
                "default_address":"1"
            ]
            print(tempDict)
            WebserviceManager.sharedInstance.updateAddressAPI(updateAddressDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
//deleteAddress API
    func callDeleteAddressAPI() {
        
        if Reachability.isConnectedToNetwork() == true {
            self.showActivityIndicator(self.view)
            
            let userID: String = (Session.sharedInstance.getUserLoginDetails()["userID"] as? String)!
            
            let tempDict:NSMutableDictionary = [
                "user_id" : userID,
                "address_id":"2"
                ]
        print(tempDict)
            WebserviceManager.sharedInstance.deleteAddressAPI(deleteAddressDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
    }
*/
    
}
