//
//  ForgotPasswordVC.swift
//  MR-Laundry
//
//  Created by LocusTech on 25/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet weak var logoImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(configInitialView), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        
        configInitialView()
    }

    func leftHeaderBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    func configInitialView() {
        Helper().addHeaderPresent(forViewController: self, isleftBtnImg: true, leftBtnName: "back_arrow_white", isleftBtnImg2: false, leftBtnName2: "", centerName:"Forgot Password".localized() as NSString, isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)



        sendBtn.layer.cornerRadius = 20.0
        sendBtn.layer.borderColor = UIColor.white.cgColor
        sendBtn.layer.borderWidth = 1
        sendBtn.clipsToBounds = true
        
        logoImg.layer.cornerRadius = logoImg.frame.size.width / 2
        logoImg.clipsToBounds = true
        
        emailTxt.placeholder = "EMAIL ADDRESS".localized()
        emailTxt.setTextFieldProperties(txtField: emailTxt, placehodercolor: UIColor.white, placehoderText: emailTxt.placeholder! as NSString, txtcolor: UIColor.white, title: emailTxt.text! as NSString)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 15))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 15))
        imageView.image = UIImage(named: "hint_email")  //location
        paddingView.addSubview(imageView)
        emailTxt.leftView = paddingView
        emailTxt.leftViewMode = UITextFieldViewMode.always
        
        sendBtn.setTitle("Send".localized(), for: .normal)

    }
    
    @IBAction func didTapOnSendBtn(_ sender: Any) {
        
        validateForgotPassword()
        
    }
    
    func validateForgotPassword() {
        if (emailTxt.text?.characters.count)! <= 0{
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter your Email", bgColor: UIColor.msgBG(), controller: self)
        } else if Helper().isValidEmail(testStr: emailTxt.text!) == false {
            
            Helper.sharedInstance.showAlertPopUPWithMessage("Enter Valid EmailID", bgColor: UIColor.msgBG(), controller: self)
        }else{
            self.forgotPasswordApi(email: emailTxt.text!)
        }
    }
    
    func forgotPasswordApi(email : String){
        if Reachability.isConnectedToNetwork() == true{
            self.showActivityIndicator(self.view)
            
            
            let tempDict:NSMutableDictionary = [
                "email": email]
            print(tempDict)
            WebserviceManager.sharedInstance.forgotPasswordAPI(forgotPasswordDetails: tempDict, successBlock:
                {[unowned self] (success, message) in
                    
                    if success {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.viewControllers = [vc]
                    }
                    else {
                        self.showAlertView(message: message, controller: self)
                    }
                    self.hideActivityIndicator(self.view)
                    
                }, failureBlock: {[unowned self] (errorMesssage) in
                    print(errorMesssage.description)
                    self.hideActivityIndicator(self.view);
            })
        }
        
    }

}
