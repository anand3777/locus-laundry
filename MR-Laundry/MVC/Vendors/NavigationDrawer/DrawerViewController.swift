//
//  DrawerViewController.swift
//  SWNavigationDrawer
//
//  Created by Anand on 27/08/16.
//  Copyright © 2016 Anand. All rights reserved.
//

import UIKit
var selectedDrawerRow: Int = 1


class DrawerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var menuTblView: UITableView!
    let menuUserProfileCellIdentifier: String = "MenuUserProfileCell"
    var userName = ""
    var userMobileNumber = ""
    var telePhoneCode = ""

    var menuItem:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTblView.dataSource = self
        menuTblView.delegate = self
//        self.view.backgroundColor = UIColor.init(red: 87.0/255.0, green: 165.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        
        menuTblView.register(UINib(nibName: menuUserProfileCellIdentifier, bundle: nil), forCellReuseIdentifier: menuUserProfileCellIdentifier)

        menuTblView.backgroundColor = .clear
        menuTblView.tableFooterView = UIView()
        menuTblView.separatorColor = .white
        menuTblView.isScrollEnabled = false
        

        NotificationCenter.default.addObserver(self, selector: #selector(test), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        
        userName = Session.sharedInstance.getUserLoginDetails().isHaveStringValue("userName") //(Session.sharedInstance.getUserLoginDetails()["userName"] as? String)!
        
        userName = userName.characters.count == 0 ? "Anand" : userName
        
        userMobileNumber = Session.sharedInstance.getUserLoginDetails().isHaveStringValue("mobileNumber")
        userMobileNumber = userMobileNumber.characters.count == 0 ? "+91 9944773777" : userMobileNumber

        telePhoneCode = Session.sharedInstance.getUserLoginDetails().isHaveStringValue("telephoneCode")
        telePhoneCode = telePhoneCode.characters.count == 0 ? "+91" : telePhoneCode

        menuItem = [
            ["title" : "Welcome".localized() + " \n" + userName,
             "image" : "sm_pickup"],
            
            ["title" : "SCHEDULE A PICK UP".localized(),
             "image" : "sm_pickup"],
            
            ["title" : "MY ORDERS".localized(),
             "image" : "sm_myorder"],
            
            [ "title" : "PRICE LIST".localized(),
              "image" : "sm_pricelist"],
            
            ["title" : "INVITE & EARN".localized(),
             "image" : "sm_invite"],
            
            ["title" : "MYCASH WALLET".localized(),
             "image" : "sm_mycash"],
            
            //                ["title" : "OFFER",
            //                 "image" : "sm_myoffer"],
            //
            ["title" : "SUPPORT".localized(),
             "image" : "sm_support"],
            
//            ["title" : "LANGUAGE SETTINGS".localized(),
//             "image" : "sm_support"]
        ]
    }
    
    func test() ->Void {
        menuTblView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 90
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        let tempMenuDict: NSDictionary = menuItem[indexPath.row] as! NSDictionary
        let titleName: String = tempMenuDict["title"] as! String
        let hintImage: String = tempMenuDict["image"] as! String
        
        cell?.imageView?.image = UIImage.init(named: hintImage)
        
        cell?.textLabel?.text = titleName
        cell?.textLabel?.numberOfLines = 2
        
        if indexPath.row == 0 {
            let userProfileCell: MenuUserProfileCell = menuTblView.dequeueReusableCell(withIdentifier: menuUserProfileCellIdentifier, for: indexPath) as! MenuUserProfileCell
            userProfileCell.userNameLbl.text = userName + "\n" + telePhoneCode + " " + userMobileNumber
            
            userProfileCell.userProfileImage.clipsToBounds = true
            userProfileCell.userProfileImage.layer.cornerRadius = userProfileCell.userProfileImage.frame.size.width / 2
            userProfileCell.selectionStyle = .none

            return userProfileCell
        } else {
            cell?.backgroundColor = .clear
        }
        cell?.textLabel?.textColor = UIColor.white

        
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = .zero
        cell?.layoutMargins = .zero

        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        NavigationDrawer.sharedInstance.toggleNavigationDrawer { () -> Void in
            
            if selectedDrawerRow == indexPath.row {
                return
            } else {
                selectedDrawerRow = indexPath.row
                
                switch indexPath.row {
                case 0:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    self.navigationController?.viewControllers = [vc]
                case 1:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.viewControllers = [vc]
                case 2:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVC
                    self.navigationController?.viewControllers = [vc]
                case 3:
                    Session.sharedInstance.saveSelectedServiceIDs(value: [0])
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PriceListVC") as! PriceListVC
                    self.navigationController?.viewControllers = [vc]
                case 4:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "InviteVC") as! InviteVC
                    self.navigationController?.viewControllers = [vc]
                case 5:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyWalletVC") as! MyWalletVC
                    self.navigationController?.viewControllers = [vc]
//                case 6:
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferVC") as! OfferVC
//                    self.navigationController?.viewControllers = [vc]
                case 6:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
                    self.navigationController?.viewControllers = [vc]
                case 7:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
                    self.navigationController?.viewControllers = [vc]

                default:
                    break
                    //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
                    //                self.navigationController?.viewControllers = [vc!]
                }
            }
        }
    }
    
}
