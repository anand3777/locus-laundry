//
//  DemoGlobalData.swift
//  AlamoFireDemo
//
//  Created by Augusta-009 on 6/19/17.
//  Copyright © 2017 Augusta. All rights reserved.
//

import UIKit

class DemoGlobalData: NSObject {
    static let sharedInstance = DemoGlobalData()

    var productListInfo :[Product] = []
    var loginDetailInfo :LoginModel!
    var userDetailInfo : MLUserModel!
    var orderPlaceInfo : MLOrderPlaceModel!
    var forgotPasswordInfo : MLForgotPasswordModel!
    var priceListInfo : MLPriceListModel!
    var selectLaundryInfo : MLSelectLaundryModel!
    var myOrderListInfo : MLMyOrderListModel!
    var addAddressInfo : MLAddAddressModel!
    var cancelOrderInfo : MLCancelOrderModel!
    var getAddressListInfo : MLGetAddressListModel!
    var updateAddressInfo : MLUpdateAddressModel!
    var deleteAddressInfo : MLDeleteAddressModel!
    
}
