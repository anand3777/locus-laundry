//
//  DemoParser.swift
//  AlamoFireDemo
//
//  Created by Augusta-009 on 6/19/17.
//  Copyright © 2017 Augusta. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DemoParser: NSObject {

    var priceListDict:NSDictionary!
    
    func parseProductDetailsList(response: AnyObject, successBlock: kSuccessBlock,
                                 failureBlock : kErrorBlock){
        
        let data : JSON = JSON(response)
        
       if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    /*
                    let prodcutList: Array<JSON> = dict["goal"]!.arrayValue
                    DemoGlobalData.sharedInstance.productListInfo.removeAll()
                    for productDict in prodcutList {
                        let serviceFeature: Dictionary<String, JSON> = productDict.dictionary!
                        DemoGlobalData.sharedInstance.productListInfo.append(Product().parseProductDetails(dict:serviceFeature))
                        
                    }*/
                    successBlock(true, (dict["message"]?.string)!)

                }
                else
                {
                    successBlock(false, (dict["message"]?.string)!)

                }
                
            }
            else
            {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    
    func parseLoginDetailsList(response: AnyObject, successBlock: kSuccessBlock,
                                 failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "true")
                {
                    DemoGlobalData.sharedInstance.userDetailInfo  = MLUserModel().parseUserDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                } else if (code == "5" || code == "true") {
                    DemoGlobalData.sharedInstance.loginDetailInfo  = LoginModel().parseLoginDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)

                }
                else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    
    func parseOTPDetails(response: AnyObject, successBlock: kSuccessBlock,
                               failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.loginDetailInfo  = LoginModel().parseLoginDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    
    func userDetails(response: AnyObject, successBlock: kSuccessBlock,
                               failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.userDetailInfo  = MLUserModel().parseUserDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }

    
    func parseForgotPasswordDetails(response: AnyObject, successBlock: kSuccessBlock,
                                    failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.forgotPasswordInfo  = MLForgotPasswordModel().forgotPasswordDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }

    func parsePriceListDetails(response: AnyObject, successBlock: kSuccessBlock,
                                    failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                priceListDict = dict as NSDictionary!
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.priceListInfo  = MLPriceListModel().priceListDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
        
    }

    func parseSelectLaundryDetails(response: AnyObject, successBlock: kSuccessBlock,
                               failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "true")
                {
                    DemoGlobalData.sharedInstance.selectLaundryInfo  = MLSelectLaundryModel().selectLaundryDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }

    func parseMyOrderListDetails(response: AnyObject, successBlock: kSuccessBlock,
                                   failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let dicttt = dict
                
//                Helper.sharedInstance.saveOrderList(value: dicttt as NSDictionary)
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.myOrderListInfo  = MLMyOrderListModel().userDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    func parseAddAddressDetails(response: AnyObject, successBlock: kSuccessBlock,
                                 failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.addAddressInfo  = MLAddAddressModel().addAddressDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    func parseCancelOrderDetails(response: AnyObject, successBlock: kSuccessBlock,
                                failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.cancelOrderInfo  = MLCancelOrderModel().CancelOrderDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }

    func parseGetAddressListDetails(response: AnyObject, successBlock: kSuccessBlock,
                                 failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
               // Session.sharedInstance.setAddressDetails(value: dict as NSDictionary)

                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.getAddressListInfo = MLGetAddressListModel().getAddressDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    func parseUpdateAddressDetails(response: AnyObject, successBlock: kSuccessBlock,
                                    failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.updateAddressInfo = MLUpdateAddressModel().updateAddressDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }
    func parseDeleteAddressDetails(response: AnyObject, successBlock: kSuccessBlock,
                                   failureBlock : kErrorBlock){
        let data : JSON = JSON(response)
        
        if data != nil  {
            
            if let dict : Dictionary<String, JSON> = data.dictionary {
                print("dict:---->\(dict)")
                
                let code = dict["status"]?.stringValue
                if(code == "1" || code == "5" || code == "true")
                {
                    DemoGlobalData.sharedInstance.deleteAddressInfo = MLDeleteAddressModel().deleteAddressDetails(dict: dict)
                    successBlock(true, (dict["message"]?.string)!)
                    
                }  else {
                    successBlock(false, (dict["message"]?.string)!)
                }
            } else {
                successBlock(false, "Services features list empty.")
            }
        }
    }

}
