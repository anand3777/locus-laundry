//
//  AlertPopUpView.swift
//  RippleJump
//
//  Created by Augusta-GauthamIOS on 01/12/16.
//  Copyright © 2016 Augusta. All rights reserved.
//

import UIKit

class AlertPopUpView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet var titleLabel:UILabel!
    
    override open func awakeFromNib()
    {
        super.awakeFromNib()
        // self.setFonts()
    }
    

    func showAlertPopUp(_ title:String,bgColor:UIColor?, forViewController:UIViewController)  {
        titleLabel.text = title
        if bgColor == nil {
            self.backgroundColor = UIColor.black
        }
        else {
            self.backgroundColor = bgColor
        }
        self.frame = CGRect(x:0, y:-40,width: forViewController.view.frame.size.width,height: 60)
       
      //  UIApplication.shared.isStatusBarHidden = true
       
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            
            self.frame = CGRect(x:0, y:0,width: forViewController.view.frame.size.width,height: 60)
            
          //  self.frame = CGRectMake(0, 0, forViewController.view.frame.size.width, self.frame.size.height);
        }, completion: { (finished) in
            
            UIView.animate(withDuration: 0.5, delay: 3, options: .curveEaseIn, animations: {
                self.frame = CGRect(x:0, y:-40,width: forViewController.view.frame.size.width,height: 60)
                
                //  self.frame = CGRectMake(0, 0, forViewController.view.frame.size.width, self.frame.size.height);
            }, completion: { (finished) in
                self.removeFromSuperview()
                // UIApplication.shared.isStatusBarHidden = false
            })
            
        })
    }
    
    func showAlertPopUpMiddle(_ title:String,bgColor:UIColor?, forViewController:UIViewController, viewDismissTiming: CGFloat)  {
        titleLabel.text = title
        if bgColor == nil {
            self.backgroundColor = UIColor.black
        }
        else {
            self.backgroundColor = bgColor
        }
        self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 5)
        titleLabel.frame = CGRect(x: 10, y: 0, width: (deviceSize.width - 20), height: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 40)
            self.titleLabel.frame = CGRect(x: 10, y: 0, width: (deviceSize.width - 20), height: 40)
        }, completion: { (finished) in
            
            UIView.animate(withDuration: 0.5, delay: TimeInterval(viewDismissTiming), options: .curveEaseIn, animations: {
                self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 5)
                self.titleLabel.frame = CGRect(x: 10, y: 0, width: (deviceSize.width - 20), height: 0)

            }, completion: { (finished) in
                self.removeFromSuperview()
            })
            
        })
    }
    
    /*
    func showAlertPopUpMiddle(_ title:String,bgColor:UIColor?, forViewController:UIViewController)  {
      
        msgTop.constant = 0

        titleLabel.text = title
        if bgColor == nil {
            self.backgroundColor = UIColor.black
        }
        else {
            self.backgroundColor = bgColor
        }
        self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 0)

        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 40)
        }, completion: { (finished) in
            UIView.animate(withDuration: 0.5, delay: 3, options: .curveEaseIn, animations: {
                self.frame = CGRect(x:0, y:63,width: forViewController.view.frame.size.width,height: 0)
            }, completion: { (finished) in
                self.removeFromSuperview()
            })
        })
    }
    */
    
    
//    func removeAlert() {
//        [UIView .animate(withDuration: 0.5, delay: 0.1, options: UIViewAnimationCurve, animations: {
//            self.postStatusView.frame = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
//        }, completion: { (finished) in
//            self.removeFromSuperview()
//        })]
//
//    }
    
}
