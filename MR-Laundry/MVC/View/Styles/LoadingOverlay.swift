//
//  LoadingOverlay.swift
//  MR-Laundry
//
//  Created by LocusTech on 14/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//
//  Usage:
//
//  # Show Overlay
//  LoadingOverlay.shared.showOverlay(self.navigationController?.view)
//
//  # Hide Overlay
//  LoadingOverlay.shared.hideOverlayView()

import UIKit
import Foundation


open class LoadingOverlay{
    var isLoading:Bool = false
    var isLoadingWithOutOverLay:Bool = false
    var overlayView = UIView()
     let overlayBGView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    
    open func showOverlay(_ view: UIView) {
        isLoading = true
        view.frame = UIScreen.main.bounds
        print(view.frame)
        overlayBGView.frame = UIScreen.main.bounds
        overlayBGView.backgroundColor = UIColor.clear
        
        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.center = view.center
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        overlayBGView.addSubview(overlayView)
        UIApplication.shared.keyWindow?.addSubview(overlayBGView);
       // view.addSubview(overlayBGView)
        
        activityIndicator.startAnimating()
    }
    
    open func showLoadingWithOutOverlay(_ view: UIView) {
        isLoadingWithOutOverLay = true
        
//        overlayBGView.frame = view.frame
//        overlayBGView.backgroundColor = UIColor.clear
        
        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.center = view.center
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
       // overlayBGView.addSubview(overlayView)
        view.addSubview(overlayView)
        
        activityIndicator.startAnimating()
    }
    
    
    open func hideOverlayView() {
        isLoading = false
        activityIndicator.stopAnimating()
        overlayBGView.removeFromSuperview()
    }
    
    open func hideLoadingWithOutOverlayView() {
        isLoadingWithOutOverLay = false
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
}
