//
//  PriceListSessionHeaderView.swift
//  MR-Laundry
//
//  Created by LocusTech on 29/05/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class PriceListSessionHeaderView: UIView {

    @IBOutlet var headerTitleLbl: UILabel!
    @IBOutlet var infoBtn: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
