//
//  HeaderView.swift
//  MR-Laundry
//
//  Created by LocusTech on 14/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    
    var callingViewController:AnyObject!
    var callingNavigiationController:UINavigationController!
    var isHaveWaringAlert:Bool!
    var isPresentedVc:Bool!
    
     @IBOutlet var leftBtn: UIButton!
     @IBOutlet var leftBtn2: UIButton!
     @IBOutlet var centerLabel: UILabel!
     @IBOutlet var rightBtn2: UIButton!
     @IBOutlet var rightBtn: UIButton!

    override internal func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override internal func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    func setFrame(_ customFrame:CGRect,
                  isFooterSeprator:Bool,
                  isleftBtnImg:Bool,
                  leftBtnName:NSString,
                  isleft2BtnImg:Bool,
                  leftBtn2Name:NSString,
                  centerName:NSString,
                  isRightBtnImg:Bool,
                  rightBtnName:NSString,
                  isRight2BtnImg:Bool,
                  rightBtn2Name:NSString,
                  fromViewController:AnyObject,
                  fromNavigation:UINavigationController) {
        
        callingViewController = fromViewController
        callingNavigiationController = fromNavigation
        
        
        leftBtn.isHidden =  true
        leftBtn2.isHidden =  true
        centerLabel.isHidden =  true
        leftBtn2.isHidden =  true
        rightBtn.isHidden =  true
        
        if(leftBtnName.length > 0) {
            
            if isleftBtnImg {
                self.setBtnImage(btnImageName: leftBtnName, forBtn: leftBtn)
            }
            else {
                leftBtn.setTitle(leftBtnName as String, for: .normal)
            }
            
            leftBtn.isHidden =  false
            if(callingViewController .responds(to: Selector(("leftHeaderBtnAction")))) {
            leftBtn.addTarget(callingViewController, action: Selector(("leftHeaderBtnAction")), for: .touchUpInside)
            }
        }
        
        if(leftBtn2Name.length > 0) {
            
            if isleft2BtnImg {
                self.setBtnImage(btnImageName: leftBtn2Name, forBtn: leftBtn2)
            }
            else {
                leftBtn2.setTitle(leftBtn2Name as String, for: .normal)
            }
            
            leftBtn2.isHidden =  false
            if(callingViewController .responds(to: Selector(("leftHeaderBtnAction2")))) {
                leftBtn2.addTarget(callingViewController, action: Selector(("leftHeaderBtnAction2")), for: .touchUpInside)
            }
        }
        
        if(centerName.length > 0) {
            centerLabel.text = centerName as String
            centerLabel.isHidden =  false
        }

        if(rightBtnName.length > 0) {
            if isRightBtnImg {
                self.setBtnImage(btnImageName: rightBtnName, forBtn: rightBtn)
            }
            else {
                rightBtn.setTitle(rightBtnName as String, for: .normal)
            }
            rightBtn.isHidden =  false
            if(callingViewController .responds(to: Selector(("rightHeaderBtnAction")))) {
            rightBtn.addTarget(callingViewController, action: Selector(("rightHeaderBtnAction")), for: .touchUpInside)
            }
        }
        
        if(rightBtn2Name.length > 0) {
            if isRight2BtnImg {
                self.setBtnImage(btnImageName: rightBtn2Name, forBtn: rightBtn2)
            }
            else {
                rightBtn2.setTitle(rightBtn2Name as String, for: .normal)
            }
            rightBtn2.isHidden =  false
            if(callingViewController .responds(to: Selector(("rightHeaderBtnAction2")))) {
                rightBtn2.addTarget(callingViewController, action: Selector(("rightHeaderBtnAction2")), for: .touchUpInside)
            }
        }
        self.frame = customFrame;
    }

    
    // For headerView
    func setTableHeaderViewFrame(_ customFrame:CGRect, isFooterSeprator:Bool, fromViewController:AnyObject, fromNavigation:UINavigationController) {
        callingViewController = fromViewController
        callingNavigiationController = fromNavigation
        self.frame = customFrame;
    }

    
    func removeHeaderView(fromVC:UIViewController)  {
    }

    
    func setBtnImage(btnImageName:NSString,forBtn:UIButton)  {
        let image = UIImage(named: btnImageName as String)
        forBtn .setImage(image, for: .normal)
        forBtn .setImage(image, for: .selected)
    }
    
    
    func setFrame(_ customFrame:CGRect,
                  isFooterSeprator:Bool,
                  isleftBtnImg:Bool,
                  leftBtnName:NSString,
                  isleft2BtnImg:Bool,
                  leftBtn2Name:NSString,
                  centerName:NSString,
                  isRightBtnImg:Bool,
                  rightBtnName:NSString,
                  isRight2BtnImg:Bool,
                  rightBtn2Name:NSString,
                  fromViewController:AnyObject) {
        
        callingViewController = fromViewController
        
        
        leftBtn.isHidden =  true
        leftBtn2.isHidden =  true
        centerLabel.isHidden =  true
        rightBtn.isHidden =  true
        rightBtn2.isHidden =  true
        
        if(leftBtnName.length > 0) {
            
            if isleftBtnImg {
                self.setBtnImage(btnImageName: leftBtnName, forBtn: leftBtn)
            }
            else {
                leftBtn.setTitle(leftBtnName as String, for: .normal)
            }
            
            leftBtn.isHidden =  false
            if(callingViewController .responds(to: Selector(("leftHeaderBtnAction")))) {
                leftBtn.addTarget(callingViewController, action: Selector(("leftHeaderBtnAction")), for: .touchUpInside)
            }
        }
        
        if(leftBtn2Name.length > 0) {
            
            if isleft2BtnImg {
                self.setBtnImage(btnImageName: leftBtn2Name, forBtn: leftBtn2)
            }
            else {
                leftBtn2.setTitle(leftBtn2Name as String, for: .normal)
            }
            
            leftBtn2.isHidden =  false
            if(callingViewController .responds(to: Selector(("leftHeaderBtnAction2")))) {
                leftBtn2.addTarget(callingViewController, action: Selector(("leftHeaderBtnAction2")), for: .touchUpInside)
            }
        }
        
        if(centerName.length > 0) {
            centerLabel.text = centerName as String
            centerLabel.isHidden =  false
        }
        
        if(rightBtnName.length > 0) {
            if isRightBtnImg {
                self.setBtnImage(btnImageName: rightBtnName, forBtn: rightBtn)
            }
            else {
                rightBtn.setTitle(rightBtnName as String, for: .normal)
            }
            rightBtn.isHidden =  false
            if(callingViewController .responds(to: Selector(("rightHeaderBtnAction")))) {
                rightBtn.addTarget(callingViewController, action: Selector(("rightHeaderBtnAction")), for: .touchUpInside)
            }
        }
        
        if(rightBtn2Name.length > 0) {
            if isRight2BtnImg {
                self.setBtnImage(btnImageName: rightBtn2Name, forBtn: rightBtn2)
            }
            else {
                rightBtn2.setTitle(rightBtn2Name as String, for: .normal)
            }
            rightBtn2.isHidden =  false
            if(callingViewController .responds(to: Selector(("rightHeaderBtnAction2")))) {
                rightBtn2.addTarget(callingViewController, action: Selector(("rightHeaderBtnAction2")), for: .touchUpInside)
            }
        }
        self.frame = customFrame;
    }
    
}
