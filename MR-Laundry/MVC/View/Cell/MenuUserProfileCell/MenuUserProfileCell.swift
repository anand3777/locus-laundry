//
//  MenuUserProfileCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/11/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class MenuUserProfileCell: UITableViewCell {
    @IBOutlet var userProfileImage: UIImageView!
    @IBOutlet var userNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
