//
//  SelectServiceCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 18/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SelectServiceCell: UITableViewCell {
    @IBOutlet var selectServiceHeight: NSLayoutConstraint!
    @IBOutlet var selectService: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
