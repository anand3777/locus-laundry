//
//  HomeScreenServiceCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 31/05/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class HomeScreenServiceCell: UITableViewCell {
    @IBOutlet var serviceViewWidth: NSLayoutConstraint!
    @IBOutlet var service1Height: NSLayoutConstraint!
    @IBOutlet var service2Height: NSLayoutConstraint!
    @IBOutlet var serviceBtn1Width: NSLayoutConstraint!
    @IBOutlet var serviceBtn1Height: NSLayoutConstraint!
    @IBOutlet var serviceBtn2Width: NSLayoutConstraint!
    @IBOutlet var serviceBtn2Height: NSLayoutConstraint!
    @IBOutlet var service1Top: NSLayoutConstraint!
    @IBOutlet var service2Top: NSLayoutConstraint!
    
    @IBOutlet var service1Img: UIImageView!
    @IBOutlet var service1Btn: UIButton!
    @IBOutlet var service1Lbl: UILabel!
    @IBOutlet var service2Img: UIImageView!
    @IBOutlet var service2Btn: UIButton!
    @IBOutlet var service2Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
