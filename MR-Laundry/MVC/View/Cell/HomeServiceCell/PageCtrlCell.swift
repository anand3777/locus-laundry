//
//  PageCtrlCell.swift
//  SamplePageController
//
//  Created by Anand on 30/05/17.
//  Copyright © 2017 Anand. All rights reserved.
//

import UIKit

class PageCtrlCell: UITableViewCell {
    @IBOutlet var collectionHeight: NSLayoutConstraint!

    @IBOutlet var serivcePageCtrl: UICollectionView!
    @IBOutlet var pageCtrl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
