
//
//  SelectLaundryCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SelectLaundryCell: UITableViewCell {

    @IBOutlet var laundryNameLbl: UILabel!
    @IBOutlet var shopimage: UIImageView!
    @IBOutlet var selectShop: UIButton!

    /*
    @IBOutlet var star1Img: UIImageView!
    @IBOutlet var star2img: UIImageView!
    @IBOutlet var star3img: UIImageView!
    @IBOutlet var star4img: UIImageView!
    @IBOutlet var star5img: UIImageView!
    */
    
    @IBOutlet var selectLaundrBtn: UIButton!
    @IBOutlet var floatRatingView: FloatRatingView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectShop.setTitle("Select Shop".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
