//
//  ReviewAddressCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ReviewAddressCell: UITableViewCell {
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var offiveTitle: UILabel!
    @IBOutlet var addressTitleLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addressTitleLabel.text = "Address".localized().uppercased()
        offiveTitle.text = "Office".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
