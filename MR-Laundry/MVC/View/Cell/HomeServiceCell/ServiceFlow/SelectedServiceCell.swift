//
//  SelectedServiceCell.swift
//  MR-Laundry
//
//  Created by Gowthaman on 02/09/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SelectedServiceCell: UITableViewCell {
    
    @IBOutlet var titleLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
