//
//  ReviewCouponCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ReviewCouponCell: UITableViewCell {
    @IBOutlet var cashBtn: UIButton!
    @IBOutlet var cardBtn: UIButton!
    @IBOutlet var payPalBtn: UIButton!
    @IBOutlet var walletBtn: UIButton!
    @IBOutlet var optionsTitle: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
}
