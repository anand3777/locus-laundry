//
//  ReviewScheduleCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ReviewScheduleCell: UITableViewCell {
    
    @IBOutlet var pickupLabel:UILabel!
    @IBOutlet var delivetLabel:UILabel!
    @IBOutlet var title:UILabel!
    @IBOutlet var orderId:UILabel!

    
    @IBOutlet var pickupValue:UILabel!
    @IBOutlet var delivetValue:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pickupLabel.text = "Pickup".localized()
        delivetLabel.text = "Delivery".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
