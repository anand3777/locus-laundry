//
//  MyOrderCell.swift
//  SegmentController
//
//  Created by Anand on 22/06/17.
//  Copyright © 2017 Anand. All rights reserved.
//

import UIKit

class MyOrderCell: UITableViewCell {
    @IBOutlet var view1Width: NSLayoutConstraint!
    @IBOutlet var view2Width: NSLayoutConstraint!
    @IBOutlet var view3Width: NSLayoutConstraint!
    @IBOutlet var view4Width: NSLayoutConstraint!

    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    @IBOutlet weak var deliveryDate: UILabel!
    @IBOutlet weak var orderPlace: UILabel!
    
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*
        pickUpLabel1.text = "Pickup By".localized()
        pickUpLabel2.text = "Pickup By".localized()
        pickUpLabel3.text = "Pickup By".localized()
        pickUpLabel4.text = "Pickup By".localized()
        cancelledLabel.text = "Cancelled".localized()
        */
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
