//
//  ProfileAddressCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 20/12/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class ProfileAddressCell: UITableViewCell {
    @IBOutlet var addressTitleLbl: UILabel!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var sepratorLeading: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
