//
//  OrderCancelReasonCell.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/6/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class OrderCancelReasonCell: UITableViewCell {


    @IBOutlet weak var orderCancelLbl: UILabel!
    
    @IBOutlet weak var selectionBtn: UIButton!

    
}
