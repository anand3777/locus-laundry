//
//  CreateAccountCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 22/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class CreateAccountCell: UITableViewCell {
    @IBOutlet var createAccountBtn: UIButton!
    @IBOutlet var signInLbl: UILabel!
    @IBOutlet var signInBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        createAccountBtn.setTitle("CREATE ACCOUNT".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
