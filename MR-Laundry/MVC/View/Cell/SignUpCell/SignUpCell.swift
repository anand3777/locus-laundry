//
//  SignUpCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 22/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class SignUpCell: UITableViewCell {
    @IBOutlet var userValueTxt: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
