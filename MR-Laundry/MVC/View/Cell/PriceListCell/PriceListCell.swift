//
//  PriceListCell.swift
//  MR-Laundry
//
//  Created by LocusTech on 29/05/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit

class PriceListCell: UITableViewCell {
    @IBOutlet var catImage: UIImageView!
    @IBOutlet var catTitleLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var quantityCountLbl: UILabel!
    @IBOutlet weak var reduceProductBtn: UIButton!
    @IBOutlet weak var addProductBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        catTitleLbl.text = "Wash & Fold (per kg)".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
