//
//  MLUserModel.swift
//  MR-Laundry
//
//  Created by LocusTech on 11/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import SwiftyJSON

class MLUserModel: NSObject {
    
    var userName: String?
    var userID: String?
    var userTelephoneCode: String?
    var userMobileNumber: String?
    var userEmailID: String?
    var walletAmount: String?
    var referallCode: String?

    
    
    func parseUserDetails(dict : [String : JSON]) -> MLUserModel {
        //print("parseUserDetails dict:----->\(dict)")
        
        let userModelObj = MLUserModel()
        
        if let userName = dict["name"]?.stringValue {
            userModelObj.userName = userName
        }
        if let userIDString = dict["user_id"]?.stringValue {
            userModelObj.userID = userIDString
        }
        if let telephoneCodeString = dict["telephone_code"]?.stringValue {
            userModelObj.userTelephoneCode = telephoneCodeString
        }
        if let mobileNumberString = dict["mobile_number"]?.stringValue {
            userModelObj.userMobileNumber = mobileNumberString
        }
        if let emailIDString = dict["email"]?.stringValue {
            userModelObj.userEmailID = emailIDString
        }
        
        if let walletString = dict["wallet_amount"]?.stringValue {
            userModelObj.walletAmount = walletString
        }
        
        if let referalString = dict["referral_code"]?.stringValue {
            userModelObj.referallCode = referalString
        }

        return userModelObj
    }
}
