//
//  MLDeleteAddressModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLDeleteAddressModel: NSObject {
    var userID: String = ""
    var addressID: String = ""
    
    func deleteAddressDetails(dict : [String : JSON]) -> MLDeleteAddressModel{
        print("deleteAddressDetails dict:----->\(dict)")
        
        let deleteAddressObj = MLDeleteAddressModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            deleteAddressObj.userID = userIDString
        }
        
        if let addressIDString = dict["address"]?.stringValue {
            deleteAddressObj.addressID = addressIDString
        }
    
        return deleteAddressObj
    }
    

}
