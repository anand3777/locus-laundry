//
//  MLSelectLaundryModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/1/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLSelectLaundryModel: NSObject {

    var storeRating: String = ""
    var storeAddress: String = ""
    var storeLatitude: String = ""
    var storeEmail: String = ""
    var storeName: String = ""
    var storeLongitude: String = ""
    var storeID: String = ""
    var storeLogo: String = ""
    var storePhone: String = ""

    
    
    var storeArray:NSArray = NSArray()
    
    func selectLaundryDetails(dict : [String : JSON]) -> MLSelectLaundryModel {
        print("selectLaundryDetails dict:----->\(dict)")
        
        let selectLaundryObj = MLSelectLaundryModel()
        
//        if let store_ratings = dict["store_ratings"]?.stringValue {
//            selectLaundryObj.userID = store_ratings
        
//        let selectLaundryObj = MLSelectLaundryModel()
        
        if let ratingValue = dict["store_ratings"]?.stringValue {
            selectLaundryObj.storeRating = ratingValue
        }
        
        if let addressValue = dict["store_address"]?.stringValue {
            selectLaundryObj.storeAddress = addressValue
        }
        
        if let latitudeString = dict["store_longitude"]?.stringValue {
            selectLaundryObj.storeLatitude = latitudeString
        }
        
        if let storeEmailString = dict["store_email"]?.stringValue {
            selectLaundryObj.storeEmail = storeEmailString
        }
        
        if let storeNameValue = dict["store_name"]?.stringValue {
            selectLaundryObj.storeName = storeNameValue
        }
        
        if let longitudeString = dict["store_latitude"]?.stringValue {
            selectLaundryObj.storeLongitude = longitudeString
        }
        
        if let storeIDString = dict["store_id"]?.stringValue {
            selectLaundryObj.storeID = storeIDString
        }
       
        if let storeLogoString = dict["store_logo"]?.stringValue {
            selectLaundryObj.storeLogo = storeLogoString
        }
        
        if let storePhoneString = dict["store_phone"]?.stringValue {
            selectLaundryObj.storePhone = storePhoneString
        }

        
        return selectLaundryObj
        
    }

}

