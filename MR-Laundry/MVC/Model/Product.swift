//
//  Product.swift
//  AlamoFireDemo
//
//  Created by Augusta-009 on 6/19/17.
//  Copyright © 2017 Augusta. All rights reserved.
//

import SwiftyJSON

struct Product {
    var GOAL: String?
    var COUNT: String?
    
    
    func parseProductDetails(dict : [String : JSON]) -> Product {
        
        var productObj = Product()
        
        if let goal = dict["GOAL"]?.stringValue {
            productObj.GOAL = goal
        }
        if let count = dict["COUNT"]?.stringValue {
            productObj.COUNT = count
        }
        return productObj
    }
}
