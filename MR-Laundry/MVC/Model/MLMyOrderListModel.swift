//
//  MLMyOrderListModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLMyOrderListModel: NSObject {

    var dropLongitude: String = ""
    var pickUpTimeFrom: String = ""
    var dropTimeTo: String = ""
    var dropLocaiton: String = ""
    var pickUpTimeTo: String = ""
    var pickUpLocaiton: String = ""
    var couponCode: String = ""
    var bookingKey: String = ""
    var paymentType: String = ""
    var bookingID: String = ""
    var serviceID: String = ""
    var address: String = ""
    var fare: String = ""
    var bookingStatus: String = ""
    var pickUplatitude: String = ""
    var createdDate: String = ""
    var dropLatitude: String = ""
    var dropTimeFrom: String = ""
    var pickUpLongitude: String = ""


    func userDetails(dict : [String : JSON]) -> MLMyOrderListModel{
        print("userDetails dict:----->\(dict)")

        let orderDetailObj = MLMyOrderListModel()
        
        if let pickUpLongitudeStr = dict["pickup_longitude"]?.stringValue {
            orderDetailObj.pickUpLongitude = pickUpLongitudeStr
        }
        
        if let dropTimeFromStr = dict["drop_time_from"]?.stringValue {
            orderDetailObj.dropTimeFrom = dropTimeFromStr
        }
        
        if let dropLatitudeStr = dict["drop_latitude"]?.stringValue {
            orderDetailObj.dropLatitude = dropLatitudeStr
        }
        
        if let createdDateStr = dict["created_date"]?.stringValue {
            orderDetailObj.createdDate = createdDateStr
        }
        
        if let pickUpLatitudeStr = dict["pickup_latitude"]?.stringValue {
            orderDetailObj.pickUplatitude = pickUpLatitudeStr
        }

        if let bookingStatusStr = dict["booking_status"]?.stringValue {
            orderDetailObj.bookingStatus = bookingStatusStr
        }
        
        if let fareStr = dict["fare"]?.stringValue {
            orderDetailObj.fare = fareStr
        }
        
        if let addressStr = dict["address"]?.stringValue {
            orderDetailObj.address = addressStr
        }
        
        if let serviceIDStr = dict["service_id"]?.stringValue {
            orderDetailObj.serviceID = serviceIDStr
        }
        
        if let bookingIDStr = dict["booking_id"]?.stringValue {
            orderDetailObj.bookingID = bookingIDStr
        }
        
        if let paymentTypeStr = dict["payment_type"]?.stringValue {
            orderDetailObj.paymentType = paymentTypeStr
        }
        
        if let bookingKeyStr = dict["booking_key"]?.stringValue {
            orderDetailObj.bookingKey = bookingKeyStr
        }
        
        if let couponCodeStr = dict["coupon_code"]?.stringValue {
            orderDetailObj.couponCode = couponCodeStr
        }
        
        if let pickUpLocaitonStr = dict["pickup_location"]?.stringValue {
            orderDetailObj.pickUpLocaiton = pickUpLocaitonStr
        }
        
        if let pickUpTimeToStr = dict["pickup_time_to"]?.stringValue {
            orderDetailObj.pickUpTimeTo = pickUpTimeToStr
        }

        if let dropLocationStr = dict["drop_location"]?.stringValue {
            orderDetailObj.dropLocaiton = dropLocationStr
        }
        
        if let dropTimeStr = dict["drop_time_to"]?.stringValue {
            orderDetailObj.dropTimeTo = dropTimeStr
        }
        
        if let pickUpTimeStr = dict["pickup_time_from"]?.stringValue {
            orderDetailObj.pickUpTimeFrom = pickUpTimeStr
        }
        
        if let dropLongitudeString = dict["drop_longitude"]?.stringValue {
            orderDetailObj.dropLongitude = dropLongitudeString
        }
        
        return orderDetailObj
    }
    
}
