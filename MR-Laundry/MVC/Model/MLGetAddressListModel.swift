//
//  MLGetAddressListModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLGetAddressListModel: NSObject {

    var userID: String = ""
    
    func getAddressDetails(dict : [String : JSON]) -> MLGetAddressListModel{
        print("getAddressDetails dict:----->\(dict)")
        
        let getAddressDetailsObj = MLGetAddressListModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            getAddressDetailsObj.userID = userIDString
        }
        return getAddressDetailsObj
    }
    

}
