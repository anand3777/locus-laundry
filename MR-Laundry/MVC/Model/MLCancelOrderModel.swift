//
//  MLCancelOrderModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLCancelOrderModel: NSObject {
    
    var bookingID: String = ""
    var userID: String = ""
    
    func CancelOrderDetails(dict : [String : JSON]) -> MLCancelOrderModel{
        print("CancelOrderDetails dict:----->\(dict)")
        
        let cancelOrderObj = MLCancelOrderModel()
        
        if let bookingIDString = dict["booking_id"]?.stringValue {
            cancelOrderObj.bookingID = bookingIDString
        }
        
        if let userIDString = dict["user_id"]?.stringValue {
            cancelOrderObj.userID = userIDString
        }
        
        return cancelOrderObj
    }
}
