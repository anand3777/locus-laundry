//
//  MLOrderPlaceModel.swift
//  MR-Laundry
//
//  Created by LocusTech on 13/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLOrderPlaceModel: NSObject {

    var userID: String = ""
    var serviceNames: String = ""
    var serviceIDs: String = ""
    var shopName: String = ""
    var pickUpTime: String = ""
    var dropTime: String = ""

    var pickUpDate: String = ""
    var pickUpAPITime: String = ""
    var dropDate: String = ""
    var dropAPITime: String = ""

    var pickUpAddress: String = ""
    var dropAddress: String = ""

  //  var serviceNamesArray: NSArray = []

    

    func orderDetails(dict : [String : JSON]) -> MLOrderPlaceModel {
      //  print("orderDetails dict:----->\(dict)")
        
        let orderPlaceObj = MLOrderPlaceModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            orderPlaceObj.userID = userIDString
        }
        
        if let serviceNameString = dict["service_name"]?.stringValue {
            orderPlaceObj.serviceNames = serviceNameString
        }
        
        if let serviceIDsString = dict["service_name"]?.stringValue {
            orderPlaceObj.serviceIDs = serviceIDsString
        }
        
        if let shopNameString = dict["shop_name"]?.stringValue {
            orderPlaceObj.shopName = shopNameString
        }
        
        if let pickUpTimeString = dict["pickup_time"]?.stringValue {
            orderPlaceObj.pickUpTime = pickUpTimeString
        }
        
        if let dropTimeString = dict["drop_time"]?.stringValue {
            orderPlaceObj.dropTime = dropTimeString
        }
        
        if let pickUpDateString = dict["pickup_date"]?.stringValue {
            orderPlaceObj.pickUpDate = pickUpDateString
        }
        
        if let dropDateString = dict["drop_date"]?.stringValue {
            orderPlaceObj.dropDate = dropDateString
        }
        
        if let pickUpAddressString = dict["pickup_address"]?.stringValue {
            orderPlaceObj.pickUpAddress = pickUpAddressString
        }
        
        if let dropAddressString = dict["drop_address"]?.stringValue {
            orderPlaceObj.dropAddress = dropAddressString
        }
        
        if let pickUpAPITimeString = dict["pickup_apitime"]?.stringValue {
            orderPlaceObj.pickUpAPITime = pickUpAPITimeString
        }
        
        if let dropAPITimeString = dict["drop_apitime"]?.stringValue {
            orderPlaceObj.dropAPITime = dropAPITimeString
        }
        

        return orderPlaceObj
    }

}
