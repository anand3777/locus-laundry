//
//  MLAddAddressModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLAddAddressModel: NSObject {

    
    var userID: String = ""
    var address: String = ""
    var defaultAddress: String = ""
    
    func addAddressDetails(dict : [String : JSON]) -> MLAddAddressModel{
        print("addAddressDetails dict:----->\(dict)")
        
        let addAddressObj = MLAddAddressModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            addAddressObj.userID = userIDString
        }
        if let addressString = dict["address"]?.stringValue {
            addAddressObj.address = addressString
        }
        if let default_addressString = dict["default_address"]?.stringValue {
            addAddressObj.defaultAddress = default_addressString
        }
        
        return addAddressObj
    }

    
}
