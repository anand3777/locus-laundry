//
//  MLForgotPasswordModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/26/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON


class MLForgotPasswordModel: NSObject {
    var emailID: String?

    func forgotPasswordDetails(dict : [String : JSON]) -> MLForgotPasswordModel {
        
        let forgotPasswordobj = MLForgotPasswordModel()
        
        
        
        if let emailString = dict["email"]?.stringValue {
            forgotPasswordobj.emailID = emailString
        }
        return forgotPasswordobj
    }

}

