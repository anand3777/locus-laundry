//
//  MLPriceListModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 7/31/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLPriceListModel: NSObject {

    var userID: String = ""
    var latitudeValue: String = ""
    var longitudeValue: String = ""
    
    func priceListDetails(dict : [String : JSON]) -> MLPriceListModel {
      //  print("priceListDetails dict:----->\(dict)")
        
        let priceListObj = MLPriceListModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            priceListObj.userID = userIDString
        }
        
        if let latitudeString = dict["latitude_value"]?.stringValue {
            priceListObj.latitudeValue = latitudeString
            
        }
        if let longitudeString = dict["longitude_value"]?.stringValue {
            priceListObj.longitudeValue = longitudeString
            
        }
        return priceListObj
        
    }
}
