//
//  MLUpdateAddressModel.swift
//  MR-Laundry
//
//  Created by Bala Krishnan on 8/2/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import UIKit
import SwiftyJSON

class MLUpdateAddressModel: NSObject {

    var userID: String = ""
    var address: String = ""
    var addressID: String = ""
    var defaultAddress: String = ""
    
    func updateAddressDetails(dict : [String : JSON]) -> MLUpdateAddressModel{
        print("updateAddressDetails dict:----->\(dict)")
        
        let updateAddressObj = MLUpdateAddressModel()
        
        if let userIDString = dict["user_id"]?.stringValue {
            updateAddressObj.userID = userIDString
        }
        
        if let addressString = dict["address"]?.stringValue {
            updateAddressObj.address = addressString
        }
        
        if let addressIDString = dict["address"]?.stringValue {
            updateAddressObj.addressID = addressIDString
        }
        
        if let default_addressString = dict["default_address"]?.stringValue {
            updateAddressObj.defaultAddress = default_addressString
        }
        
        return updateAddressObj
    }
    
    

    
}
