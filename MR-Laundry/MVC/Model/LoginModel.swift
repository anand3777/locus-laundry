//
//  LoginModel.swift
//  MR-Laundry
//
//  Created by LocusTech on 09/07/17.
//  Copyright © 2017 Locus. All rights reserved.
//

import SwiftyJSON

class LoginModel: NSObject {

    var OTP: String?
    var emailID: String?
    var identi: String?

    
    func parseLoginDetails(dict : [String : JSON]) -> LoginModel {
        
        //print("parseLoginDetails dict:----->\(dict)")
        
        let loginObj = LoginModel()
        
        if let otp = dict["otp"]?.stringValue {
            loginObj.OTP = otp
        }
        
        if let emailString = dict["email"]?.stringValue {
            loginObj.emailID = emailString
        }
        
        if let identiString = dict["user_id"]?.stringValue {
            loginObj.identi = identiString
        }
       
        return loginObj
    }
}
