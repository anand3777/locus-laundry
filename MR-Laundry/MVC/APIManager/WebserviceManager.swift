//
//  WebserviceManager.swift
//  AlamofireDemo

import Foundation

import Alamofire

import SwiftyJSON

class WebserviceManager: NSObject {
    static let sharedInstance = WebserviceManager()
    
    /**
     Get Product Details.
     
     : param : productType - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func getProductDetails(productType: NSDictionary,successBlock: @escaping kSuccessBlock,
                            failureBlock : @escaping kErrorBlock){
        let headers = ["Authorization": "JFRoaXMgaXMgTlNGIEF1dG9tb3RpdmUgTW9iaWxlIEFwcGxpY2F0aW9uIQ=="]

        Alamofire.request(BASE_URL + kProductList, method: .post, parameters:productType as? [String : AnyObject], encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseProductDetailsList(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break

            }

        }

    }

    
    /**
     Register user details.
     
     : param : userRegisterationDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func userRegisterationAPI(userRegisterationDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                           failureBlock : @escaping kErrorBlock){
        //let headers = ["Authorization": "JFRoaXMgaXMgTlNGIEF1dG9tb3RpdmUgTW9iaWxlIEFwcGxpY2F0aW9uIQ=="]
        
        let userRegisterationAPI = BASE_URL + kUserRegisteration
        print("userRegisterationAPI:----->\(userRegisterationAPI) \n userRegisterationDetails:----->\(userRegisterationDetails)")

        Alamofire.request(userRegisterationAPI, method: .post, parameters:userRegisterationDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseOTPDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
     User Login details.
     
     : param : userLoginDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func userLoginAPI(userLoginDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                              failureBlock : @escaping kErrorBlock){
        
        Alamofire.request(BASE_URL + kUserLogin, method: .post, parameters:userLoginDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseLoginDetailsList(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
     Verify OTP Details.
     
     : param : OTPDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func verifyOTPAPI(OTPDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                      failureBlock : @escaping kErrorBlock){
        
        let verifyOTPURL = BASE_URL + kVerifyOTP

        Alamofire.request(verifyOTPURL, method: .post, parameters:OTPDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            print("verifyOTPURL:----->\(verifyOTPURL) \n VerifyOTPDetails:----->\(OTPDetails)")

            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().userDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
   
    /**
     Order Place Details.
     
     : param : orderDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func orderPlaceDetails(orderDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                      failureBlock : @escaping kErrorBlock){
        
        let saveBookingURL = BASE_URL + kSaveLaundryBooking
        
        print("saveBookingURL:----->\(saveBookingURL) \n orderDetails:----->\(orderDetails)")
        
        Alamofire.request(saveBookingURL, method: .post, parameters:orderDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseProductDetailsList(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
     Forgot Password Details.
     
     : param : forgotPasswordDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func forgotPasswordAPI(forgotPasswordDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                           failureBlock : @escaping kErrorBlock){
        
        //print("forgotPasswordDetails:----->\(forgotPasswordDetails)")
        
        Alamofire.request(BASE_URL + kForgotPassword, method: .post, parameters:forgotPasswordDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseForgotPasswordDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }

    /**
     Price List Details.
     
     : param : priceListDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func priceListAPI(priceListDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                           failureBlock : @escaping kErrorBlock){
        
        //print("forgotPasswordDetails:----->\(forgotPasswordDetails)")
        
        Alamofire.request(BASE_URL + kPriceList, method: .post, parameters:priceListDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            print(response )

            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parsePriceListDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }

    
    /**
    Select Laundry Details(get nearest store).
     
     : param : selectLaundryDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func selectLaundryAPI(selectLaundryDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                      failureBlock : @escaping kErrorBlock){
        
        //print("forgotPasswordDetails:----->\(forgotPasswordDetails)")
        
        Alamofire.request(BASE_URL + kSelectLaundry, method: .post, parameters:selectLaundryDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseSelectLaundryDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    

    /**
     Get My Order Details(get nearest store).
     
     : param : selectLaundryDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func getMyOrderListAPI(userDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                          failureBlock : @escaping kErrorBlock){
        
        let myOrderDetailsURL = BASE_URL + kMyOrderList
        
        print("myOrderDetailsURL:----->\(myOrderDetailsURL) \n userDetails:----->\(userDetails)")
        
        Alamofire.request(myOrderDetailsURL, method: .post, parameters:userDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseMyOrderListDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
     Add Address Details.
     
     : param : addAddressDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func addAddressAPI(addAddressDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                          failureBlock : @escaping kErrorBlock){
        
        
        Alamofire.request(BASE_URL + kAddAddress, method: .post, parameters:addAddressDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseAddAddressDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
        cancelOrderDetails.
     
     : param : cancelOrderDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func cancelOrderAPI(cancelOrderDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                       failureBlock : @escaping kErrorBlock){
        
        
        Alamofire.request(BASE_URL + kCancelOrder, method: .post, parameters:cancelOrderDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseCancelOrderDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    /**
     GetAddressListDetails.
     
     : param : getAddressDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func getAddressLIstAPI(getAddressDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                        failureBlock : @escaping kErrorBlock){
        
        
        Alamofire.request(BASE_URL + kGetAddressList, method: .post, parameters:getAddressDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseGetAddressListDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }

    /**
     updateAddressDetails.
     
     : param : updateAddressDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    func updateAddressAPI(updateAddressDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                           failureBlock : @escaping kErrorBlock){
        
        
        Alamofire.request(BASE_URL + kUpdateAddress, method: .post, parameters:updateAddressDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseUpdateAddressDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    
    /**
     deleteAddressDetails.
     
     : param : deleteAddressDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */

    func deleteAddressAPI(deleteAddressDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                          failureBlock : @escaping kErrorBlock){
        
        
        Alamofire.request(BASE_URL + kDeleteAddress, method: .post, parameters:deleteAddressDetails as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseDeleteAddressDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }

    
    /**
     Register user details.
     
     : param : userRegisterationDetails - NSDictionary type.
     
     : param : successBlock - kSuccessBlock type.
     
     : param : failureBlock - kErrorBlock type.
     
     : returns : void.
     */
    /*
    func userReferalSharingAPI(userInputDetails: NSDictionary,successBlock: @escaping kSuccessBlock,
                              failureBlock : @escaping kErrorBlock){
        //let headers = ["Authorization": "JFRoaXMgaXMgTlNGIEF1dG9tb3RpdmUgTW9iaWxlIEFwcGxpY2F0aW9uIQ=="]
        
        let referalSharingAPI = BASE_URL + kReferalSharing
        print("referalSharingAPI:----->\(referalSharingAPI) \n userInputDetails:----->\(userInputDetails)")
        
        Alamofire.request(userRegisterationAPI, method: .post, parameters:referalSharingAPI as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value {
                    DemoParser().parseOTPDetails(response: json as AnyObject, successBlock: { (success, message) in
                        if success{
                            successBlock(true, message)
                        }
                        else
                        {
                            successBlock(false, message)
                        }
                    }, failureBlock: { (errorMesssage) in
                        failureBlock(errorMesssage)
                    })
                }
                break
                
            case .failure(let error):
                failureBlock(error.localizedDescription as String)
                break
            }
        }
    }
    */
    
    
}
